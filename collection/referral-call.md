# Referral Call

Here's how I handle the referral call:


## Act 1 - Getting the Referral

Robert Ortiz - Lead Automation Engineer - Metlife
https://api.twilio.com/2010-04-01/Accounts/AC881d04f3540c44cc4545bf49c9aaf04b/Recordings/RE0575b8311553b39774cb9b187facdf42.wav
Referred me to Bryan Diehl.


Glenn Allison - Tractor Supply - VP
Referral shunt to Adam Gaines. Adam is using BitBucket and Confluence.
https://api.twilio.com/2010-04-01/Accounts/AC881d04f3540c44cc4545bf49c9aaf04b/Recordings/RE9d0227ad73e718fb31e6033d82e4fbf0.wav


## Act 2 - The Referral Voicemail

https://api.twilio.com/2010-04-01/Accounts/AC881d04f3540c44cc4545bf49c9aaf04b/Recordings/REa50dd26af4638f10e1091630a5da5714.wav

Initial Voicemail

"Hey Mike, Kevin McKinley. Just had a nice conversation with Dave Mattson at Mattson Incorporated. He and I got around to talk about you and your company. He thought it might be important we speak, I promised him I'd reach out to you. Mike, call me, Kevin McKinley at 555-555-5555, 555-555-5555."

*Follow up call*

"Hey Mike, Kevin McKinley. It is 2:35 PM Wednesday. Reached out to you earlier on Monday. Hoped we could connect. Shared with you that Dave Mattson and I got around to talking about your business, and again, he thought it would be valuable you and I speak. I did promise him I would reach out to you."


## Act 3 - The Live Referral Call

Brian Diehl
https://api.twilio.com/2010-04-01/Accounts/AC881d04f3540c44cc4545bf49c9aaf04b/Recordings/RE0a23a5ffa7145f20552ed44637868ee8.wav
Not in a position to talk about GitLab until the 1st of next year.

## Referral Email

Dear {{investor_fname}},

I happened to notice on your LinkedIn profile that you’re connected to Mike Amend who just joined Lowe’s as their President of Online. They are currently modernizing their architecture, process maturity, and capabilities - and are exactly in the right position to benefit from GitLab’s application. 

Would you be willing to introduce us? If so I have attached a template for your review. Please feel free to edit and change it in any way you want.

Warm Regards,
Kevin McKinley

****

Mike, this is Kevin. I wanted to take the opportunity to introduce the two of you. Mike is a good friend of mine, and Kevin works for one of our investment companies (GitLab) He’s written a letter explaining how GitLab can help with the digital transformation you’re looking to achieve at Lowe’s, which I’ve attached below. Kevin, I would ask you reach out to Mike and set up a time to speak. If either of you want me to be part of that conversation, or have any questions, please reach out.



****

## referral email

{{first_name}},

You mention on LinkedIn that you're in {{title}} at {{company}} and I think we might be able to help each other.

Most of the {{title}}s I speak with tell me that they're looking stand up DevOps pipelines that work out-of-the-box. From what I understand about your initiatives, I think you could get an especially high ROI from us. (We've been able to help the companies like Goldman Sachs get to 1000+ builds a day)

Who should I be reaching out to over there?

Thank you so much!

{{sender.first_name}}
