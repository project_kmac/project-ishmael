# Seling to VITO

Bald Eagles perch at the top. 

Investing large amounts of your time with anyone other than the VITO or direct reports is sales malpractice.

Decision Makers get paid to say "Yes". Once they say Yes, they then go into acquisition mode. "How do I aquire an asset, resource, or ally that will over accomplish this seemingly impossible goal for VITO?"

The further you get away from VITO, the more "interpretation" you hear about what's really needed from VITO. How many steps away has your message been filtered down?

>When communicating with VITO, focus on measurable benefits (end result that will match VITO’s goals, plans, and objectives) during a particular period of time.

> When communicating with the Decision Maker, focus on advantages (what you will do to tweak, modify, customize, or implement what you sell; how what you sell will provide an edge over the competition, and/or what’s currently being used or the status quo at VITO, Inc.).

> When communicating with Seemore the Influencer, focus on features (the pieces, parts, and components that make up what your product, service, and solution actually is).

> When communicating with the Recommender, focus on functions (how people will actually use your product, service, and solution; the stuff you would put in an owner’s manual).

3 Mistakes:

1. Not Focusing on Benefits
2. Using Jargon VITO doesn't understand/care about
3. Not Respecting VITOs time

EQUAL BUSINESS STATURE

1. Passionate About Life and Business
"VITOs are not wishy-washy, indifferent, or doubtful. Their passion touches everything they do, both in their personal life and their business operations. They are grateful for each day."
2. Good Work Life Balance
3. Constantly Expanding Communication Skills
4. Curious
5. Self-Determined
6. Focus to COMPLETTION
7. Integrity
8. Team Player
9. Optimistic and Upbeat
10. Problem Solving
11. Sense of Purpose
12. Resourceful Enough to DIG for the Strategies, Resources, and Answers I need

Neither the Seemoore or the Recommender can buy anything!

Spend your time engaging with the Decision Maker and the VITO.

Attention Span:

You have 30 seconds in an email, 3 seconds in a phone call, 3 slides in a presentation, and just 30 seconds face to face.

Existing Business:

[Mr. Benefito], your team and my team have been [increasing shareholder value/revenues/margins . . . cutting expenses/increasing efficiencies/improving compliance] by an average of [X percent] for the past [Y] years. We strongly suspect that we could create similar or even greater results in your [shipping and receiving, accounting, operations and marketing departments]. How would you suggest we direct our teams to move forward in the next [XX] days to quickly uncover all of the possibilities?

New Business:

[Mr. Benefito], we have been [increasing revenues/cutting expenses/ increasing efficiencies/improving compliance] for [3 of the top 4] companies in your industry. We strongly suspect that we could create similar or even greater results in your [shipping and receiving, accounting, operations and marketing departments]. How would you suggest we move forward in the next [XX] days to quickly uncover all of the possibilities?

****

## Sample VITO Letter

The world’s largest electronic manufacturer increased stock value and reduced time-to-market by 63 days while eliminating $87.9 million in unnecessary inventory. My team led the way in just 144 days.

May 12, 20XX
Ms. Importanta
President

During the past 12 years, we have worked with 29 manufacturing companies collectively, and we’ve been able to increase margins and shareholder value. Are any of the following achievements on your list of goals, plans, or objectives for the first half of this calendar year? If so, the good news is that we have created a proven, repeatable process that we guarantee to deliver results such as:

Reduction of annual variable cost by as much as $6.4 million without compromising patient care
Up to 30 percent increase in patient billings while maintaining full compliance and fiduciary responsibilities
Lowering inventory and improving product availability by detecting supply disruptions early and responding with cost-effectiveness in mind.

Ms. Importanta, the possibility for your company to achieve similar or even greater results is difficult to determine at this point. One fact is certain: You are the one person who can initiate the call to action, and together, our team of experts can quickly explore exactly what all the possibilities are.

To greater success!
Will Prosper
ABC Corp.
760-555-1212

P.S. I’ll call your office on Thursday, May 14, at 9:30 a . m . If this is an inconvenient time, please have Tommie inform me as to when I should make the call. Or, if you like, you can reach me Monday or Tuesday between 9:00 a . m . and 2:00 p . m . I look forward to our conversation.

(Handwritten Postit to EA on the back of the letter)
[Tommie], we can actually deliver much, much more than is mentioned in this correspondence! So, if it’s okay with you, a quick conversation will help me get on the right track and stay there! If the time that I’ve selected isn’t convenient, please let me know.

*****

Outcomes:

Not A Good Time
VITO: “Hold on. I don’t have time for this. I am heading off to a
meeting (or whatever other emergency is currently playing out at VITO,
Inc.).”
You: “Okay. When we do have a chance to catch up, let’s make sure
we chat about increasing your [shareholder value] while [cutting your
time to market] and saving up to [$87,000] in [monthly] [inventory
costs].”

Not Interested

VITO: “This doesn’t sound like anything I’d be interested in.”
You: “Okay, no surprise. Before you get back to your busy day, let
me ask you: In what area of your organization do you want to see
the biggest improvements in the shortest amounts of time?”

Happy with Current State
VITO: “No thanks. We’ve got all the help we need in this area.
Our current business relationships are satisfying our needs.”
You: “Ms. Importanta, would you like to know if your loyalty to
your existing source of supply could be costing you anything?”

Send Information
VITO: “Why don’t you just send me some information?”
You: (Because you don’t yet know whether this VITO prefers to learn
by watching, listening, or reading) “Great idea. Would you prefer to
watch, listen, or read about how we’ve been able to [increase shareholder
value], [cut time to market], and save over [$87,000] each [month] of
[inventory costs] for [34] other [CEOs] in the [manufacturing industry]?”

Push Off
VITO: “We’re too busy for this right now. Why don’t you call me
back in six months?”
You: “Okay. Just to let you know, though, there’s a strong possibility
that during that time your organization could unintentionally waste as
much as [$260,000] of your hard-earned revenue.”

Do not accept unqualified shunts. Ask these questions in order to fully qualify this opportunity.

1. What specific goals, plans, and objectives must be overachieved over the next month (or quarter, or other specific period of time) at VITO, Inc.?
2. What are the expectations of a business partner that can help accomplish that for VITO, Inc.?
3. If the objectives in Number 1 and the expectations in Number 2 can be exceeded, would VITO consider you his or her business partner of choice during the next month (or quarter, or otherspecific period of time)?

Would you like to discuss {{!benefit}} or is there something else that’s more important to you?

“I feel like I’m talking too much. What is your take on what we’ve covered thus far?”


****

1. Increasing revenues and exceeding the projected revenue plan
2. Increasing efficiencies and effectiveness of: 
»» Mission-critical employees & processes
»» Workflow and related operations
»» Procurement
»» Capital resources
»» Customer-facing services"
3. Cutting and or containing costs and moving away from unpredictable expenses to accurate and forecasted budgets
4. Staying in compliance and operating well within corporate culture and any state and or federal agency and governmental regulations