# Handling Objections

This document gathers the common (RBOs) Reflex Responses, Brush-Offs, and Objections we hear from prospects.

Most RBOs come in one of the following forms:

* Not interested
* Don't have budget
* Too busy
* Send information
* Overwhelmed - too many things going on
* Just looking


#### Common Objections

https://docs.google.com/document/d/1ZAvYP61OPS5rXRK4UjHNFwB9539BUotVaRoizdtW7Ls/edit


>Brush off (I’m not interested).
I appreciate your time, what part of DevOps aren’t you interested in?

>Send me an email.
I’ll send you an email, first I’d like to make sure it’s something you’re interested in.

>I’m walking into a meeting.
When does that meeting end? I’d like to call you back.

>I’m not involved with this. *Click*
Email your pitch?

>Git what?
GitLab, an open source tool used by developers to create and manage code collaboratively...

>Are you like GitHub?
We’re similar in that we’re both build on Git. We differ in that instead of focusing on hosting open source projects we focus on the needs of the enterprise

>We use Atlassian (Bitbucket).


>We just signed with another vendor.
Who? What needs are that company meeting?

>We’re significantly invested in one of your competitors.
Which one? I can tell you we launch new features monthly and are iterating on our product faster than anyone in the market...

>I’ve never heard of GitLab before.
GitLab is the only platform for complete DevOps, use it to bring modern applications from idea to production quickly and reliably.

>What is Git?
Git is a commonly used, distributed version control system.

>We use Atlassian and are happy.
BitBucket has done a lot for the enterprise Git space, great company. That being said, GitLab Libre is completely free, Bitbucket charges if you have more than 5 users...

>We use GitHub and are happy.
GitHub has done a lot for the Git space, great company. That being said, We scale better with multiple application servers, we have multiple authentication levels, we have group level milestones, and we’re 4 times more cost effective.

>We use GitLab CE.
Our free options are great for smaller teams and personal projects. You should upgrade for distributed teams who need advanced features, high availability, and 24/7 support. Our enterprise versions are more customizable and secure.

>How did you get my number?
Mention mutual connections if you have them, or say “I had to do some digging”.

>Gatekeeper.
Clearly state the reason for your call, be direct and honest. Ask for their help to get an appointment on the targets calendar and ask for their advice on how to do so. Respect them and their role.

>I was expecting someone else.
Sometimes the wrong road brings you to the right place. *half joking*

>I don’t do anything with development anymore.
Who would you recommend I reach out to instead?

>Is this a cold call?
It’s not a call you were expecting, but I have a good reason for reaching out.

>We use TFS.


>We outsource software development.
For both internal and external projects? Do you have influence on the DevOps tools they use?

>We love GitLab and will be in touch when the timing is right.
What needs to happen for the timing to be right?

>I need to speak with _ first...
Can I put something on the calendar and invite them as well?

We already use (BitBucket/Jira/Jenkins/etc.). (The objection here being that they're pretty mature in terms of DevOps.) What can you do that our current stack doesn't already accomplish?


#### Handling Up Front

"Art, sometimes when I talk to people about what we do, and it may not be the case here, sometimes they tell me one of the following: They see all vendors as being the same; they hate the idea of going through the process of whom to select to provide this product (or service); they had a bad experience the last time they tried someone new; or they're not sure which direction or application will be best for them. Which of these, if any, Art, is a concern to you?"

-> "Really? I'm surprised by your answer. Why did you pick that one?"

#### I'm Busy

"I'm in a meeting right now. Can you email me the information?"

Makes sense, when would be a better time for me to call back?


"You've caught me at a bad time."

Listen, no worries, we'll talk another time. Might it make sense to take 40 seconds to tell you the reason for the call and then you can decide should we even talk next?

No worries Dave. I'll reach out again a different time. Hey, would it be helpful if I took 40 seconds to tell you why I was calling, and then you can tell me if we should even have that follow on call?

#### Send Information

"I'm in a meeting right now. Can you email me the information?"

What kind of information are you looking for?

#### Happy with current provider

"No one's perfect, fair statement? If there was one thing your current provider could do better, what would it be  
-> Whatever they say, don't jump on it. Do the opposite. 
-> "Who do you currently work with? Oh, I know those guys, they're great at _x_. Tell me something else..."
-> "Okay, let's go back to that first thing..."

#### We use GIT

> We do use GIT as part of our change and release process.

<!-- This objection reads to me like a misunderstanding of GitLab as GIT-->

Hi Laura, thank you for the quick response. It’s great to hear your group at Unisys is leveraging GIT based development and makes sense since we’ve noticed significant usage of our free, open-source application there. Curious, are your teams leveraging GitLab?

I’d like to find time to connect for a 15-min intro call to learn more about your current initiatives and share how we help teams release software anywhere from 3-30x faster. How does next Thursday or Friday afternoon sound?”

#### We're already in a POC

"Great, you haven't signed yet. That gives me plenty of time to get in there and share with you why you should be looking at GitLab instead."

What do you like about {{provider}}?

#### I'm not a Decision Maker / Can't push those decisions in the company

Hey, not a problem, we'd still like to speak with you and find out how you're using the product, what you like about it, would you be open-minded to a brief 5 min call sometime in the next week?

Not looking to ask you to make any decisions, buy any tools, or change any of your processes. But I was hoping you wouldn't mind lending your expertise for a few minutes and taking a look - see if this something your teams could even use. If at the end of that 15 mins you decide "We really can't use this", not a problem, we can part ways as friends.


#### That's above my paygrade


https://api.twilio.com/2010-04-01/Accounts/AC881d04f3540c44cc4545bf49c9aaf04b/Recordings/REf544c4dc02859c21678eb5289d890f6f.wav

#### Acquisition

That reminds me of the x acquistion. They used 

#### We use TFS / Azure DevOps


#### Not Relevant 

(Thanks to Jeff for this objection handling technique)

Sorry wanted to make this really relevant to you .... but what it really is is more than just a {{x}}  tool. 

Aplogize + Contextualize -> It's more than that -> Different Value Prop based on new Info

#### We already use GitLab

 I was hoping to connect regarding how you're currently using GitLab. We have a few important DevOps features coming out, and I’d like to see which features are providing the most value.

#### Push Off Too Far into the Future

"Is there a brief 10 min window in the next week where we can figure out if we should even have that longer, follow on call?"

#### Email Me More Info

Happy to do that, but just so I’m sending relevant information for you, can you tell me how many sales staff you currently have? 

How do they currently structure their phone calls? 
Have they ever missed a deal because of an objection? 
If we could help them gain more confidence on the phone and overcome objections, would you be interested in talking in more detail?

Happy to do that, but just so I’m sending relevant information for you, can you tell me a little bit about your current toolchain? 
	
For example:

* How are you currently handling your CI/CD pipeline? 
* Have you ever had downtime because of a broken integration? 
* If we could help you speed up your software delivery cycles, would you be interested in talking in more detail?


#### No Budget

 That’s exactly why I’m calling you. You see we work with companies like yours all the time and GitLab actually helps free up budget. 

 That’s why so many companies don’t even need a budget for this, because it's a FRACTION of the cost of what they're paying for their current stack/toolchain. But before we even talk about that, it’s important to see if this can help you, too.


#### Just Moved to a New Role

#### Already Happy with Tools

## Sales Wolf

### Bad Time/No Time to Talk

### Already Using Competitor

### Not Interested

### Too Busy 

### Call Next Quarter

### Email Me Info

### No Budget

### Need to Think

### Too Expensive

### Referral

"Talk to Sasi..."

"Okay, I appreciate that. Would you mind sharing with me why Sasi, not you? Want to make sure we're having the right conversations, so maybe you could help me a little bit about what you do, anything you picked up there? Want to make sure it's worth Sasi's time, would you mind mind sharing a bit about what you do so we can see if there's a fit?"

### Don't See a Need

"Thanks for checking. I don’t see the need for GitLab as we already have a CI/CD built on AWS code pipeline  with Jenkin integration. Our code repository is on bit bucket  and this set up works fine for us."

Hi Ames, great to hear that you're happy with your current set up. It may be the case that there's not a fit, but because GitLab is much more than a CI/CD tool, can I ask what would be priority for you right now? 

As an example, we typically see that after a company has built a CI/CD pipeline like yours, security, compliance, and value stream management become the next priorities.

GitLab helps address all of this and more...like collaborating across functions while managing security/risk and aligning IT and business strategies. 

While a new tool may not be a focus, would you be willing to take 10 mins to learn about GitLab's full offering and see if it lines up with any of your current goals?

**** 

Hi Dana, great to hear that it was a huge success. It may be the case that you're not pursuing GitLab, however, because you have DevOps initiatives, I'm hesitant to leave it at that.

GitLab is designed specifically to address DevOps tooling for financial services like PNC. Clients in banking like BNY Mellon, Northwestern Mutual, and ING Bank have used it to align IT strategy and innovation to business objectives and enable digital transformation throughout their entire organization. (THIS value prop is too broad -aka sucks- but he doesn't give me much meat on his LI or news to work with)

I don't want to assume that GitLab can't help with your objectives, but a quick call can definitely help us figure that out. Would you have a brief 10-min window in your calendar for this?

### Send Me an Invite

"I'm not the best at guessing calendars... what time/day works best for you?"

### Happy With Jenkins

A: We're happy with Jenkins, our pipelines are running well. It does what it needs to do and we are fully automated.

B: "You probably never ever have an issue with {{"maintaining plugins", "cloud native architecture", "scalability", "vulnerability" }} while using Jenkins, do you?"

"...but at least Jenkins is working and your pipelines are running well, right?"  

"I gotta ask you a question. A few minutes ago, you said everything was fine, no problems. And over the last few minutes, I've heard you do have some issues with Jenkins plugins/scalability/vulnerabilities. What should we do about that?"


### "We don't take unsolicited sales calls."

(No guts. "Sure, I can appreciate that, I wouldn't take one myself if I was in your shoes. Let's say I'm not going to sell you anything, you're not going to buy anything from me. Now that it's over, can I ask you a question? You're probably not experiencing any issues with...")


### Using Other Tools

* Using others, not aware of GitLab

* Using others, aware of GitLab


### We've evaluated GitLab and didn't choose it for x


Could you elaborate on that?

Did you work with us on that evaluation? We have really defined permission models. We have the segregation of duties you need for tooling. (Vendor lock in. Who has access to what?)

Help you understand your options, save you time, even if it's for down the road.


### More Information

If the answer to any of those questions are negative, there's no sense for us to meet. But if they're all positive, we should probably have at least a phone conversation if nothing else.

When should we meet, if that does happen. We'll put it in pencil. Possibly, tentatively, if you say yes to these four questions.

Let's pretend I do.... What happens next?

That question is touching on the threshold of my technical knowledge. We can certainly set up a conversation like that if you want to have that level of conversation.

### How'd you get my number?

Hamilton's handling was excellent. Answer the question and then redirect to "The reason for my call is

“We use a tool called DiscoverOrg which has the numbers of IT folks in the Fortune 2000. It’s publicly available and I can send you a link to it. But the reason for my call is…”


### We're already using GitLab

Most who are using our free version are just using a repo. We’ve made great strides since then, including CI/CD. Are you doing anything with regards to security?
You're probably not dealing with anything in regards to security?


That's okay. Most of the people I speak with are using a B2B lead database.  I'm sorry if I came across as trying to replace what you have.  I just wanted to see if you'd be open to a re-looking at your prospecting process given the recent changes that have occurred in the last 4 months to see if there are opportunities beyond what you have right now.

That's okay. Most of the people I speak with are using (tool). I apologize if I came across as trying to replace what you have (there's no business justification for that). What I'm really asking for is to see if you would be open to revisiting your pipeline given the recent shifts in DevOps to see if there are capabilities beyond what you're utilizing right now. You probably wouldn't be open to that, would you?

That's okay. I'm not sure if we're a fit, but would you be open to learning about a few new ideas, just to see if there's an opportunity to help your team speed up your software delivery?  Okay if I asked you a few questions to see if this might be relevant?


### Inbound

Does that ring a bell? -> No -> No problem, I forget half the things I download on the net. To jog your memory, it was about ways to get more qualified meetings. Generally speaking, is that of interest to you?

Does that ring a bell? -> Yes -> Curious to learn, out of all the ebooks/demos, what prompted you to view this one?

### Not a Priority

> We are busy managing our siloed, legacy apps. We know it needs to be addressed but we have too many competing priorities to even think about modernizing.

I completely understand. Many of the {{role}} I speak with are in the same boat of being too busy.

I apologize if I came across as trying to add more work to your plate (there's no business justification for that). Not looking to ask you to take on the job of modernizing.

What I'm really asking for is to see if, given some of the recent changes, you would be open to a conversation to see if there's a way we can help you get less busy. You probably wouldn't be open to that, would you?

### Not Relevant

Hey Stuart, thank you for sharing some insights and honestly, you are more sophisticated than many of the customers we help. If I may ask, are their any areas of your security testing that you are working to improve? reducing overall spend on security testing without comprimising results is often an area we help with, or getting better response time to the bugs and vulnerabilities those tests uncover? Would you be comfortable sharing what has been top of mind for your team?"