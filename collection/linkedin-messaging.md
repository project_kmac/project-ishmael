# LinkedIn Messaging


## GitLab Usage

Hi Robert,

Saw that you're focused on DevOps and that ODFL is using GitLab. As part of your enterprise team, wanted to make myself available to you and ensure we're providing the support you need. Would you be open to connecting?

Warm Regards,
Kevin

## Share Useful Information

Hi Ryan,
Reaching out to you because you're leading innovation at ATD, and thought it'd be relevant to share that both Red Ventures and Michelin use GitLab to innovate faster.
Would you be open to connecting, so I can share more useful information?
Warm Regards, 
Kevin

(The original phrasing from AJ is something along the lines of "I like to share important information with my professional network")

## Prospect Views your Profile

Happy New Year!

Saw that you viewed my profile, so thought I'd reach out and share how I can help.

You mention that you're responsible for "focused on cloud infrastructure lifecycle automation and the administration of applications in the DevOps toolchain" and that's exactly what our application enables. In fact, there are already teams at AT&T using GitLab.

Would like to briefly discuss what you're working on and see if there's a fit. Would you be open-minded to a 5-10 min call? 

Looking forward to hearing from you,
Kevin

****

Hello Derrick,

I noticed that you viewed my profile, so thought I'd reach out and share how I can help.

You mention on your profile that you're a DevOps engineer that's worked with Puppet, Jenkins, and Git, and this is exactly what our application enables.

GitLab is an "all-in-one" DevOps tool that handles the entire SDLC. Clients in retail like Dollar General and Macy's use GitLab in their deployment pipeline.

You probably wouldn't be interested in a brief 10min call to discuss what you're working on and see if there's a fit, would you? 

Warm Regards,
Kevin

## Connect Messaging

Here's what I say when sending a connect request.

### Matt Walsh Sept 17

LinkedIn Connect Note: Hi Mark, I'm connecting with technology and product leaders around Fiserv to share how GitLab is helping groups reduce toolchain friction, increase visibility down the value chain and speed up delivery + cloud deployment for software and IT operations teams. Look forward to connecting with you, Matt

Nice to connect with you Sudhir! If fourth week of September is good for a call, let me know and I'll get out an invite.


### Haitch

Hi, saw that you use Kubernetes at Allstate. GitLab is used by ING Bank, Nasdaq, and Freddie Mac for cloud native technologies, moving to microservices, or using Kubernetes and containerization. Looking for a brief 5-10 mins of your time to share how GitLab can help {{company}}, what's the best way to get on your calendar?

Haitch + ATT

Hi Chuck, saw that you lead DevOps @ AT&T and thought it'd be important to reach you. GitLab is used by Comcast, Dish, and Verizon to go cloud native, move to microservices architecture, use Kubernetes and containerization, or automate their CI/CD pipeline. Looking for a brief 5-10 mins of your time to share how GitLab can help enable digital transformation at AT&T, what's the best way to get on your calendar?



### Unisys

Hi Andrew, saw that you authored the APMS SOA Framework @ Unisys and thought it'd be important to reach out. In addition to clients like US Air Force and Interpol, some teams at Unisys use GitLab to adopt DevOps, modernize their architecture, and transition from monolithic applications. Would you be open to connecting?

Hi Alexei, saw that you lead Application Products dev @ Unisys and thought it'd be important to reach out. In addition to clients like US Air Force and Interpol, some teams at Unisys use GitLab to adopt DevOps, deliver software 3x faster, and reduce IT costs by 30%. Would you be open to connecting?

### Metlife

Hi Mike, reaching out to ask if you would be open to a call early Sept. We're working with your tooling team at Metlife and looking to share how we consolidate your toolchain, reduce IT costs, and speed up solutions delivery. Look forward to connecting with you.

### Regions / Shared Services

Hi Greg, saw that you lead Shared Services @ Regions and thought it'd be important to reach out. GitLab is used by Nasdaq, ING Bank, and Freddie Mac to deliver software 3x faster and reduce IT costs by 30% by consolidating their toolchain. Would you be open to connecting?

### Allstate

Hi Chad, saw that you lead Agile @ Allstate and thought it'd be important to reach out. GitLab is used by Nasdaq, ING Bank, and Freddie Mac to adopt DevOps, deliver software 3x faster, and reduce infrastructure costs by 30%. Would you be open to connecting?


### AT&T 

Hi Chuck, saw that you lead DevOps @ AT&T and thought it'd be important to reach you. GitLab is used by Comcast, Dish, and Verizon to speed up software delivery 3x and reduce infrastructure costs by 30%. Would you be open to connecting?

### KeyBank

Hi Keith, saw that you align IT strategy w/ business objectives @ Keybank and thought it'd be important to reach you. GitLab is used by US Bank, ING Bank, and Nasdaq to speed up software delivery 3x and reduce infrastructure costs by 30%. Would you be open to connecting?

### Cybersecurity

Hi Lori, saw that you're involved with IS at CNA and thought it'd be important to reach out. Clients like Nasdaq, ING Bank, and Freddie Mac to embrace DevSecOps and automate application security and manage access & identity, while enabling compliance for audits. Would you be open to connecting?

### Healthcare

Hi William, saw that you're leading DevOps @ Merck and thought it'd be important to reach out. GitLab helps healthcare companies like Bayer embrace DevOps and automate CI/CD, while reducing infrastructure costs by 30%, and speeding up software delivery 3x. Would you be open to connecting?

#### Some Teams

Hi Mike, saw that you were involved with DevOps at Metlife and thought it'd be important to reach out. Clients like Nasdaq, ING, and Metlife (yes, some teams there) use GitLab for DevOps, typically to automate CI and Kubernetes. Would you be open to connecting?

#### SCM + CI + K8s

Hi {{first.name}}, saw that you were involved with DevOps at Deutsche and thought it'd be important to reach out to you. GitLab is SCM + CI + K8s for banks. Even if there's no immediate need and only a "down-the-road" interest, would you be open-minded to an exploratory 10min conversation?

#### Post Connection

Hi {{first.name}}, thanks for connecting with me. I saw that you were involved with DevOps at {{company}} and thought it'd be important to reach out to you. GitLab is Monitoring + SCM + CI + K8s. Even if there's no immediate need and only a "down-the-road" interest, would you be open-minded to an exploratory 10min conversation?


#### Automation

Greg, GitLab's single app for DevOps with auto testing enables companies like Comcast and Verizon to develop software up to 26x faster while reducing costs by as much as 30%. Even if there's no immediate need and only a "down-the-road" interest, would you be open to a 10min exploratory conversation?

#### Cloud Infrastructure, Social Coding

Neerav, GitLab's single app for DevOps enables companies like Comcast and Verizon to collaborate with social coding and reduce complexity in cloud infrastructure. Even if there's no immediate need and only a "down-the-road" interest, would you be open to a 10min exploratory conversation?

#### Security Engineer

Joe, GitLab's end-to-end solution for the entire SDLC enables visibility into your pipeline, as well as regression testing and security scanning at the code level. Even if there's no immediate need and only a "down-the-road" interest, would you be open minded to a 10min exploratory conversation?


#### General

Hi Scott, our single app for the SLDC enables companies to increase collaboration/visibility, reduce toolchain costs by 30%, and speed up software delivery 26x. Even if there's no immediate need and only a "down-the-road" interest, would you be open-minded to a 10min exploratory conversation?

#### CI/CD
Hi Jerry, our single app for the SLDC enables companies to reduce toolchain costs by 30%, speed up software delivery 26x , and manage their CI/CD in only 15mins/month. Even if there's no immediate need and only a "down-the-road" interest, would you be open-minded to a 10min exploratory conversation?

#### Matt Walsh

Hi David -- noticed your work there at IBM and interesting background so thought I’d extend an invite to link up. I’m from an old IBM family in Endicott, NY, currently helping companies speed up DevOps with GitLab. Would love to connect!

Hi Vishal, I tried reaching you today to ask if you would be open to a call early Sept. We're in conversations with managers of the digital teams, sharing details on our growing relationship to GE and how we reduce toolchain friction and speed up delivery. Look forward to connecting with you, Matt.


#### Adam Pestreich

Thanks for connecting James. We are currently seeing interest in our open source application and are receiving trial requests from teams within Toyota Financial who are interested in how GitLab can speed up software development and unify the DevOps tool chain. I see that GitLab would be relevant to you and thought it would make sense to discuss your specific objectives and how GitLab would support them. I thought we could discuss how GitLab reduces DevOps tool friction, improves developer experience and allows you to accelerate delivery time by 3x. How can I go about getting 15 min on your calendar?

Thanks for connecting Aaron. There are multiple teams at John Deere using GitLab today to speed up their software development lifecycle. I see that GitLab would be relevant to you and thought it would make sense to discuss your specific objectives and how GitLab would support them. I thought we could discuss how GitLab reduces DevOps tool friction, improves developer experience and allows you to accelerate delivery time by 3x. How should I go about getting 15 min on your calendar?

Thanks for connecting Tanya. There are multiple teams at United Airlines using GitLab today to speed up their software development lifecycle. I see that GitLab would be relevant to you and thought it would make sense to discuss your specific objectives and how GitLab would support them. I thought we could discuss how GitLab reduces DevOps tool friction, improves developer experience and allows you to accelerate delivery time by 3x. How should I go about getting 15 min on your calendar?

Thank you for connecting Michael. We are currently working with Chris Rasco and his team who are looking into how GitLab can speed up software development, unify First Data's DevOps tool chain and reduce tooling cost. GitLab handles all aspects of the SDLC in a single UI and I thought we could discuss Gitlab's industry leading CI/CD tool (Forrester ranked) and your current goals for DevOps in order to determine how GitLab can support your specific needs. How should I go about getting 15 minutes on your calendar?

#### Kaleb Hill

- Connection Requests
  1) Hi {{first_name}} - I look forward to connecting over {{1 or 2 points of interest}} soon!

  2) Hi {{first_name}} - I am on the strategic account team supporting {{company}} and use LinkedIn to keep in touch/share information from time to time. Let me know if there is anything I can provide you at any time. Thanks.



## InMail

With InMail, I can reach a prospect even if I don't have their email. If I do have their email, sending the same first email of the Agoge sequence increases my footprint (omnipresence).

****

Hi Jim,

You mention on LinkedIn that you "continuously update Unisys solutions offerings in the areas of SOA, application modernization, and open source". That's exactly what GitLab enables, so I thought I'd introduce myself with a short video here for you:

https://share.vidyard.com/watch/MHbayBmza2SjPm4Y5bnhNG

In addition to clients like US Air Force and Interpol, some teams at Unisys use GitLab to adopt DevOps, deliver solutions 3x faster, and reduce IT costs by 30%.

Looking to share how GitLab can add capabilities to your team. What's the best way to get a brief, introductory 5-10 min call on your schedule? 

Warm Regards,
Kevin

****

Hello Connie,

You mention on LinkedIn that you "Lead the organization that creates the end to end architecture for satellite and OTT" at AT&T. That's exactly what GitLab enables, so I thought I'd introduce myself with a short video here for you:


GitLab is used by some teams at AT&T, as well as clients like Comcast, Dish, and Verizon to generate top line revenue while enabling executives to manage and have visbility into their value stream. Where clients can really benefit is if you're using Microservices, Continuous Integration and Continuous Delivery, and containers, Docker, or Kubernetes.

Looking for a brief 5-10 mins of your time to share how GitLab can help enable digital transformation at AT&T, what's the best way to get on your calendar?

Warm Regards,
Kevin

****

Hello Sanjeev,

You mention on LinkedIn that you're the "Digital Transformation Leader" at AT&T. That's exactly what GitLab enables, so I thought I'd introduce myself with a short video here for you:


https://share.vidyard.com/watch/9kS1Xvd5h3HCZfQpFaNo5K?

GitLab is used by some teams at AT&T, as well as clients like Comcast, Dish, and Verizon to generate top line revenue while enabling executives to manage and have visbility into their value stream. Where clients can really benefit is if you're using Microservices, Continuous Integration and Continuous Delivery, and containers, Docker, or Kubernetes.

Looking for a brief 5-10 mins of your time to share how GitLab can help enable digital transformation in your business unit. What's the best way to get on your calendar?

Warm Regards,
Kevin

Kevin
