lunch on March 13th at the Open Source Leadership Summit?

Tom, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

At the behest of the Linux Foundation, we are hosting an Executive Lunch with leaders who are driving digital transformation using open source technologies. And as you may already know, there are teams at AT&T utilizing GitLab's open source technology to innovate faster and adopt DevOps.

This will be during the Open Source Leadership Summit, in the Fireplace Room, on Wednesday, March 13th, 12:40pm - 2:10pm.

It is by invitation only, so please do let me know if we should reserve your spot.

Warmest regards,
Kevin

