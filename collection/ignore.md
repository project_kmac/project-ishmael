# Ignore 

I use this file for personal notes/daily iterations on messaging, and do not push it to master.

What do you do above what we're doing? 

******

I believe that it is because the battlefield has changed. The war is no longer won on the options, configurations, and extensibility of features that pure technologists enjoy. It is about speed of implementation. It is about simplicity, and ease of installation. It's about using DevOps to improve the Bottom Line.



Jon Gibson had mentioned some of your Quality Engineering initiatives and thought it might be beneficial that we speak. I'd like to share some of those reasons with you on this call.

GitLab is an "all-in-one" DevOps tool that integrates with QE to a) reduce time for environment provisioning, b) increase code deployments, and c) automate quality assurance and security testing.



"We have several tools in the pipeline... but we don't have the time and we don't have the people to manage all the tools."

So we need a tool to deliver our servers in an automatic way. 





*******

### Sandler Leveraging Age

Mr Smith, you've been around a long time, sir. Can I go off the record? 

How'd you like to meet the guy you were 30 years ago?

> I sure would like to meet him.

Get out your calendar. 

How old are you sir?

> I would guess you're about.

You understand I'm under a tremendous amount of pressure?

> Why?

You're 45, I'm 28. I haven't been around yet.

> I'll help you.

****


### LinkedIn Messaging

Hi Sohaib,  saw that you were involved with DevOps at Allstate and thought it'd be important to reach out to you. GitLab helps enable DevOps through automation, CI/CD, and Kubernetes. Even if there's no immediate need and only a "down-the-road" interest, would you be open to connecting?

Hi Mark,  saw that you're leading the cloud transformation at GP and thought it'd be important to reach out to you. GitLab enables cloud transformation through automation, CI/CD, and Kubernetes. Even if there's no immediate need and only a "down-the-road" interest, would you be open to connecting?

Hi Aby, saw that you were involved with Identity & Access Management + SDLC at Metlife and thought it'd be important to reach out because that's exactly what GitLab does. Clients like Nasdaq, ING, and Metlife (yes, some teams there) use GitLab for DevOps + Security. Would you be open to connecting?


### Brian's 3 Pain Points

1. Automate Continuous Integration, spinning up environments, create docker images, push to registry to code production (manage those for you). (Internal Networking Software). Automation to make your development testing or deployments better.

2. Reduce costs while increasing ROI. 

3. Increase Velocity to get products out faster. How long does it take you to deploy? How many times to deploy a year? How many times do you want to deploy?

Hi Beth, saw that you were involved with Starlink at Subaru and thought it'd be important to reach out. Clients like Toyota, Tesla, and Jaguar Land Rover use GitLab's tool for SDLC to speed up software delivery 3x and reduce toolchain costs 30%. Would you be open to connecting?





I want to make sure you have all the information you need to go share with your supervisor, and so I'd like to connect you with our product specialist. This will be super informal and educational. Should just be a brief 5-10 mins.

Would you mind if I put something on your calendar for end of late next week to sync back up after that call in case any questions come up? It's okay if we have to move that time, I just want to make sure there's time there if we need it.




Hey {{first.name}},

I saw your presentation at Dockercon

I work with SAL at GitLab. We do XYZ. Having spoken to a few architects/engineers at Metlife, we've heard that you're looking to consolidate your toolchain. 

I know you're really busy, what's the best way to get on your calendar for a short informal call in the next few weeks?

Warm Regards,
Kevin

Optimistic Foreshadowing


Hi Robert, saw that you're involved with DevOps at CNA and thought it'd be important to reach out. Clients like Nasdaq, ING Bank, and Freddie Mac to embrace DevOps and automate CI/CD, while reducing infrastructure costs by 30%, and speeding up software delivery 3x. Would you be open to connecting?


Hi Cassandra, saw that you lead the Agile transformation @ Allstate and thought it'd be important to reach you. GitLab is used by Nasdaq, Freddie Mac, and ING Bank to deliver software 3x faster while reducing costs by 30%. Would you be open to connecting?

Hi Bagirathi, saw that you lead technology adoption @ Metlife. Gitlab is used by some teams at Metlife as well as clients like Nasdaq, Freddie Mac, and ING Bank to deliver software 3x faster while reducing costs by 30%, thereby accelerating business value. Would you be open to connecting?


technology adoption for delivering innovative, cost optimized and speed to market solutions that accelerates business value.


facilitate the review of new technologies and 2) maintain the enterprise technology portfolio

Hi Bagirathi, saw that you facilitate the review of new technologies and maintain the enterprise technology portfolio @ Metlife. Gitlab is used by clients like Nasdaq, Freddie Mac, and ING Bank to consolidate toolchain, deliver software 3x, and reduce costs by 30%. Would you be open to connecting?


Hi Alex, saw that you lead architecture/tech standards @ Metlife and thought it important to reach you. Gitlab is used by clients like Nasdaq, Freddie Mac, and ING Bank to consolidate their toolchain, deliver software 3x faster, and reduce architecture costs by 30%. Would you be open to connecting?

Hi Narmada, saw that you lead DevOps @ Metlife and thought it important to reach out. Gitlab is used by clients like Nasdaq, Freddie Mac, and ING Bank to automate CI/CD, consolidate their toolchain, deliver software 3x faster, and reduce architecture costs by 30%. Would you be open to connecting?

Thank you for connecting with me John. I was looking at your summary on LinkedIn, and your experience with DevOps made me reach out. We are currently used by ING Bank, Nasdaq, and Freddie Mac to deliver software 3x faster and reduce IT costs 30% by consolidating their tool chain. Saw that you're expanding Agile/DevOps at Regions, and thought it'd be important to let you know that GitLab is only application that handles every aspect of DevOps under 1 UI (which gets rid of the bottlenecks and overhead that comes with a traditional tool chain). We also have the number 1 ranked CI/CD tool in the industry ranked by Forrester, and can integrate with your stack. Looking to find a brief 5-10 min window on your calendar to share what GitLab can do for you in your role; do you have some time in the first or second week of September?


Thank you for connecting with me Lori. I was looking at your summary on LinkedIn, and your experience with DevOps made me reach out. We are currently used by ING Bank, Nasdaq, and Freddie Mac to deliver software 3x faster and reduce IT costs 30% by consolidating their tool chain. Saw that you're expanding Agile/DevOps at Regions, and thought it'd be important to let you know that GitLab is only application that handles every aspect of DevSecOps under 1 UI (which gets rid of the bottlenecks, silos, and overhead that comes with a traditional tool chain). Additionally, GitLab performs container scanning, automated testing at the merge request level, and LDAp. Looking to find a brief 5-10 min window on your calendar to share what GitLab can do for you in your role within InfoSec; do you have some time in the first or second week of September?

Hi Vishal, I tried reaching you today to ask if you would be open to a call early Sept. We're in conversations with managers of the digital teams, sharing details on our growing relationship to GE and how we reduce toolchain friction and speed up delivery. Look forward to connecting with you, Matt.

Hi Mike, reaching out to ask if you would be open to a call early Sept. We're working with your tooling teams, sharing details on how we can help consolidate Metlife's toolchain, reduce IT infrastructure costs, and speed up solutions delivery. Look forward to connecting with you.

Hi Harlan, thanks for taking my call earlier. We're sharing details on how we can help automate Allstate's CI/CD piple, reduce build time, cut IT costs by 30%, and speed up software delivery. Look forward to connecting with you.

Thank you for connecting with me Safiya. I was looking at your summary on LinkedIn, and your experience with Agile made me reach out. We are currently used by ING Bank, Nasdaq, and Freddie Mac to deliver software 3x faster and reduce IT costs 30% by consolidating their tool chain. Saw that you're expanding Agile/DevOps at Regions, and thought it'd be important to let you know that GitLab is only application that handles every aspect of DevSecOps under 1 UI (which gets rid of the bottlenecks, silos, and overhead that comes with a traditional tool chain). Additionally, GitLab performs container scanning, automated testing at the merge request level, and LDAp. Looking to find a brief 5-10 min window on your calendar to share what GitLab can do for you in your role within InfoSec; do you have some time in the first or second week of September?

Thank you for connecting Safiya. We are currently working with teams at Metlife who are looking into how GitLab can speed up software development, consolidate Metlife's tool chain, and reduce IT costs. Looking to find a brief 5-10 min window on your calendar to share what GitLab can do for you in your role; do you have some time in the first or second week of September?


Hi Nilesh, saw that you provide Strategic direction to products built with DevOps @ Allstate and thought it'd be important to reach out. GitLab is used by Nasdaq, ING Bank, and Freddie Mac to adopt DevOps, automate their CI/CD pipeline, and reduce IT costs by 30%. Would you be open to connecting?



Hi Shane, saw that you planning& implementing CI/CD @ Allstate and thought it'd be important to reach out. GitLab is used by Nasdaq, ING Bank, and Freddie Mac to adopt DevOps, automate their CI/CD pipeline, and reduce IT costs by 30%. Would you be open to connecting?




**************


Hello Benjamin,

I appreciate your invitation for a call with your team, and requesting more information about GitLab.

GitLab is a single application for the whole software development lifecycle, (but it’s not a rip and replace). We integrate with your existing tool stack and meet you where you are to reduce administrative complexity as well as licensing costs. Clients like ING Bank, Freddie Mac, and Nasdaq use GitLab to speed up software delivery 3x while reducing overhead 30% and automating their CI.

Some of the features that our clients get the most use out of is GitLab Container Registry, CI/CD, integration with Kubernetes, automated security and testing at the code level, as well as performance metrics for deployed apps.

Naturally you’re going to have a bunch of questions for me, such as: what can GitLab do for you at Regions, how well we can integrate with your stack, the benefits of GitLab for CI, etc.. 

Obviously, we're going to need to better understand some things, such as: what are your current strategic initiatives, what's in your toolchain, etc. So we'll also have a few questions for you.

At the end of the brief call we will decide what the next steps will look like, and how to best proceed—even if it’s a no at this stage. 

Also on the call will be Sean Billow (SAL) and Brian Wald (Solutions Architect). 

Proposed Agenda:
1. Mutual Introductions
2.. Regions's current SDLC toolchain / DevOps ecosystem
4. Overview of GitLab
5. Questions/next steps

If you feel that you want to invite anyone else, just drop me a quick note. Please feel free to modify and add to the agenda as you see fit to better use our time together.

Warmest regards,


*********


Hi Lara,

You mention on LinkedIn that "Change and Release Management, Development Operations" are part of your role. That's exactly what GitLab enables, so I thought I'd introduce myself with a short video here for you: https://share.vidyard.com/watch/hvtFeLtvcU3Bh3Ni6XwsiD



In addition to clients like Lockheed Martin and Rockwell Automation, some teams at Unisys use GitLab to adopt DevOps, reduce IT costs by 30%, and deliver software 3x faster.

What's the best way to get a brief, introductory 5-10 min call on your schedule?

Warm Regards,
Kevin



Hi Andrew, saw that you authored the APMS SOA Framework @ Unisys and thought it'd be important to reach out. In addition to clients like US Air Force and Interpol, some teams at Unisys use GitLab to adopt DevOps, modernize their architecture, and transition from monolithic applications. Would you be open to connecting?


****

Hello Senthil,

You mention on LinkedIn that your succesfully completed a "Rollout of a no-touch Continuous Integration/Deployment (CI/CD) using Docker/Kubernetes images and a Micro Services architecture". That's exactly what GitLab enables, so I thought I'd introduce myself with a short video here for you:

https://share.vidyard.com/watch/XwMsQvpH9zsUf3c1vso4QT?

GitLab is used by some teams at AT&T, as well as clients like Comcast, Dish, and Verizon to go cloud native, move to microservices architecture, use Kubernetes and containerization, or automate their CI/CD pipeline. 

Looking for a brief 5-10 mins of your time to share how GitLab can help enable digital transformation at AT&T, what's the best way to get on your calendar?

Warm Regards,
Kevin

****


Hello Connie,

You mention on LinkedIn that you "Lead the organization that creates the end to end architecture for satellite and OTT" at AT&T. That's exactly what GitLab enables, so I thought I'd introduce myself with a short video here for you:


GitLab is used by some teams at AT&T, as well as clients like Comcast, Dish, and Verizon to generate top line revenue while enabling executives to manage and have visbility into their value stream. Where clients can really benefit is if you're using Microservices, Continuous Integration and Continuous Delivery, and containers, Docker, or Kubernetes.

Looking for a brief 5-10 mins of your time to share how GitLab can help enable digital transformation at AT&T, what's the best way to get on your calendar?

Warm Regards,
Kevin


Hello Sanjeev,

You mention on LinkedIn that you're the "Digital Transformation Leader" at AT&T. That's exactly what GitLab enables, so I thought I'd introduce myself with a short video here for you:


https://share.vidyard.com/watch/9kS1Xvd5h3HCZfQpFaNo5K?

GitLab is used by some teams at AT&T, as well as clients like Comcast, Dish, and Verizon to generate top line revenue while enabling executives to manage and have visbility into their value stream. Where clients can really benefit is if you're using Microservices, Continuous Integration and Continuous Delivery, and containers, Docker, or Kubernetes.

Looking for a brief 5-10 mins of your time to share how GitLab can help enable digital transformation in your business unit. What's the best way to get on your calendar?

Warm Regards,
Kevin


****

Hello Aaron,

You mention on LinkedIn that you "Support cloud platform using kubernetes" at AT&T. That's exactly what GitLab enables, so I thought I'd introduce myself with a short video here for you:

https://share.vidyard.com/watch/opphgcbhomER4sNopou24U

GitLab is used by some teams at AT&T, as well as clients like Comcast, Dish, and Verizon to enable DevOps, orchestrate their Kubernetes Clusters, move to a containerized  Microservices architecture, and automate their CI/CD pipeline.

Looking for a brief 5-10 mins of your time to share how GitLab can help enable digital transformation in your business unit. What's the best way to get on your calendar?

Warm Regards,
Kevin

