# Module 1: Account Research

## Why

## 
The Customer Awareness Spectrum as adapted for GitLab Enterprise Sales.

1. Most Aware. GitLab Customer - Land and Expand, believes our solution will solve their pain.
2. Product Aware: GitLab CE User - connects their problems and pains to our solution.
3. Solution Aware: No GitLab Usage, maybe uses GitLab competitor, is actively seeking solutions, tools, and capabilities.
4. Problem Aware: No GitLab Usage, has some DevOps/Agile/Digital Transformation Initiatives.
5. Unaware: No GitLab, No Initiatives, has little to no awareness of the existence of pain or your solution.


|---  	|---   	|   Yes	|  No 	
|--- 	|---	|---	|---	
| Yes  	|---   	|   	|   	
| No 	|---   	|   	|   	



## Ideal Client Profiles

If you are not speaking with decision makers or direct influencers, then you are committing sales malpractice. As part of the Enterprise Sales Team focusing on Large and Strategic Accounts, if you are spending the majority of your time selling to anyone other than the C-Suite, VP level, or Directors, you are committing sales malpractice. This is like the world's best heart surgeon going out and putting band-aids on boo-boos at a children's clinic while there are critical cases waiting on the surgery table.

