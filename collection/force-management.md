# Force Management

## Value Framework

Before Scenarios -> Negative Consequences
After Scenarios -> Positive Outcomes

Required Capabilities-> Metrics
How We Do It -> Better -> Proof Points
Discovery & Trap Setting Questions


Create Value Cards per each Value Driver

Value Drivers are related to revenue, cost, & risk:
* The value buyers are seeking (even if your company didn’t exist)
• Causes Economic Buyers to reallocate discretionary funds fast
• Are “outside-in” and belong to the customer

We target this to the "Center of Mass Persona" as opposed to each individual ICP.

## Objection Handling Guide

#### We Already Have a Solution

I'm glad to hear that you're already working with a provider. This confirms that you see the value in using such a solution to achieve {{positive_business_outcome}}

I'm calling you because in addition to {{positive_business_outcome}}, we've worked with companies like {{client}} to {{result}}

Discussing the matter for 15 minutes sometime this week will either confirm that your current solution is the best choice for your business, or give you some insights into potential opportunities.

#### We don't have a budget for your product

Thank you for the insight {{first_name}}. I understand why you may be hesitant to open some budget for a solution you have no experience with. 