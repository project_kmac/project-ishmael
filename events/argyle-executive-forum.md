# Argyle Executive Forum

subject: Dinner at The President's Room tomorrow night?

{{first_name}}, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

I saw on your profile that you lead application architecture and thought you might enjoy our complimentary executive dinner on this topic. As you may already know, there are teams at P&G using our application, GitLab.

In partnership with the Argyle Executive Forum, we're hosting a dinner roundtable bringing leading IT executives together to discuss how to integrate application security with digital transformation.

Colleagues from JPMorgan Chase, Kroger, Macy's, and more will be in attendance.

During the dinner, we’ll be discussing key topics such as: 

* Innovative ways to drive culture change within your organization
* Streamline application delivery, and foster continuous improvement on a day to day basis 
* Discuss best practices to succeed in today’s digital landscape by using automation to unlock velocity 
* Methods to avoid spending too much time complicating your application security 
* The importance of monitoring key metrics to ensure success

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

As this is happening tomorrow night, May 2, you probably wouldn't be available to join us, would you?

Warm Regards,
Kevin


*****


subject: Dinner at 1789 Restaurant June 13th?

{{first_name}}, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

You mention your profile that you {{!earn_the_right}}. As such, I thought you might enjoy our complimentary executive dinner on this topic. As you may already know, there are teams at {{!company}} using our application, GitLab.

In partnership with the Argyle Executive Forum, we're hosting a roundtable bringing leading IT executives together to discuss how to integrate application security with digital transformation.

Executives from SS&C, Capital One, and more will be in attendance.

We’ll be discussing key topics such as: 

* Innovative ways to drive culture change within your organization
* Streamline application delivery, and foster continuous improvement on a day to day basis 
* Discuss best practices to succeed in today’s digital landscape by using automation to unlock velocity 
* Methods to avoid spending too much time complicating your application security 
* The importance of monitoring key metrics to ensure success

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You probably wouldn't be interested in joining us, would you?

Warm Regards,
Kevin