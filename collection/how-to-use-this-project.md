# How to Use This Project

### This is a living document. 

I use Sublime Text, have a local copy of the project, and add content as I go. This allows me to have a local repository and history of messaging that I've used and tried that I can always reuse with any company, vertical, or prospect.

Keep a file called "ignore.md" that I use for notes/daily iterations that aren't to be pushed to the master branch.

The important changes, we roll out and go live to master.

### It's iterative, opinionated, and we try to keep it up to date with what's working today. 

We say what we've tried, we share what works, and whenever possible we share results.

For example, if you encounter an objection that you hadn't heard before (even if it stumped you), note it down in Handling Objections, provide a template for responding, and merge.

This way we build documentation of best practices.

### Please contribute.

If you find anything useful in here, please contribute. If you find something is lacking, please contribute.

We all stand on the shoulders of those who have come before us, and it is each of our responsibility to lay the groundwork/foundation for those yet to come.

Be a **Citizen** (actively participate), not a **Consumer**.

I guarantee that there is something YOU can contribute that no one else is able to add. Go forth and contribute.