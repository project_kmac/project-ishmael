Fanatical Prospecting by Jeb Blount Notes:

******************


Chap 5. The More you Prospect, the Luckier you Get 

Three Core Laws of Prospecting:

1. The Universal Law of Need - The more you need something, the less likely you are to get it. Do whatever it takes to not be desperate. (The Borrower is Slave to the Lender; Greed wins over Need)
2. The 30-Day Rule - The prospecting you do in this 30 day period will pay off for the next 90 days. (Lead time and lag time.)
3. The Law of Replacement - You must constantly push new opportunities into your pipeline to replace the opportunities that will naturally fall out, at a rate that matches or exceeds your closing ratio. (For example, if you have a 10% closing ratio, when you close one deal, you must bring in 10 more prospects to fill up the pipeline and replace the one you just sold.)

The first rule of holes is that when you are in one, stop digging. The first rule of sales slumps is that when you are in one, start prospecting.

Chap 6: Know Your Numbers

"Everything around you is mathematics. Everything around you is numbers." - Shankuntala Devi

Know your Ratios. Elite athletes know their numbers. Michael Phelps.

Track your activities. 

Chap 7: The Three Ps that are Holding you Back

"Start by doing what's necessary; then do what's possible; and suddenly you are doing the impossible." - St Francis of Assisi

Procrastination:

"Do not wait to strike till the iron is hot; but make it hot by striking." - Yeats

You eat an elephat one bite at a time.

The journey of a thousand miles beings with one step.

The next sales begins with the next call.

Perfectionism:

Ceramics class split in two groups. One group focuses on making one perfect vase all semester. The other group makes as many vases as possible. Which group created the better vase in the end? 

Paralysis by Analysis:

"Once begun, the job's half done" - Felix Dennis

Chap 8: Time

Parkinson's Law: Work expands to fill the time alloted for it.

Horstman's Corollary: Work contracts to fill the time alloted.

Focus. No multi-tasking. Do one thing, do it well, and do it until it is completed.

Chap 9: The Four Objectives of Prospecting

"I don't focus on what I'm up against. I focus on my goals and I try to ignore the rest." - Venus Williams

Four core objectives of prospecting: 

* Set an Appointment
* Gather information and qualify
* Close a sale
* Build familiarity

Define the strike zone. "Don't swing at nothin' ugly." 


Chap 10: Leveraging the Prospecting Pyramid

Chap 11: Own Your Database

"The most expensive thing you can do in sales is spend your time with the wrong prospect." - Jeb Blount

Chap 12: The Law of Familiarity

ASK FOR REFERRALS: "Thank you again for your business. I'm glad to hear you are happy with us. I'm working hard to add more customers like you. Would you be able to introduce me to other people in your network who might want to use our product?"

Chap 13: Social Selling


Chap 14: Message Matters

"Hi, Windsor, this is Jeb Blount from Sales Gravy. The reason I am calling is to set an appointment with you. I read in Fast Company that you are adding another hundred sales reps to keep up with growth. I imagine that it has got to be a bit stressful to bring on that many reps and get them producing.

I’ve worked with a number of companies in your industry to reduce ramp-up time for new reps. At Xjam Software, for example, we cut ramp-up time to ROI for their new reps by 50 percent.

While I don’t know if our solution would be a fit in your unique situation, I’ve got some ideas and best practices I’ve seen work well for companies like yours and thought you might be interested in learning more about them. How about we get together for a short meeting on Thursday at 2:00 PM ?”

Chap 15: Telephone Prospecting Excellence

"Eat the Frog" Do the hardest, most important task of the day first thing in the morning.

In lieu of this chapter, follow Prospect the Sandler Way. 

https://www.youtube.com/watch?v=vaQf7n8XO7w

Chap 16: Turning Around RBOs

Reflex Response, Brush-Offs, and Objections

Reflex Reponses (Use a Pattern Interrupt)

Brush-Offs are about avoiding conflict.

Objections tend to be logical rebuttals.

RBOs come in the following forms:

* Not interested
* Don't have budget
* Too busy
* Send information
* Overwhelmed - too many things going on
* Just looking

Chap 17: The Secret Lives of Gatekeepers

Chap 18: In Person Prospecting

Chap 19: Email Prospecting

Chap 20: Text Messaging

Chap 21: Developing Mental Toughness

Outlearn = Outearn

"The cultivation of the mind is as necessary as food to the body." - Cicero

“We should live as if we will die tomorrow and learn as if we will live forever.” - Ghandi

Chap 22: Eleven Words that Changed My Life

"Do more than is required. What is the distance between someone who achieves their goals consistently and those who spend their lives and careers merely following? The extra mile." —Gary Ryan Blair

"When it is time to go home, make one more call"

Chap 23: The Only Question That Really Matters

"How Bad Do You Want It?"

The world is naught but will and represtentation. 