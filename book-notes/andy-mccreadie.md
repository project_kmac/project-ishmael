# Andy McCreadie

https://learn.sandler.com/mod/page/view.php?id=35404

Qualify Stringently, Close Easily

The Culmination of a Process
Starts with calling on the right prospect. Ends with effectively connecting what you have to offer with the outcome the prospect desires.

## 1 - We're not sure we need it

* What did you like about it?

* What age do you want to retire?
* What's your plan to achieve that?
* How are you being tax efficient with your earnings?
* How are you ensuring your money is invested in the best places?
* Would having a plan to achieve this be useful?
* Why would it be useful?


## 2 - We Need It, but Not Today

Help them discover the impact of waiting.

* If you solve this problem now, what is the benefit to your organization?
* What's the benefit to you, personally?
* Are you committed to taking action, now?
* If you put off doing something, will things get better, or worse?
* What is the cost of putting this off?

## 3 - I Prefer Someone Else

FIND OUT WHY THE WOULD OR WOULD NOT DEAL WITH YOU

## 4 - The Moment Has Passed

YOU HAVE TO MOVE WITH PACE

"You know, {{month}} gets kind of busy. Just on the off chance, take a look at your calendar for next week. I've got a couple of slots. You wouldn't happen to have anything there, do you?"

#### Price is a Decision

Train them early on that they have to make decisions. Train them early on so that they make the ultimate decision at the close.

* "Let me tell you why I'm calling, then you tell me if we should be talking. Is that fair?"
* Might be worth carrying on talking for another 2-3 minutes. Why don't we do that, I'll ask a couple more questions, and then let's decide whether or not to keep talking. Is that okay for you?
* We'll meet for an hour. I'm going to ask some questions, you'll do the same, look at the end, if it makes no sense to continue, just tell me. We'll park it. And I'll do the same if I can't help. On the other hand, if we think we should schedule another meeting, we will get our diaries out and we'll book it. Does that sound fair?

By the time they get to the fulfillment step, they should be used to working like that with you. You should have people telling you things like, "I"ve got it, yeah, we'll make a decision." 

When you get to the fulfillment step, the price of your presentation has to be a decision. The price of a presentation is a decision.

"Do you think that sounds reasonable?"


#### Worst Case Scenario

Are you going to be in a position to make a decision at the conclusion of our meeting next Tuesday? What, if anything, might prevent you from doing so?

Are you going to be in a position to make a decision at the conclusion of our meeting next Tuesday? Let me be clear. If anything I present, doesn't fit, if you're even 1% uncomfortable, I want you to say No to me. Will you be okay doing that?

Now, on the other hand, if what I present fits, it's what you expect, it reflects what we've talked about, then typically we get the paperwork out and move forward. Is that something you would be able to do, if you're comfortable? 

Let's schedule 60 minutes for the presentation, and that will leave you 30 mins to discuss with your colleagues, then you can tell me what you decided.

Why don't we do this, we'll schedule a call for Friday morning. All I ask is that we both commit to making that call. Will you be able to tell me on that call whether or not we'll move forward. Is that something that could work for you?

#### Prospect Breaks Contract

Last week when we scheduled this meeting, you said you were willing and able to make a decision today. You said you would tell me "Yes" or "No". Now you're not willing to do that. What happened?

## 5 - We Don't Have That Budget

Know their expectations and limitations.

How much is in the budget? What if it needs to be more? I expect it to come in between x and y. Is that going to be workable for you? Where would that money come from?

These are our payment terms. Is that going to work? Should we do business together?

## 6 - I Want It, But They Don't Agree

Does anyone else have to approve this before we can get started?

## 7 - Something Doesn't Fit

There really shouldn't be questions in the fulfillment step IF...

* You did a great job of qualifying
* You checked that nothing had changed
* You set the UFC with very specific terms about what will happen next
* You present something that mirrors what they said (rather than add stuff that goes beyond what you found) 

The presentation should be nothing more and nothing less than what you uncovered in the qualifying steps. Nothing less.

Two reasons: 
1) you added something - and then they ask questions and you create objections you never needed to handle
2) you try to sell the whole damn elephant in the first sale - if you sell today, you can educate tomorrow. sell a monkey's paw and grow the account after that.

## The Mindset

1. You are the doctor of your world. You have a right to a thorough and proper diagnosis
2. You can't lose what you don't have.
3. The hard way is the easy way. Ask the tough questions early.
4. Expect great things. Debrief rigorously.
5. Excuse free zone. 