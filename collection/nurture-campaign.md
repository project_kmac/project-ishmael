# Nurture Campaign

This is for MOF (Middle of Funnel) prospects that need more information, need to be warmed up, etc.

Create objection handling templates, pain positioning templates as they are discovered.

****

## SDLC 2chainzzz

subject: Why use 5 tools where 1 will suffice?

Hey {{first_name}},

Application rationalization...

Toolchain consolidation...

Whatever your organization calls it, it comes down to this...

"Why use 5 tools where 1 will suffice?"

Well, did you know that GitLab is a single application that handles the ENTIRE SDLC (or DevSecOps) toolchain?

One application to do what typically takes Jira + GitHub/Bitbucket + Jenkins + (whatever else is in your stack).

This gives you faster software delivery, creates visibility in your pipeline, eliminates silos/bottlenecks, and reduces your IT costs.

So why use a traditional toolchain? (I actually had a DevOps Engineer brag that they've got 79+ tools in their stack.)

Especially when we're not a rip-and-replace (unless you're into that kind of thing), and we can coexist peacefully with your existing stack.



Listen, if you're interested in learning more, I'd be happy to speak with you and share how enterprises like Nasdaq, Verizon, and NASA are standardizing on GitLab to deliver software 3x faster while reducing IT costs.

Just send back a one word response "YES" and we'll find a time that works.

Warm Regards,
Kevin "SDLC 2chainzzz" McKinley

****

## Jenkinstein

subject: Is your org running a Jenkinstinian Monstrosity?

Hey {{first_name}},

“It’s great that Jenkins has 1,400 plugins, but you’re not supposed to install 1,400 plugins”–CloudBees CEO Sacha Labourey

If you're anything like most of our clients, chances are you're facing challenges with Jenkins including "Service instability. . . Brittle configuration. . . Assembly required. . . Reduced development velocity".  

Kawaguchi himself wrote a blog post about this where he revealed that: They plan on BREAKING backward compatibility in their upcoming releases, and they plan on introducing a NEW flavor of Jenkins for Cloud native

...Which means that If you’re a Jenkins user today, it’s going to be a rough ride ahead.

Well, did you know that GitLab is not only SCM, but also has built in CI/CD for a seamless experience?

In fact, Forrester Wave ranked it as the #1 CI tool.



Here's my offer. If your organization is running on Jenkins, and you're frustrated with the vulnerabilities, brittle integrations, or interested in learning about a better way, let's talk.

We'll get on the phone and discuss about your toolchain, DevOps initiatives, CI/CD pipeline - and you'll get 2-3 actionable ideas you can use immediately, whether we decide there's a next step or not.

If you're interested in learning more, simply send me back a one word reply "YES".

Warm Regards,
Kevin "Jenkinstein" McKinley

****

P.S. Whenever you’re ready... here are 4 ways I can help you start or scale DevOps in your business unit:
⠀⠀⠀
1. Grab a free copy of Gary Gruver's book "Starting and Scaling DevOps in Enterprise".
It’s the roadmap to starting DevOps, scaling it in Enterprise, and avoiding the common pitfalls of being a change agent. — Click Here (https://about.gitlab.com/resources/scaling-enterprise-devops/)
⠀⠀⠀⠀
2. Try GitLab SaaS
It's super easy and there to try GitLab. You can use our FREE online Saas Offering with Unlimited Private Projects, Built in CI/CD, and 2,000 CI pipeline minutes per group per month on our shared runners. https://about.gitlab.com/pricing/#gitlab-com

3. Try GitLab Self-Hosted
You can easily stand up your own instance of GitLab (we are open-core). DigitalOcean has an easy 1-click install, or you can use our Omnibus installer here: https://about.gitlab.com/install/

4. Work with our Enterprise Team privately. 
If you’d like to work directly with me and my team to take help you implement GitLab in your stack... just send me a LinkedIn message/connect with the word “Enterprise”... tell me a little about your role and what you’d like to work on together, and I’ll get you all the details!


