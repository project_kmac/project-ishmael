# The DevOps Handbook Notes

> ### The Costs: Human And Economic
>
> When people are trapped in this downward spiral for years, especially those who are downstream of Development, they often feel stuck in a system that pre-ordains failure and leaves them powerless to change the outcomes. This powerlessness is often followed by burnout, with the associated feelings of fatigue, cynicism, and even hopelessness and despair.
>
> Many psychologists assert that creating systems that cause feelings of powerlessness is one of the most damaging things we can do to fellow human beings--we deprive other people of their ability to control their own outcomes and even create a culture where people are afraid to do the right thing because of fear of punishment, failure, or jeopardizing their livelihood. This can create conditions of *helplessness*, where people become unwilling or unable to act in a way that avoids the same problem in the future.
> For our employees, it means long hours, working on weekends, and a decreased quality of life, not just for the employee, but for everyone who depends on them, including family and friends. It is not surprising that when this occurs, we lose our best people (except for those that feel like they can't leave, because of a sense of duty or obligation).

> ## THE ETHICS OF DEVOPS: THERE IS A BETTER WAY
>
>[...]
> By solving these problems, DevOps astonishingly enables us to simultaneously improve organizational performance, achieve the goals of all the various functional technology roles (e.g., Development, QA, IT Operations, Infosec), and improve the human condition.

