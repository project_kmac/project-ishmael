# Gary Gruver

Templates for the Gary Gruver Events.

## Email

subject: Are you looking to start or scale DevOps?

Hey {{first.name}},

I know this is short notice, but we're going to be visiting your area and thought it'd be important to loop you in.

Gary Gruver (co-author of "Leading the Transformation: Applying Agile and DevOps Principles at Scale") and the GitLab team will be in Chicago next Tuesday, Oct 2.

Because of your role at {{company}}, we would like to hold a complimentary spot for you.

You can expect to meet other DevOps Engineers, Architects, and Directors from Transunion, Motorola, and Redbox to name a few.

Wanted to invite you in for a chance to connect with some of your peers.

Here are the details:
Date: Tuesday, October 2nd
Time: 8:30 AM - 2:00 PM
Location:  Kimpton Hotel Palomar Chicago (505 N State St, Chicago, IL 60654))

The event starts at 8:30 AM and we will close the session around 2 PM. Breakfast & lunch will be provided. 

If you would like to be added to the guest list, simply send back a one word reply "YES" and I'll make sure you get in for free.

Warm Regards,

Kevin

****

Hey {{first.name}},

I know this is short notice, but we're going to be visiting your area and thought it'd be important to loop you in.

Gary Gruver (co-author of "Leading the Transformation: Applying Agile and DevOps Principles at Scale") and the GitLab team will be in New York next Wed, Oct 3.

Gary will share his experiences, best practices he’s used with his Fortune 500 clients, and answer your questions on how to lead significant DevOps and Agile transformations in mission-critical software environments.

You can expect to meet 30 other DevOps Directors, Engineers, and Architects from companies like Goldman Sachs, AMEX, and Northwestern Mutual.

Wanted to invite you for a chance to learn from an industry pioneer and connect with some of your peers.

Here are the details:
Date: Wednesday, Oct 3rd
Time: 12:00 PM - 5:00 PM
Location:  The Benjamin (125 E 50th St, New York, NY 10022)

The event starts at 12:00 PM and we will close the session around 5 PM. Lunch and afternoon snacks will be provided.

If you would like to be added to the guest list, simply send back a one word reply "YES" and I'll make sure you get in.

Warm Regards,

Kevin

****

## Joey D 

subject: Gary Gruver talk, downtown on the 3rd

{{first_name}}- Because of your role at {{company}}, you’re invited to an afternoon with DevOps Industry Pioneer, Gary Gruver on Oct. 3rd at the Benjamin Hotel in NYC. We would like to open a spot for you.

You can expect to meet others from Goldman Sachs, Black Rock, AMEX, Guardian Life, Northwestern Mutual to name a few.

Gary will share his experiences, best practices he’s shared with many of his Fortune 1000 clients, and answer your questions on how he led significant DevOps and Agile transformations in mission-critical software environments.

Mind if I send along more information?

Best,
Joe

****


subject: Gary Gruver talk, downtown next Wednesday Oct 3

{{first_name}}- you’re invited to an afternoon with DevOps Industry Pioneer, Gary Gruver on Oct. 3rd at The Benjamin Hotel in NYC. 

Because of your role at {{company}}, we would like to hold a complimentary spot for you.

You can expect to meet others from companies like Goldman Sachs, Black Rock, AMEX, and Northwestern Mutual.

Gary will share his experiences, best practices he’s used with Fortune 500 clients, and answer your questions on how to lead significant DevOps and Agile transformation in mission-critical software environments.

Here are the details:
Date: Wednesday, Oct 3rd
Time: 12:00 PM - 5:00 PM
Location:  The Benjamin (125 E 50th St, New York, NY 10022)

The event starts at 12:00 PM and we will close the session around 5 PM. Lunch and afternoon snacks will be provided.

If you would like to be added to the guest list, simply send back a one word reply "YES" and I'll make sure you get in.

Warmest regards,
Kevin


****

subject: Gary Gruver talk, downtown next Tuesday Oct 2nd

{{first_name}}- you’re invited to an afternoon with DevOps Industry Pioneer, Gary Gruver on Oct. 2nd at the Kimpton Hotel Palomar in Chicago. 

Because of your role at {{company}}, we would like to hold a complimentary spot for you.

You can expect to meet others from companies like Transunion, Motorola, and Redbox.

Gary will share his experiences, present best practices he’s used with many of his Fortune 1000 clients, and answer your questions on how to lead significant DevOps and Agile transformation in mission-critical software environments.

Here are the details:
Date: Tuesday, October 2nd
Time: 8:30 AM - 2:00 PM
Location:  Kimpton Hotel Palomar Chicago (505 N State St, Chicago, IL 60654))

The event starts at 8:30 AM and we will close the session around 2 PM. Breakfast & lunch will be provided. 

If you would like to be added to the guest list, simply send back a one word reply "YES" and I'll make sure you get in for free.

Warmest regards,
Kevin

#### Subject Lines to Test

You’re invited to Our Roadshow with Special Guest Gary Gruver

Meet us at the Gary Gruver Roadshow

Meet us at The Benjamin Hotel in NYC

Meet us at the Kimpton Hotel Palomar

Reserve your seat at the Gary Gruver Roadshow

You're invited!

{{first_name}}, here's your invite

{{first_name}} you're invited 