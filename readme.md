# Project Ishmael: Sales Development as Self Mastery

"We're not improving the process... We're just doing a new thing that's making everything else go away." 
- John Rzeszotarski, SVP of Continuous Delivery @ KeyBank

## Letter To You

Dear SDR,

In the following pages, I would like to present to you a methodology and philosophy. I believe in sales development as self-development, and that those who go down this path consciously choose self-mastery.

In pursuit of this, I'll outline below what I believe are the key skills and subjects each SDR must learn during his/her tenure. 

Some of these may be controversial and contradict what is often repeated as common sales practice. Take what you will, try what excites you, and leave what doesn't work for you. This is a way, not The Way, and I don't presume that my dogma is better than yours.

It is my hope that those of you who are willing to go on this journey will find great value in what lies within.

Warmest Regards,

K-Mac

## The Seven Games:

Seven categories of interactions for you and your prospect. In practicing these Seven "Games", you develop a language that will earn you your prospect's respect as a leader and subject matter expert. The Games are based on the ways your prospects want to be sold to, and are the basis of communicating and reaching mutual agreement.

We do not force, coerce, manipulate, or use fear-based tactics. We invite and receive. We are curious and we listen. We diagnose and we offer help.

1. The Pattern Interrupt - We start with the pattern interrupt in which we earn your prospects attention, whether it be a cold call, cold email, or other form of contact. Why a pattern interrupt? Because they are busy going about their day and to simply go into our pitch would be disrespectful and ineffectual. By doing something that's different, we garner attention and have an opportunity to provide value.
2. The Up Front Contract - The up-front contract is a way of removing the feeling of pressure from prospects. 
3. Earn The Right - This game is about showing your prospect that you have done your homework, that you are not just spamming them out of the blue, and that what you have to say matters to them because you UNDERSTAND them. 
4. The 30 Second Commercial - Here's what's usually referred to as your "pitch". The 30 Second Commercial is a specific type of pitch, using what are called Third-Party Stories to engage on an emotional level. Imagine a commercial for a movie and how it engages you with a Set Up, Conflict, and Resolution. Now imagine a movie commercial that, instead of telling a story, sells you based on logic and bullet points (for example, "Watch this movie because it had a budget of $100 million, it uses the latest in special effect technologies, stars The Rock, and was nominated for 10 Oscars." ) One of these is logic, the other is emotion - prospects buy based on emotion, albeit we justify purchases through logic.
5. Fishing for Pain - AT likes to call this "Create the Need". This game is about creating space for the prospect to share what they need help with and allowing them to find reasons why they need your help. The most compelling reason is pain, as we humans are loss averse and would do anything to get out of pain. 
6. The Call to Action - The game of CTA is all about finding creative ways to ask for the appointment, and positioning it in such a way that it is easy for the prospect to accept.
7. The Post Sell - This game helps you reduce no-shows by eliminating the buyer's remorse and ambiguity about the next step. We make it clear what the agenda is, we make it clear that there is mutual agreement, respect, and expectations. They are not simply buying a tool from a vendor - we are evaluting each other as potential partners.

1. Pattern Interrupt
2. Up-Front Contract
3. 30 Second Commercial
4. Fishing for Pain
5. Reversing
6. Negative Reverse Selling
7. Post-Sell

We want our prospects to become aware of what they need, confident we can help, and ready to take the next step with us.

It is my belief that the best SDRs are fueled by empathy for their prospect. My intent when I'm engaging with a prospect is thus, "I understand the problems you are facing. I'm here to show you that I understand what you need and can help you find a solution. I will not coerce, force, manipulate, or use fear based tactics. Instead I will educate, inspire, and invite you into a partnership designed to help alleviate your problems."



## Onboarding Bootcamp

This is a proposed order of how to onboard SDRs to get them ramped within just 4 weeks. I present this to you as an SDR looking back as what I wish my onboarding would have consisted of. Starting at GitLab can feel very much like drinking out of a firehose. In just a few weeks, you are expected to learn what GitLab is (challenging when we are adding new capabilities every month), understand the market you compete in (difficult when we compete against so many verticals in the space), and then be able to produce SAOs at the end of an unstructured onboarding experience during which we spend the majority of time watching videos and learning about the "Features" of GitLab. 

I suggest we break onboarding into a four week bootcamp, complete with practical exercises and applied theory that prepares new SDRs for the type of scenarios they will face.

1. Module 1: Account Research
2. Module 2: The Written Text
3. Module 3: Live on the Phones
4. Module 4: Surrounded By Campaigns


## Release Notes

You are now reading Project Ishmael v2.0

Contributions in this Version include:

- Moved all of the previous version into the Deprecated Folder
- Introduction of the Seven Games 
- 4 week bootcamp for onboarding new reps
