# 10 Commandments of Sales Development


1. Thou Shalt Always Prospect (Adding New Prospects is the Key to Keeping Your Pipeline Full. Keep in mind the The Law of Replacement, which states, "You must constantly push new opportunities into your pipeline to replace the opportunities that will naturally fall out, at a rate that matches or exceeds your closing ratio. (For example, if you have a 10% closing ratio, when you close one deal, you must bring in 10 more prospects to fill up the pipeline and replace the one you just sold.)) 

2. Thou Shalt Always Have a Reason for Reaching the Prospect (Why You, Why Now?)

3. Thou Shalt Have Patience (Lead Time & Lag Time) The prospecting you do in this 30 day period will pay off for the next 90 days.

4. Thou Shalt ALWAYS Personalize Thine Messaging (If you're not willing to spend 5 mins doing account research and crafting a custom message, why should they invest 15 mins in a call with you?)

5. Thou Shalt Master The Art of Cold Calling (Pattern Interrupt + UpFront Contract -> 30 Second Commercial)

6. Thou Shalt "Tap-Tap-Tap" with Thine Emails (Emails are like Golf, just get the ball closer every time)

7. Thou Shalt Leverage LinkedIn Lightly (By Offering Value - Don't just connect for the appt)

8. Thou Shalt Wisely Prioritize Which Prospects to Go After (Segment Propsects in to Tiers 1, 2, and 3)

9. Thou Shalt Uphold the Golden Hours (Money time is money time - You call between 8:30 AM - 10 AM the prospects time - no one interrupts the golden hours.)

10. Thou Shalt Know Thine Prospects Business (Account Research - Your Business is to Know Their Business... so you can help them better)