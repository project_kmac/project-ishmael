# Google Next


Please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

## dinner at FANG Restaurant on Tuesday?

Hello Chris, 

Please accept my apology for the intrusion. I'm writing you for two reasons.

First, I wanted to congratulate you on the recent promotion. I'm excited for you and look forward to seeing how you continue to evolve Key's delivery.

Secondly, I noticed that you're speaking at Google Cloud Next.

As as sponsor for the conference, we are hosting an Executive Dinner with leaders who are driving digital transformation through GKE and Kubernetes adoption.

Executives and DevOps Practictioners from Google, Blackrock, and Verizon will be in attendance.

This will be on Tuesday, April 9th at the FANG Restaurant (660 Howard Street, San Francisco, CA) from 7:00pm - 9:30 pm.

It is by invitation only, so please do let me know if we should reserve your spot.

Warmest Regards,
Kevin

*****

Hello Michael, 

Noticed that you signed up for the ARC207 session "GitLab's move from Azure to GCP" during Google Next, so I thought you might like to join us for dinner as well.

As as sponsor for the conference, we are hosting an Executive Dinner with leaders who are driving digital transformation through GKE and Kubernetes adoption.

Executives and DevOps Practictioners from Blackrock, Verizon, and CNCF will be in attendance.

This will be on Tuesday, April 9th at the FANG Restaurant (660 Howard Street, San Francisco, CA) from 7:00pm - 9:30 pm.

It is by invitation only, so please do let me know if we should reserve your spot.

You probably wouldn't be interested in joining us for this evening, would you?

Warmest Regards,
Kevin

*****

### Speaker Invitations


Hello Michael, 

Noticed that you'll be speaking at Google Cloud Next, so I thought you might like to join us for dinner as well.

As as sponsor for the conference, we are hosting an Executive Dinner with leaders who are driving digital transformation through GKE and Kubernetes adoption.

Executives and DevOps Practictioners from Blackrock, Verizon, and CNCF will be in attendance.

This will be on Tuesday, April 9th at the FANG Restaurant (660 Howard Street, San Francisco, CA) from 7:00pm - 9:30 pm.

It is by invitation only, so please do let me know if we should reserve your spot.

You probably wouldn't be interested in joining us for this evening, would you?


Warmest Regards,
Kevin

*****