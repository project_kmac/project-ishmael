# Looping You In

I use this with accounts where we have already made contact and have some level of activity (whether that's CE usage, IQMs, etc.)

As long as the email gets delivered and isn't caught in spam, you'll find high open and CTR.


****
## Email

subject: {{first_name}}, looping you in

Hey {{first_name}},

We're discussing toolchain consolidation & cost reduction with some teams at {{company}}. Thought it'd be important to loop you in so I made a short video here for you:

{{video_thumbnail}}
{{link_to_video}}

You mention on LinkedIn that your expertise is in "improving efficiencies, reducing costs, and increasing revenue through application development". That's exactly what GitLab enables. 

Clients like ING Bank, Nasdaq, and Freddie Mac use GitLab to consolidate/streamline their toolchain, reduce IT costs by 30%, and deliver software 3x faster.

What's the best way to get a brief, introductory 5-10 min call on your schedule?

Warm Regards,
{{sender.first_name}}

****

## Video Examples

I've used two styles of video.

### Guerilla Selfie Style

Michael Slama:
https://drive.google.com/file/d/1WloOKL6HdlMEsp7ZW2Cy-0SvztdJ_q4x/view?usp=sharing

Jeff Murr-
https://drive.google.com/file/d/1zV3CqSd43rNDweJzvPPQWrDV33ZBWoLL/view

### Vidyard

Michael Roseman-
https://share.vidyard.com/watch/czkX8bcgPXFNHVBXGwSpRF

Susan Golden-Perkins
https://share.vidyard.com/watch/skTCTedbAvrwzWkkiWAtAr