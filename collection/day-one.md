The vision is that a new SDR can join GitLab and on day-one is able to jump in and make dials. In other words, delivering value on the first day on the job. By the afternoon, our new hires are able to produce qualified appointments that convert into SAOs.

Instead of having to come up with his/her own sequence, call template, voicemail script, etc...  (All of which introduce ambiguity, work & rework, and inefficiencies into the system. Think about it, how much did you know about GitLab on your first day? Would you have been effective and able to sell a meeting to a prospect then?)

Instead of all that entropy... Imagine there's a standardized process that we KNOW works and has a proven return. 

What if, as an SDR, you have a roadmap you can follow to predictably hit your SAOs? Wouldn't it be a relief if you could remove the ambiguity and know exactly how many calls, how many emails, and where you are in the process to hitting your goals?

All of this happens by following a simple structure. Our new SDR doesn't need to be an expert in GitLab, DevOps,  or even have any product knowledge for this to occur. In fact, our new SDR doesn't need to know anything except how to pick up the phone and follow the process.

Remember, this happens on day-one. 

That's what I'm going to share with you today... a path forward to hitting our SAOs and SCLAU quotas that will keep those pipelines full and maximise sales velocity.

