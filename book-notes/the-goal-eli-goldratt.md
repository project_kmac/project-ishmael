# The Goal : A Process of Ongoing Improvement by E M Goldratt

The goal of a business is to make money.

"If the company doesn't make money by producing and selling products, or by maintenance contracts, or by selling some of its assets, or by some other means . . . the company is finished. It will cease to function."

"If the goal is to make money, then (putting it in terms Jonah might have used), an action that moves us toward making money is productive. And an action that takes away from making money is non-productive."

Productivity is taking actions that move us closer to the goal ("making money").

Cash flow. ROI. Net Profit.

"Yeah, but suppose you've got enough cash coming in every month to meet expenses for a year," I tell him. "If you've got enough of it, then cash flow doesn't matter."

"But if you don't, nothing else matters," says Lou. "It's a measure of survival: stay above the line and you're okay; go below and you're dead."

Cashflow is a measure of survival: stay above the line and you're okay; go below and you're dead.

(Think of Paul Graham's "How Not to Die")

So this is the goal: To make money by increasing net profit, while simultaneously increasing return on investment, and simultaneously increasing cash flow.

**THROUGHPUT, INVENTORY, OPERATIONAL EXPENSE**

Your cold calls and emails that lead to SCLAUs = throughput

The prospects you've loaded up in sequences = inventory

You can have good inventory (correct direct dials and emails, connections on LinkedIn, high level prospects that fit ICP) and bad inventory (bad numbers, prospects that have a low ICP fit, aged data)

"Throughput," he says, "is the rate at which the system generates money through sales." Through sales, not production. If you produce something but don't sell, it's not throughput.

Our throughput is conversations->SAOs/SCLAUs.

"Inventory is all the money that the system has invested in purchasing things which it intends to sell."

"Operational expense is all the money the system spends in order to turn inventory into throughput."

"We are not concerned with local optimums." (Remember, Jenkins got stuck in a local optimum. Same for GitHub, which is why they're trying to iterate and add Actions and the rest of the SDLC.)


Throughput is conversations into SCLAUs. How many conversations does it take to produce a SCLAU?

Inventory is the prospects loaded for outreach (not Outreach, but outreach).

OPEX is how many actions / all the activity you have to spend to turn prospects into SCLAUs.

"Increase throughput while simultaneously reducing both inventory and operating expense." -> Increase SCLAUs while simultaneously reducing prospects and actions.

"Interesting, isn't it, that each one of those definitions contains the word money," he says. "Throughput is the money coming in. Inventory is the money currently inside the system. And operational expense is the money we have to pay out to make throughput happen. One measurement for the incoming money, one for the money still stuck inside, and one for the money going out."

Money lost and carrying costs is OPEX. Any investment we can sell is inventory.

Manage the capacity of your plant according to the goal.

"A balanced plant is essentially what every manufacturing manager in the whole western world has struggled to achieve. It's a plant where the capacity of each and every resource is balanced exactly with demand from the market."

"The real reason is that the closer you come to a balanced plant, the closer you are to bankruptcy.

Balance = capacity of every resource matches demand from market

The goal is not to reduce OPEX by itself, inventory by itself, or increase throughput by itself. 

Increase SCLAUs while reducing Prospects and Actions.

Dependent events = bottlenecks

Statistical fluctuations = ?

Calls/emails/video/social touches (OPEX) are DEPENDENT EVENTS. They depend on prospects loaded (inventory).

"What's happening isn't an averaging out of the fluctuations in our various speeds, but an accumulation of the fluctuations. And mostly it's an accumulation of slowness—because dependency limits the opportunities for higher fluctuations. And that's why the line is spreading. We can make the line shrink only by having everyone in the back of the line move much faster than Ron's average over some distance."

Remember the first rule of investing - Don't Lose Money. This applies here as well.

"There was no reserve. When the kids downstream in the balanced model got behind, they had no extra capacity to make up for the loss. And as the negative deviations accumulated, they got deeper and deeper in the hole."

**"...as the negative deviations accumulated, they got deeper and deeper in the hole."**

Remember Mike Caro's Threshold of Misery.

"Optimal pace - If this were my plant, it would be as if there were a never-ending supply of work—no idle time. But look at what's happening: the length of the line is spreading farther and faster than ever before."

"whoever is moving the slowest in the troop is the one who will govern throughput"

The bottlenecks govern the throughput.

"If this were my plant, Peach wouldn't even give me three months. I'd already be on the street by now. The demand was for us to cover ten miles in five hours, and we've only done half of that. Inventory is racing out of sight. The carrying costs on that inventory would be rising. We'd be ruining the company"

"The idea of this hike is not to see who can get there the fastest. The idea is to get there together. We're not a bunch of individuals out here. We're a team. And the team does not arrive in camp until all of us arrive in camp."

"if you guys want to go faster, then you have to figure out a way to let Herbie go faster," 

"The maximum deviation of a preceding operation will become the starting point of a subsequent operation."

"A bottleneck," Jonah continues, "is any resource whose capacity is equal to or less than the demand placed upon it. And a non-bottleneck is any resource whose capacity is greater than the demand placed on it."

The idea is to make the flow through the bottleneck should be on par or slightly less than demand from the market.

Adjust capacity so that the bottleneck is at the front of production.

Out on
the trail, you could tell the slower kids by the gaps in the line.
The slower the kid, the greater the distance between him and the
kid in front of him. In terms of the analogy, those gaps were
inventory."

If we've got a Herbie, it's
probably going to have a huge pile of work-in-process sitting in
front of it."

Bottlenecks are followed by WIP (work-in-process).

How do you increase capacity?

* reorganize everything so the resource with the least capacity would be first in the routings
* purchase more equipment 
* offloading the bottleneck to other resource (outsourcing/offshoring)

Learn how to run your plant by its contraints

"Your bottlenecks are not maintaining a flow sufficient to meet demand and make money," he says. "So there is only one thing to do. We have to find more capacity."

**"If you are like most manufacturers, you will have capacity that is hidden from you because some of your thinking is incorrect."**

**"If you lose one of those hours, or even half of it, you have lost it forever. You cannot recover it someplace else in the system. Your throughput for the entire plant will be lower by whatever amount the bottleneck produces in that time."**

DO NOT LOSE GOLDEN HOURS. Your throughput will be lower by whatever amount the bottleneck produces in that time. You cannot recover conversations elsewhere in the system.

USE QUALITY CONTROL IN FRONT OF THE BOTTLENECKS, NOT AFTER.

This is why we focus outreach on TIER 1 prospects. You don't want SALs having conversations with individual contributors, or directors who don't have DevOps initiatives. They're wasting their time and will kick it back later anyway. We do QC at the top - who we prospect into. Outreach must be done on TIER 1 high quality prospects who a) have initiatives that can benefit from GitLab and b) have decision making/influence ability. VOLUME DOESN'T MATTER. Upping volume of prospects only decreases quality of outreach and increases INVENTORY that has to be processed. You're wasting time and costing the company MILLIONS in revenue by introducing entropy/inventory without QC.

"Make sure the bottleneck works only on good parts by weeding out the ones that are defective. If you scrap a part before it reaches the bottleneck, all you have lost is a scrapped part. But if you scrap the part after it's passed the bottleneck, you have lost time that cannot be recovered."


"I want to be absolutely sure you understand the importance of the bottlenecks," says Jonah. "Every time a bottleneck finishes a part, you are making it possible to ship a finished product. And how much does that mean to you in sales?"

Bottlenecks make it possible to ship a finished product. How much does that mean to you in sales?

"What you have learned is that the capacity of the plant is
equal to the capacity of its bottlenecks," says Jonah. "Whatever
the bottlenecks produce in an hour is the equivalent of what the
plant produces in an hour. So ... an hour lost at a bottleneck is
an hour lost for the entire system."

The capacity of the plant is equal to the capacity of the bottlenecks. 

Optimizing the use of bottlenecks requires 

1) Making sure that the bottleneck's time is not wasted  

1a) No Idle Bottleneck time
1b) Not processing parts that are already defective (QC)
1c) Make it work on parts you don't need

and

2) Increasing bottleneck capacity

2a. Bottlenecks only work on what contributes to throughput today
2b. Take load off bottlenecks and give it to non-bottlenecks

* reorganize everything so the resource with the least capacity would be first in the routings
* purchase more equipment 
* offloading the bottleneck to other resource (outsourcing/offshoring)



"Because what happens when you build inventory now that
you won't sell for months in the future? You are sacrificing pres-
ent money for future money; the question is, can your cash flow
sustain it? In your case, absolutely not."

"Then make the bottlenecks work only on what will contribute to throughput today . . . not nine months from now"

The other way you increase bottleneck capacity is to take some of the load off the bottlenecks and give it to non-bottle-
necks."

"Do all of the parts have to be processed by the bottleneck? If not, the ones which don't can be shifted to non-bottlenecks for processing. And the result is you gain capacity on your bottleneck. A second question: do you have other machines to do the same process? If you have the machines, or if you have a vendor with the right equipment, you can offload from the bottle-neck. And, again, you gain capacity which enables you to increase throughput."

"reduce the efficiency of some operations and make the entire plant more productive."

" Whenever it's possible to activate Y above the level of X, doing so results only in excess inventory, not in greater throughput."

Play http://adarkroom.doublespeakgames.com/ to understand this concept

You have created this mountain of inventory with your own decisions. And why? Because of the wrong assumption that you must make the workers produce one hundred percent of the time, or else get rid of them to 'save' money."

Activating a resource =/= utilizing a resource

They can also happen asynchronously

"we must not seek to optimize every resource in the system," says Jonah. "A system of local optimums is not an optimum system at all; it is a very inefficient system."

Four elements of time to inventory/part/WIP:
1. Setup time - waiting for a resource
2. Process time - being alchemised
3. Queue time - time in line waiting
4. Wait time - time a part waits to be assembled

CUT BATCH SIZES - (Not processing 1000 prospects at once, but 10-100 prospects at a time?) And here I was thinking that larger prospect batch sizes helped efficiency. /don't want to be trapped in a local optimum

 What are the steps the Bearington plant took?

1. Withold the release of materials and time the releases according to the bottleneck
2. Cut batch sizes in half for non-bottlenecks

WRONG ASSUMPTION #1: Balance capacity with demand, then maintain flow

Correct Paradigm: Balance flow with demand. We need excess capacity.

WRONG ASSUMPTION #2: The level of utilization of any worker is determined by his own potential

Correct Paradigm:  For any resource that is not a bottleneck, the level of activity from which the system is able to profit is not determined by its individual potential but by some other constraint within the system

WRONG ASSUMPTION #3: When someone is working, we get some use out of him. Utilization and activation are the same

Correct Paradigm: Utilization and activation are asynonymous.

"as you climb up the ladder and your responsibilities grow, you should learn to rely more and more on yourself. Asking me to come now will lead to the opposite; it will increase the dependency."

I want to learn how to manage my life.  -> What techniques are needed for effective management?

They all made common sense, and at the same time, they flew directly in the face of everything I'd ever learned. Would we have had the courage to try to implement them if it weren't for the fact that we'd had to sweat to construct them? Most probably not. If it weren't for the conviction that we gained in the struggle—for the ownership that we developed in the process—I don't think we'd actually have had the guts to put our solutions into practice.


"Spelling out the answers when you are trying to convince someone who blindly follows the common practice is totally ineffective. Actually there are only two possibilities, either you are not understood, or you are understood."

"Almost every big company is oscillating, every five to ten years from centralization to decentralization, and then back again."

A process of ongoing improvement.

Choosing throughput as the most important measurement.

The weakest link determines the strength of the chain. If you want to improve the strength of your chain, the first step is to find it's weakest link. It's bottleneck.

Here are the steps of a process of continuous improvement:

1. IDENTIFY the system's constraints
2. Decide how to EXPLOIT those contraints
3. SUBORDINATE everything else to the above decision
4. ELEVATE the system's constraints
5. WARNING!!!! If in the previous steps a constraint has been broken, go back to step 1, but do not allow INERTIA to cause a system's constraint.


"Determining the management techniques must come from the need itself, from examining how I currently operate and then trying to find out how I should operate."

*****

So if we define the goal as: Increase SCLAUs while simultaneously reducing prospects in process and activities needed to convert (calls, emails, etc.) The bottleneck is "How many of the right prospects can you add consistently?" 

Using only DiscoverOrg data leads to the problem of too many leads (who are not necessarily ICP- so there's a quality control issue there as well). 

Filtering through SalesNav using buying trigger keywords (such as "GCP Migration") leads to the second problem of not enough leads (because we have to do manual data entry to get them uploaded into SFDC -> Outreach). 

The solution is to do the LinkedIn SalesNav search, scrape + extract those leads into a CSV, upload the CSV into multiple data sources (DiscoverOrg, Zoominfo)  to fill in the information, and then upload to SFDC -> Outreach. 

This solves the manual data entry issue, while keeping prospect quality high (they are ICPs with buying trigger keywords) and maintaining volume (you do this for every named account and company in your GEO).

Ultimately, this reduces both your inventory of prospects and opex of prospecting activites. Because you're no longer doing bulk uploads of all TEDD and IT in DiscoverOrg from your TAM (total addressable market) --  only the ICPs with buying trigger keywords within your territory.

"Identify the system's constraints," he reads. "That is not the
problem now. The problem is that the bottlenecks are moving all
over the place

That means that there are times when the non-bottlenecks must have more capacity than the bottlenecks.

If the upstream resources don't have spare capacity, we won't be able to utilize even one single resource to the maximum; starvation will preclude it.

"Then the contribution of any single person to the organization's purpose is strongly dependent upon the performance of others."

"If synchronized efforts are required and the contribution
of one link is strongly dependent on the performance of the
other links, we cannot ignore the fact that organizations are not
just a pile of different links, they should be regarded as chains."

The important thing is
you've just proven that any organization should be viewed as a
chain. I can take it from here. Since the strength of the chain is
determined by the weakest link, then the first step to improve an
organization must be to identify the weakest link."

"I still claim that there are only few constraints. Our division
is too complex to have more than a very few independent chains.
Lou, don't you realize that everything we mentioned so far is
closely connected? The lack of sensible long-term strategy, the
measurement issues, the lag in product design, the long lead
times in production, the general attitude of passing the ball, of
apathy, are all connected. We must put our finger on the core
problem, on the root that causes them all. That is what actually is
meant by identify the constraint. It's not prioritizing the bad ef-
fects, it's identifying what causes them all."

The real constraints, even in our plant, were not the machines, they were the policies.

Thinking process:

1. What needs to be changed?
2. What should it be changed to?
3. What actions will cause the change?