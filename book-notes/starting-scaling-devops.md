# Starting and Scaling DevOps in The Enterprise Notes

#### Chap1 
DP = Deployment Pipeline = The flow of code from idea to production

Creating, documenting, automating, and optimizing the Deployment Pipeline is key to improving efficency and effectiveness.

Document the DP so everyone has a Common View.

Optimize the DP based on Value Stream Mapping to align the organization (ensure that we're delivering business value, not just code -- no one wants to sit on inventory that doesn't get bought)

The biggest barriers to adoption are not technical, but involve organizational change management and getting people to work differently. Help the org understand the principles, while providing flexibility (self-responsibility+authority) to allow them to develop and take ownership of their plans.

"A couple of things are important to consider when contemplating this type of organizational change: first, start where it provides the most benefit so you can build positive momentum, and second, find executives that are willing to lead the change and prioritize improvements that will optimize the DP instead of letting teams sub-optimize their segment of the DP." pg 13

Infrastructure as code forces a common definition of environments and deployment processes and ensures consistency.

#### Chap2 The Basic Deployment Pipeline

Business Idea - > Developer -> Enironment Testing -> Prodction 


Problems with waterfall are twofold:

First is the excess inventory that leads to waste and rework.

Second is that priorities are not evolving alongside the marketplace. This leads to a) throwing out the requirements and having to replan, b) sticking to the plan and delivering something nobody wants. 

"The technical and cultural shifts associated with this will change how everyone works on a day-to-day basis. The goal is to get people to accept these cultural changes and embrace different ways of working. For example: As an Operations person, I have always logged into a server to debug and fix issues on the fly. Now I can log on to debug, but the fix is going to require updating and running the script. This is going to be slower at first and will feel unnatural to me, but the change means I know, as does everyone else, that the exact state of the server with all changes are under version control, and I can create new servers at will that are exactly the same. Short-term pain for long-term gain is going to be hard to get some people to embrace, but this is the type of cultural change that is required to truly transform your development processes." - pg 25

DevOps will help you address issues that have been plaguing you for years that were not visible while operating at a low cadence. When you deploy once a month, you don't see the issues repeating enough to see the common cause/underlying pattern that needs to be fixed. Deploying each day reveals the patterns.

When deploying manually, you can use brute force which takes up time, energy, and creates frustration.

When deploying daily, you cannot brute force, but must automate to improve frequency - automation demands that you fix repetitive issues.

Three Types of Work:

1) New and Unique Work
2) Traige Work to Discover Issues
3) Repetitive Work

New and unique has to be optimized based on increasing the feedback so that we don't waste time and energy on things that won't work with changes other people are making, won't work in production, or don't meet customer needs. For new and unique work, ideally feedback comes from the customer.

Validating with the customer is done to address the fact that 