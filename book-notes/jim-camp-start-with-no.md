# Jim Camp - Start With No 

Negotiation is the human effort to bring about agreements between two or more parties with all parties having the right to veto.

"Great negotiators seek the no because they know that's when the negotiation begins."

"our opportunities for success do not come with our action. They begin to develop for us in our re-reaction. So action, reaction, re-reaction."

We only try to manage our activity and behaviour - not the result.

Transactional Analysis - 

Parent states come in nurturing or critical.

Child states are rebellious, adaptive, and natural.

The greatest failing in the system is to maintain that nurturing parent state.

