# Call Script

I keep a log of my calls that lead to conversations and their results here: https://docs.google.com/document/d/1tHyXf243vutvoJYpT8A58r_TnV9yVZqy/

### Miscellaneous Tactics

Whenever possible, do your calls standing up, in your power stance, doing The Steeple (see https://gitlab.com/gitlabkevin/sdr-playbook/blob/master/book-notes/body-language.md)

Use these two techniques sparingly for TIER 1 prospects that you've identified have the appropriate initiatives ONLY:

Double-tap = call once, do not leave voicemail, if they don't pick up, immediately call again

Triple-dip = call 3x in one day (do not leave voicemail)

### Features and Benefits Script

https://docs.google.com/document/d/1aXeFKzwu5eGgGI6x_JrljIDL_ssTZrA7M7o0sAFNXVw/edit?usp=sharing

A - Approach/Intro
“Hi________, this is _______ calling with GitLab
...I know you weren’t expecting my call…
...so i’ll be brief OR ...do you have just a few minutes?
...did I catch you at an okay time?
...have you heard of GitLab?

B - Brief/Benefit Statement (Why You?)
“I recently___(read/saw/spoke to/understand)__that___(something you found from research)__
...The purpose for my call is…
...While I have you on the phone, I had a couple quick questions...

C - Connect/Ask ?’s
A few examples
...Have you heard of GitLab?
“No” - tell them
“Yes”
Where did you hear about it?
Have you used it before?
...What are you using for code review?
...What do you like about X tool/current benefits from X tool?
...Anything you would change about X tool if you could?

D - Dive into Value (Why Now?)
Once you’ve found an area where we can potentially add value…
Initial Benefit Statement (focused on how we can help, give example with case studies)
Recommend/Suggest next steps based on pain, needs, and goals identified
Be assumptive, be consultative

E - Exit/Commitment scheduled next event
Meeting first - if they say no to initial meeting, bring up the pain, need, reposition and ask again if they could get X value, would it be worth a 5-10 minute call?
“Why not?” Form…
If still no, suggest/recommend webinar, Event, etc.
If still no, biggest.recommend case study, article (something you can send and follow up on)

F - Follow-up
Send recap email of the conversation, invite to meeting or send info to read
Set tasks for follow up in the future with notes


Hi {{first.name}} this is Kevin McKinley from GitLab. Can I share with you the reason why I'm calling, and then you can decide if it makes sense for us to talk further?

I recently saw on the the news... The purpose for my call is to find 5 minutes to discuss with you how GitLab addresses shortcomings in your toolchain. 

Our single application for the entire software development toolchain helps companies across all DevOps maturity levels that are concerned with modernizing their infrastructure and adopting cloud or transforming from on-premise, unhappy with the silos/handoffs/bottlenecks that slow down their solution delivery, or disappointed by the amount of time it takes to manage and maintain the integrations within the different tools used in the organization. Over 100,000 enterprise customers, including Nasdaq, Freddie Mac, and ING Bank use GitLab to deliver solutions 26x faster, implement automation that reduces architecture overhead by 30%, and tighten the feedback loop between business and IT (enabling you to deliver solutions that bring business value and get to market faster). Does any of that seem relevant to you, and is worthy of a brief conversation?


### Pain Based Script (Sandler)

Hey Mike, Kevin McKinley. Looking for just a little bit of help. I don't know if it's going to make sense for us to talk. Let me put a little bit of context around the conversation, and then you can tell me if it makes sense for us to talk further. Fair enough? 

I saw the big news about the AT&T / Time Warner merger.

Mike, I'm with GitLab, and the reason I'm reaching out is GitLab's single application for DevOps enables telecom companies like AT&T to simplify architecture and eliminate toolchain complexity while increasing visibility into the delivery pipeline. We've helped clients like Comcast, Dish, and Verizon deliver software up to 26x faster while reducing infrastructure, administrative, and licensing costs by as much as 30%.

Does any of that sound relevant or worthy of a 3-5 minute conversation? / Even if there's no immediate need, and only a "down-the-road" interest, are you open minded to a 10 minute exploratory conversation?


"Mike let me ask you, any of that ever been an issue worth a 3-5 minute conversation?"

"If you had to pick one, where would you start?"

"When you say it's... Tell me a little bit more about that."

"Can you help me for a second, I want to make sure I can see the world from your point of view. Can you give me kind of a real life example?"

They start telling a story. **Every Time I Think About It I Get Angry**. Give them time to put themselves into the story. Let them relive the experience, because people buy emotionally.

"And so because of that, what happens?"

"Can I make a suggestion? Do this for me. Take a look at your calendar. Let's find a time in the next week, two weeks, where you can invite me out for an hour. You and I can talk much more in depth about some of the things you're doing with regard to {{problem/pain}}, what's working, what are some of the things you do know you need to change, pick my brain about the kind of work we do, and then we can figure out together, is there a reason to do some work together. If not listen, 100% okay. We're very good at what we do, we're not right in every situation. And we can figure that out together. Sound fair? And if it does make sense then we can begin to map out more specifically what would be the steps your organization and my organization would take to really figure out, does it make sense to do business together?

****

 Mike, I’m with GitLab. GitLab's single application for DevOps enables companies like yours to simplify architecture and eliminate toolchain complexity while increasing visibility into the delivery pipeline. 

We’ve been very successful in developing and implementing (brief description of product/ service) for (companies/ people) who are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). 

Mike, I'm with GitLab, and the reason I'm reaching out is GitLab's single application for DevOps enables telecom companies like AT&T to simplify architecture and eliminate toolchain complexity while increasing visibility into the delivery pipeline. We've helped clients like Comcast, Dish, and Verizon deliver software up to 26x faster while reducing infrastructure, administrative, and licensing costs by as much as 30%.

GitLab is used by over 100,000 organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel. And our CI/CD tool is ranked #1 in the industry by Forrester

Does anything at all of what I’ve just said (sound like an issue for you/ seem relevant to your world/ describe something with which you are dealing)?


> So, Bill, I’m with (So-and-So Inc.). We’re a company that specializes in (very brief description of what you do). Bill, I spend a lot of my time talking to (Vice Presidents of Everything), typically in the (manufacturing) world. And typically, when I speak to those people, here’s what they say to me: They tell me their people are doing a good job of (A, B, and C), but they are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). Has any of that ever been relevant to your world, or even worth a brief conversation?

****

### Gatekeeper Script

> Hi Shannon. If Mike is around, would you let him know you have Kevin McKinley on the line?
> 
> He doesn't have a voicemail does he?
>
> Is it okay then, if I let you know the reason why I'm calling?
>
> I've got a company called GitLab. We enable telecom companies to release software up to 26x faster while reducing costs by as much as 30% eliminating toolchain complexity. Some clients include Comcast, Dish, and Verizon.
> 
> Does any of that sound relevant or worthwhile? 


### Voicemails

#### Research
Hey, Mike, Kevin McKinley. Listen, I’m doing a little bit of research on you and AT&T. I saw the big news about the Time Warner merger going through, and I have a couple of questions for you. Mike, call me— again, it’s Kevin, and my number is (555)-555-5555.

Hey, Stephanie, Kevin McKinley at GitLab. Listen, I’m doing a little bit of research, and I saw your presentation at DEV OPS ENTERPRISE SUMMIT 2017, and I have a couple of questions for you. Stephanie, call me— again, it’s Kevin McKinley with GitLab, and my number is 415-949-7117. 415-949-7117

#### Personal Connection

Hey {{first.name}}, this is Kevin McKinley. It’s about 4:20 on Tuesday. I had a nice conversation with {{referrer}}, and we got around to discussing you and your role at {{company}}. He thought it would be important that you and I talk. I promised him I would reach out to you. Call me at 555-555-5555, again this is Kevin McKinley, 555-555-5555.

#### Current User 

Hi {{first.name}}, this is Kevin McKinley at GitLab. Came across your name on LinkedIn where I saw that you were involved with {{title}}, and this prompted me to reach out because there are already teams at {{company}} using GitLab to deliver software 3x faster while reducing costs by 30%. I'm looking for 5 minutes of your time to discuss your objectives and share how GitLab can support you. Call me back at 555-555-5555, this is Kevin McKinley with GitLab 555-555-5555

#### jBarrows

Hi Sarah the reason for my call is one of the VPs of Sales I recently worked with told me they ran a call blitz after my prospecting training and improved their conversion rates from 5-15% while driving 25 meetings using my techniques. I was looking for 15 minutes of your time to see if that was something that impacted your priority list. Call me back at at 555-555-5555. This is Kevin McKinley with GitLab, 555-555-5555.

#### Story

Hi Gary, this is Kevin McKinley at GitLab, my number is 555-555-5555. The reason for my call is that I read about the launch of CloudForte, and this prompted me to reach out and share a story that that would be relevant to you given your role in cloud & infrastructure.
At a company dinner last week, an executive at Bank of America mentioned that they have 20 people maintaining their DevOps tools. One of our current clients replied that they only have one guy working a few hours on GitLab.
GitLab's already being used in a few teams at Unisys, but I was looking for 5 minutes of your time to discuss your toolchain and how we may be able to help you deliver solutions 26x faster, implement automation that reduces architecture overhead by 30%. 
Call me back at 555-555-5555, this is Kevin McKinley with GitLab 555-555-5555


## Asking for the Appointment

What do you do if the prospect says "No" the first time you ask for an appointment? Ask for the appointment again.

BUT, you need to ask for the appointment in a DIFFERENT way so that you're not repeating yourself and evoking the same response.

Here are a few suggested one-liners for asking for the appointment:

* Would it make sense to have a brief 5-10 min call...?
* Would you be "open-minded" to a brief, introductory call...?
* If we could help you do x... would it be worth a 3-5 minute conversation...?
* What's the best way to get on your calendar?
* Do you have a 10 min window sometime in the next week for a brief call?

The keywords here are ( make sense, open minded, worth,)

These are just a few ideas, not fully fleshed out. Please add as you come up with more methods of asking for the appointment.


### From Ryan Reisert

What should a cold call sound like?
Introduction:  Hi [First Name], this is [Name] at [Company], how are you doing?
Permission: I called to see if what we do for [Problem] can benefit your team. Did I catch you with two minutes?
Value proposition: We help [Buyer persona] who [Problem] by [Solution]. In fact, [Customer success story].
Question + leading statement: I’ve seen a lot of [Buyer persona] who are dealing with [specific facet of problem]. How are you addressing that today?
Qualify for interest + fit: [This is the part you cannot script – you have to know what makes a qualified buyer and really listen to their answers.]
Ask for the appointment: Well, you’ve been kind to give me a few minutes today and it sounds like there’s reason to continue the conversation. Do you have time this coming [Day] or [Day] that we can get into more detail and determine if there’s a mutual fit?
