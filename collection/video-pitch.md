# Video Pitch

I keep a log of the contacts I've sent a video to here: https://docs.google.com/document/d/1TTR6JX4zUSCkIxaZPXVjZov2K2yOhEFJ/

Here's the simple script I use for video.

***

Hey {{first_name}} happy Tuesday. I hope you're doing well. {{sender.first_name}} at GitLab here. The reason I'm reaching out to you is because I see on LinkedIn that you {{relevant_research}}. 

This prompted me to reach out to you because, as you may already know, GitLab is a single application for the entire software development lifecycle. We're used by clients like Nasdaq, ING Bank, and Freddie Mac to deliver software 3x faster and reduce IT costs. {{relevant_pain_point}}

Looking for a brief, 5-10 mins of your time to share how GitLab can help in your role. What's the best way to get on your calendar? 

Thanks {{first_name}}, looking forward to speaking with you.
