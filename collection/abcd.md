# Account Based Campaign Development

This is the ABCD of SDR-ing.

"I haven't even had a fucking chance to learn their names yet." - Aulus Plautius, Britannia

The days of spray-and-pray, high-volume uncustomized SDR activities are over. Buyers are inundated with messages on all mediums, so it doesn't matter if you're using Phone, Email, Social Selling, or whatever the latest fad "technique" is... what matters is that wherever you reach your buyer, your message is relevant.

If you're blasting out email generic email templates that could just as easily have been sent by Mailchimp, what value do you add as an SDR?

The work of MJ Hoffman (and jbarrows subsequently) often uses the phrase "Why You, Why You Now?"

When we're reaching out to prospects cold RELEVANCY is paramount to getting and keeping their attention.


## Call Script

*ring ring*
Hi {{first.name}}, this is Kevin McKinley with GitLab.

a) Pattern Interrupt
Can I share with you the reason why I'm calling, and then you can decide if it makes sense for us to talk further?

b) Relevant Research

c) 30 Second Commercial

d) 


## Email Template

I use a simple structure in my email templates to open up my campaigns.

1) Personal Research to Establish Relevancy
2) Why I'm Reaching out	
3) About our company
3) CTA



## Subject lines

I like to use the subject's first name in the subject line to immediately establish relevancy. Hard not to open an email that directly says your name.

For High Profile/Visibility contacts with relevant keynotes/presentations/whitepapers/etc. 
{{first.name}}, Your Presentation at {{event}}
John, your presentation at DOES 17 / John, your presentation "Augmenting the ORG for DevOps"

If you can find a published executive quote 
{{first.name}}, {{exec.name}}'s quote in {{publication}}
John, Len Hardy's quote in the Banker

For a volume, generic approach
{{first.name}}, question about {{company}}
John, question about KeyBank
