# Keynote: The Power of Nonverbal Communications | Joe Navarro | CMX Summit West 2015

https://www.youtube.com/watch?v=HRl0dvPRkSI


Steeple. - Do it high, do it strongly. Deliver your message with confidence.

Head Tilt - Shows receptivity. Cultivate curiousity towards your prospects, be receptive to whatever it is they have to say.

"How do I acheive more facetime?" - Reframe the problem as a way of increasing facetime.

When people are curious, we gravitate towards them. 

We remember negative things for about 13 years. 

Always be framed by blue.

Empathy is tactile and it's present. 


## 5 Traits of Exceptional People:

1. We must have mastery over ourselves. Plow through whatever life throws at you.

2. Be a good observer. Know what to look for. See what your needs, wants, and desires are.

3. Communicate effectively with a purpose. 

4. Act. Timely or pro-social acts that seek to achieve the needs, desires, or that which is befitting of others.

5. Comfort. Achieved by meeting or exceeding the needs, desires, or preferences of others. You want people to come to you, spend time with you, gravitate to you? Provide psychological comfort. What does this person need in the moment?

The true measure of a person is how they treat those that can do absolutely nothing for them.