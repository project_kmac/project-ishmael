# GitLab Stories:

Special thanks to Joseph Davidson for collecting these.

## GitLab on GitLab

As an example, one company reduced downtime from 32 minutes per week average to 13 minutes per week - a 61 percent improvement in availability following a migration to Google Cloud Platform using GitLab.

## Verizon: 
Been on Bitbucket for a number of years (problems with how it scales & operates with to other tools)
Looking at Github but didnt get good customer service / pricing
1k in trial and now migrating 6,000 from Bitbucket 

## Verizon Telematics: 
Has been able to make things so much faster using GitLab that they no longer need JIRA
All communication is at the merge request level now
Verizon has also set up a Dojo where they bring in teams to learn how to develop more efficiently and GitLab is part of that Dojo. 
Overall it was a great dinner and Verizon, Northwest Mutual and Goldman were all singing the praises of GitLab. 

"Some of the other features that we really loved, and we didn’t find with any other CI/CD tool, are the project management features. GitLab replaced a bunch of disparate systems for us like Jira, BitBucket, and Jenkins. GitLab provided us with a one-stop solution." - Mohammed Mehdi, Principal DevOps, Verizon


## Dell EMC:
Working to go to cloud native and was looking to incorporate Kubernetes
Dell EMC is one of the largest TFS users and are moving everything to GitLab.  They already use GitLab for their Cloud Native development but are wanting to move away from TFS.
Since then, rapid adoption has taken place & they want to replace all their other current SDLC tools

## Bank of New York Mellon: 
8,000 developers on single instance of free GitLab
They had Jenkins making 10k API calls -- cost them about $4 million of productivity per day 
They are moving all the way from Jenkins to using GitLab’s CI/CD

## Goldman Sachs:
At first, wanted to adopt a tool at scale
They knew they had to change tools when people would leave their first day after seeing how their legacy toolset looked like
Went from 1,000 people to 5,000 people on GitLab’s platform
Teams that were on releasing on a 2 week schedule // now they are releasing 6 times a day 
Goal: Every merge request in 30 minutes or less
“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure”

In regards to the NYC financial services dinner the other night both the Goldman guys and the Verizon person were going on about how GitLab has helped them deploy faster than ever thought possible. Michael Turok should be a sales person for us as he loves GitLab. He and his team leverage GitLab on the Quant side and they do a lot of data analysis and use GitLab CI to assist. 

There were also discussions about security and how giving the developers the ability to scan the code at the merge can actually help companies become much more nimble and increase security. I think everyone agreed that the developers do need to go through some short security training and I believe the Goldman team has done that training. AIG had some let's say "Mature" security folks there and it was a good conversation for them to be a part of and hear how others are approaching application security. 

Verizon has also set up a Dojo where they bring in teams to learn how to develop more efficiently and GitLab is part of that Dojo.

Overall it was a great dinner and Verizon, Northwest Mutual and Goldman were all singing the praises of GitLab. 

Oh and the Goldman investment team was there and they shared how this was the first time that they have invested in a company based on the recommendations of their own engineering team. She also talked about the GitLab transparency and how that helped them make the decision to invest in GitLab much faster than the typical investment timeline. 

“GitLab has allowed us to dramatically increase the velocity of development in our Engineering Division. We believe GitLab’s dedication to helping enterprises rapidly and effectively bring software to market will help other companies achieve the same sort of efficiencies we have seen inside Goldman Sachs. We now see some teams running and merging 1000+ CI feature branch builds a day!”
Andrew Knight
Managing Director at Goldman Sachs

“We’re bringing into the firm a platform that our engineers actually want to use – which helps drive adoption across multiple teams and increase productivity without having to ‘force’ anyone to adopt it. This is really helping to create an ecosystem where our end users are actively helping us drive towards our strategic goals - more releases, better controls, better software.”
George Grant
VP technology fellow, Goldman Sachs


## ING Bank
Had GitLab for code repo and Jenkins for the CI
Developers were using GitLab’s CI at home and went to management recommending change
Once approved, 3000 developers went to GitLab on their own will 

## Blackrock
12 engineers supporting 1,400 developers, 1 spending 25% time.
"At a recent company dinner, a Head of Risk at one of the big 4 banks mentioned that she had a team of 20 to keep SDLC tools running in her org. Whereas one of our clients at an asset management firm said, "I have 1 guy that spends 25% time to keep GitLab up and going for my team of 1500 devs"

## Jaguar Land Rover
6 builds a day to 600/day. JLR uses GitLab to achieve "blue-green deployment" for their infotainment systems.
Chris is the Head of Systems Engineering at Jaguar Land Rover. He manages the Infotainment system for their lineup of luxury cars. He had a difficult job because they weren't able to deliver software fast enough to meet the demands of the business.

Devs waited three weeks for the Ops team to add build dependencies on the build servers. This lead to feedback loops taking 4-6 weeks, deploys per day being below one, and meant that not all of their new software would actually make it to vehicles.

It was obvious to Chris that their output wasn't meeting their potential. So when he discovered two years ago that our technology would let him speed up software delivery and implement Continuous Improvement, he decided to take the risk of being the first to implement DevOps within JLR using GitLab CI.

We're now two years into this journey with Chris. Today, they are able to deploy their software directly to the cars, from build to fix, in an hour, even while the car is moving. And they can deploy 50-70x a day of each piece of individual software. This means they cut the time to get feedback on new features from six weeks to 30 minutes. Additionally, each developer now has the keys to run a build, so that for example, a JavaScript developer, who doesn't know Linux, can add his component to the build system by himself within 30 mins - instead of having to wait for Ops for 3 weeks.

Enough about Chris. Does anything of what I've just said seem relevant to your world and worthy of a brief conversation?"


Equinix	"“ Moving into a system like GitLab will help any organization or enterprise to get into DevOps methodology and continuously improve the deployment workflows to achieve quality, agility, and self-serviceability. ”
Bala Kannan, senior software engineer
"
Extra Hop	"“With GitLab, we finally had a single tool that not only aligned to divergent engineering workflows, but also allowed for meaningful continuous integration.”
Bri Hatch,
Director of IT"
University of Washington	
Cloud Native Computing foundation	"
“ GitLab takes the culture of the community and brings it to where you can actually codify how humans can interact together well. That’s difficult to capture, and I think GitLab does a really excellent job of not forcing people but really encouraging a collaborative beneficial work environment. ”
Chris McClimans,
Cross-Cloud CI Project co-founder"
Ticketmaster	Things were looking pretty scrappy for our CI pipeline only a few months ago. Now it is a whole different ballgame. If your team is looking for a way to breathe fresh life into a legacy CI pipeline, I suggest taking a look at GitLab CI. It has been a real game changer for our mobile team at Ticketmaster.  Jeff Kelsy, Android developer
Paessler AG	"""[With GitLab] The amount of effort involved in actually getting to the newest version that you’re supposed to be testing, whether you’re a developer or a QA engineer, is minimized immensely.”  QA engineer’s tasks – about an hour a day in total – have been slashed to 30 seconds, a 120x speed increase. Greg Campion,
Senior Systems Administrator"
Trek10	“Essentially, we look at GitLab as a building block, and we just build whatever we need on top of it. Whether it’s a wiki or a custom integration, GitLab helps create an engineering culture.” Jared Short, director of innovation
CERN	"The significant improvement that we’ve seen is a huge increase in the visibility of what is changing in the code. It’s become easier to track what’s being changed, by whom, and for what purpose. We now have this record of all of the questions that are asked about changes in the code, why changes are made and can more easily identify mistakes” Alexandre Lossent, Version Control Systems Service Manager, CERN IT department