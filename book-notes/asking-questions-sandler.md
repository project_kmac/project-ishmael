# Asking Questions the Sandler Way 

### UpFront Contract:

1. “So, Tom, exactly where and when would be best for us to meet?” 
2. “Let’s just agree on our top most-important items on the agenda. Is that fair?” 
3. “Just so that we make sure that we are both on the same page, let’s agree what the next steps might look like if we’re a good fit. Are you comfortable with all that?”

### Email After Setting Meeting:

Use ANOT.

Subject: Meeting: Thursday at 4:00 

Mark,

I appreciate the invitation to meet to explore [insert issue]. 

As discussed, naturally you’re going to have a bunch of questions for me, such as: [insert three or four typical issues surrounding the issue]. 

Obviously, as agreed, I’m going to need to better understand some things, such as: [insert three or four typical questions that you need to know answers to concerning the prospect’s world, experiences, and history, and the issues]. 

At the end of the meeting we will decide what the next steps will look like, and how to best proceed—even if it’s a no at this stage. If you feel that you want to add to the agenda or invite anyone else, just drop me a quick note.

Regards, 
Julie

### Opening the Meeting

“Thanks again for the invitation, Mark. How are things with you today?” Say this with a warm smile. Learn how to get your tonality and Nurturing Parent voice just right; it’s your most successful selling voice. Always use the word “invitation”; it’s very strong. Be sure you use genuine tonality with your opening gambit. If you come off as snarky or insincere, it doesn’t matter what words you use. Speak slowly, clearly, and with authority. Once Julie has listened actively to Mark’s answer and responded appropriately, the real work begins. Take a look at the below questioning structure and follow it as closely as you can. You should have prepared a pre-call plan, of course, with objectives for your call, along with appropriate probing questions (more on this later). “When we spoke on the phone,” Julie says, “we agreed that we would meet today to discuss ‘x,’ right? And we talked about how you would naturally be wanting to ask me lots of questions about how we do or deliver ‘y,’ correct? And, in order for me to be able to better see your world through my eyes, I would likely be needing to better understand ‘z,’ yes?” Julie waits for Mark’s agreement after every question, of course. “We also agreed that by the end of this meeting we would likely realize whether or not we’re a good fit, in which case either of us could say so without hurting the other’s feelings, didn’t we? But we also said that if we both believed that it would make sense to continue, we could agree to take things to the next level and agree on the next steps; are you still going to be OK with that? Finally, we said that we would meet today for up to an hour, and that we didn’t need anyone else in the room at this early stage, correct? So, before we begin, has anything changed, or do we need to think about anything else right now? Great! Are you ready to begin?”

### The Single Most Important Question to Ask

“So, Omar, in order that I can really understand some of the issues you’re facing and to try to see the world through your eyes, I’m going to be asking some specific and sometimes quite probing questions during our meeting. It will really help us figure out whether my company is the best fit for you at this stage. Does that make sense?”
