# GitLab 30 Second Commercials

As I've been working on the 30 Second Commercials lately, I've realized that not only is this an iterative process, there's a predictable evolution over time. 30 Second Commercials get better and better as you discover the most important pain points for each of the buyer personas, and integrate them into the script. 

****

There are 4 parts to a 30 Second commercial:

1. Introduction
2. Pain Statement
3. Benefit
4. Hook Question

****

### Haitch

This prompted me to reach out to you because I'm with GitLab. We're a single application for the entire software development lifecycle. We're used by clients like ING Bank, Nasdaq, and Freddie Mac to move to the cloud, generate top-line revenue, and give executives visibility into the value stream of their applications. We help with a lot of different things including tool consolidation, but our sweet spot is really if you're looking at cloud native technologies, moving to microservices, or using Kubernetes and containerization. Looking for a brief 5-10 mins of your time to share how GitLab can help {{company}}, what's the best way to get on your calendar?


### AT&T Pitch

This prompted me to reach out to you specifically because I’m with GitLab. GitLab is a single application for the entire software development lifecycle used by some teams at AT&T, and companies like Dish, Comcast, and Verizon to speed up software delivery 3x while reducing architecture costs by 30%. I’m looking for a short 10 mins of your time to share that information with you; what’s the best way to get on your calendar? 


### Jeff’s Basic Pitch

Calling to find 10 minutes to share with you how companies like {{competitors}} use GitLab to addresses shortcoming with their software development lifecycle toolchain.
Have you heard of GitLab before?
It’s the first single application for the whole SDLC. But it’s not a rip and replace.
We meet you where you are to reduce complexity and costs (administrative and licensing).
Let’s find time to discuss further next week


### Seven Stories a Salesperson Must Tell (90 Sec) 	

‘Can I share a short story about a materials manager who works less than a mile from here and who I’ve been working with for the past 18 months?’

90-second story of Ed Blackman. 

Ed is the materials manager at Elpac Electronics. Elpac manufactures power supplies. I met Ed two years ago at an Orange County, California, industry meeting. 

Ed had a very difficult job. He was experiencing massive shortages, which was impacting their production schedule. There were partially completed kits stacked all over his factory floor, and he had to deal with two bills of material for every power supply they made. Ed’s boss, the VP Manufacturing, was not meeting the shipment schedule and their backlog was out of control. To top it off, their CFO was unhappy with the cost of carrying too much inventory. To say Ed was feeling pressured is an understatement. 

It was obvious to Ed that he needed a better way to respond to major vendor delivery changes. When he discovered 18 months ago that our technology would let him pre-plan his production schedule overnight, he decided to take the risk of being the first Southern California company to implement our new materials requirements planning (MRP) system. 

We are now 18 months into the MRP implementation with Ed. His inventory turns have gone from 1.9 to almost 5. His backlog has been eliminated. They are operating off one bill-of-material, and 95 percent of shipments to customers are on time. Ed is beyond pleased.

And then I would transition to: ‘Enough about Ed. What’s going on around here?’

.....

"Can I share a short story about a {role} who works in the {industry} and whom we've been working with for two years?

Chris is the Head of Systems Engineering at Jaguar Land Rover. He manages the Infotainment system for their lineup of luxury cars. He had a difficult job because they weren't able to deliver software fast enough to meet the demands of the business.

Devs waited three weeks for the Ops team to add build dependencies on the build servers. This lead to feedback loops taking 4-6 weeks, deploys per day being below one, and meant that not all of their new software would actually make it to vehicles.

It was obvious to Chris that their output wasn't meeting their potential. So when he discovered two years ago that our technology would let him speed up software delivery and implement Continuous Improvement, he decided to take the risk of being the first to implement DevOps within JLR using GitLab CI.

We're now two years into this journey with Chris. Today, they are able to deploy their software directly to the cars, from build to fix, in an hour, even while the car is moving. And they can deploy 50-70x a day of each piece of individual software. This means they cut the time to get feedback on new features from six weeks to 30 minutes. Additionally, each developer now has the keys to run a build, so that for example, a JavaScript developer, who doesn't know Linux, can add his component to the build system by himself within 30 mins - instead of having to wait for Ops for 3 weeks.

Enough about Chris. Does anything of what I've just said seem relevant to your world and worthy of a brief conversation?"


Notes from Chris Hill's JLR DOES18 presentation:

>Change agent. Output isn't meeting potential. Read about higher performing teams/Devops. Nobody went to the same seminar. Working software trumps everything. Prove out on a small scale. 
>
>"I asked the ops team three weeks ago to add a build dependency on the build servers, and it still hasn't been added yet. I'm just going to go back to building on my own."
>
>"When somebody is promoting a tool, and this tool offers and is packed in there 10 years of software engineering experience, that we put in to this massive feature set, does my team actually acquire a free 10 years of software engineering experience?" 
>
>**Transfer the pain and get them to understand why things are the way they are, and why we've created this tool set.**




****

### Aug 6

GitLab helps over 100,000 enterprises like {{company}} deliver software 3x faster and reduce overhead by 30% through consolidating their toolchain. I spend a lot of my time speaking with {{role(VP, Director, Architect/Engineer)}} in the {{industry}} world. 

Typically, here's what they say to me: They tell me they're doing a great job of moving to DevOps, going cloud native, and modernizing their architecture... but oftentimes this can lead to having as many as 79 tools in their stack, which means spending too much time maintaining tools rather than producing code... or they'd like to automate their Continuous Integration, development testing, and deployments, spinning up environments, creating docker images, pushing to registry to code production (so they don't have to write a thousand lines of code just to spin up a couple of VMs)... 

For example, a Head of Risk at one of the big banks mentioned that she had a team of 20 to keep SDLC tools running in her org. Whereas one of our clients at an asset management firm said, "I have 1 guy that spends 25% time to keep GitLab up and going for my team of 1500 devs"

Does any of that sound relevant to your world, or even worth a brief conversation?

 

> So, Bill, I’m with (So-and-So Inc.). We’re a company that specializes in (very brief description of what you do). Bill, I spend a lot of my time talking to (Vice Presidents of Everything), typically in the (manufacturing) world. And typically, when I speak to those people, here’s what they say to me: They tell me their people are doing a good job of (A, B, and C), but they are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). Has any of that ever been relevant to your world, or even worth a brief conversation?



### Brian Wald's 3 Pain Points

GitLab has built a single application for the entire Software Development Life Cycle that enables DevOps in over 100,000 enterprises. We've been very successful in helping companies in highly regulated industries. Where we typically help {{role}} win is in automating your Continuous Integration/development testing/deployments or replacing tools that cause issues like Bamboo, Jenkins, and BitBucket. 

We've been able to help clients like Nasdaq, Ticketmaster, and Jaguar ship software 26x faster, and reduce infrastructure overhead and costs by 30%, all while helping them deliver business value.

We do an awful lot for a lot of different companies. I don't suppose those three issues that I've brought up are anything that you're experiencing?

1. Automate Continuous Integration, spinning up environments, create docker images, push to registry to code production (manage those for you). (Internal Networking Software). Automation to make your development testing or deployments better.

2. Reduce costs while increasing ROI. 

3. Increase Velocity to get products out faster. How long does it take you to deploy? How many times to deploy a year? How many times do you want to deploy?

### Version July 30

GitLab has built a single application for the entire Software Development Life Cycle that enables DevOps in over 100,000 enterprises. We've been very successful in helping F500 companies in highly regulated industries who are concerned with keeping up with business demand, disappointed with the bottlenecks in QA, Security, and IT, or frustrated with the complexity of their toolchain.

We've been able to help clients like Nasdaq, Ticketmaster, and Jaguar ship software 26x faster, and reduce infrastructure overhead and costs by 30%, all while helping them deliver business value.

We do an awful lot for a lot of different companies. I don't suppose those three issues that I've brought up are anything that you're experiencing?

****

We've been very successful in helping F500 companies achieve a shorter time to market and reduced product development and support costs through reuse, standardization, and increased engineering productivity

keeping up with the speed of business, disappointed with increasings costs, 

>  Bill, I’m (your name) with (name of your company). 
> We are a (type of company) firm that specializes in (solution you deliver). 
> We’ve been very successful in developing and implementing (brief description of product/ service) for (companies/ people) who are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). 
> We’ve been able to help our (clients/ customers) to (description of product/ service benefit, with the emphasis on “what’s in it for me” from the customer’s point of view). 
> (NOTE: If you can include a reference to some kind of “social proof” here, such as a happy customer you’ve worked with, or a trade association you take part in, that’s a plus.) 
> Does anything at all of what I’ve just said (sound like an issue for you/ seem relevant to your world/ describe something with which you are dealing)?

3 Poin Points are

Are you able to keep up with business demand
How are you dealing with the bottlenecks in QA, Security and IT Ops
How difficult is it for you to complete your annual audits for compliance


### VP/Director of Business Apps

Michael, I'm Kevin McKinley with GitLab. We are a single application for the entire software development lifecycle.

I spend a lot of my time with {{VPs of Business Unit}}, typically in the {{financial services/banking industry}}.

Here's what they typically say to me: 

a) They tell me they’ve got a great team, but sometimes can get into a little trouble keeping up with business demands, leading to missed deadlines or not shipping fast enough

b) Oftentimes they've identified that this is caused by bottlenecks in QA, Security, and IT Ops, 

c) Chances are, if you’re anything like our current clients, it can sometimes be difficult completing your annual audits for compliance.

Has any of that ever been relevant to your world, or worthy of a brief conversation?

### DevOps Director/Manager/Leadership 

Michael, I'm Kevin McKinley with GitLab. We are a single application for the entire software development lifecycle.

I spend a lot of my time with {{role}}, typically in the {{financial services/banking industry}}.

Here's what they typically say to me: 

a) They tell me they’ve got a great team, but sometimes have trouble managing the complex tool chains and integrations in the pipeline, which leads to a lot of wasted cycles or spending resources on maintenance. For example, a Head of Risk at one of the big banks mentioned that she had a team of 20 to keep SDLC tools running in her org. Whereas one of our customers said, "I have 1 guy that spends 25% time to keep GitLab up and going for my team of 1500 devs"

b) Oftentimes, it can be an uphill battle encouraging adoption of DevOps practices and trying to standardize best-practices

c) Maybe, if you’re anything like our current clients, it can be challenging introducing DevOps into a legacy infrastructure - especially if you’re growing by acquisition.

Has any of that ever been relevant to your world, or worthy of a brief conversation?


### DevOps

> So, Bill, I’m with (So-and-So Inc.). We’re a company that specializes in (very brief description of what you do). Bill, I spend a lot of my time talking to (Vice Presidents of Everything), typically in the (manufacturing) world. And typically, when I speak to those people, here’s what they say to me: They tell me their people are doing a good job of (A, B, and C), but they are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). Has any of that ever been relevant to your world, or even worth a brief conversation?


John, I'm Kevin McKinley with GitLab. We are a single application for the entire DevSecOps application lifecycle.

I spend a lot of my time with {{role}}, typically in the {{industry}}.

Typically, when I speak with these DevOps folks, here's what they say to me: 

a) They tell me they're embracing DevOps and modernizing their architecture, but their managing their toolchain can sometimes be a pain (leading to things like having to write 1000 lines of code just to spin up some VMs), 

b) Or they've got all the latest tech in their stack, using Jenkins, Kubernetes, Ansible, etc. but they spend so much overhead or manpower just maintaining the integrations. For example, a Head of Risk at one of the big banks mentioned that she had a team of 20 to keep SDLC tools running in her org. Whereas one of our customers said, "I have 1 guy that spends 25% time to keep GitLab up and going for my team of 1500 devs"

c) Chances are, if you have a toolchain like that, identity and access management is another issue, meaning getting the right people, onto all the right tools, with the right permission levels -- could be very difficult, especially in a highly regulatory industry like yours.

Has any of that ever been relevant to your world, or even worth a brief conversation?


****

### Security 

>  Bill, I’m (your name) with (name of your company). 
> We are a (type of company) firm that specializes in (solution you deliver). 
> We’ve been very successful in developing and implementing (brief description of product/ service) for (companies/ people) who are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). 
> We’ve been able to help our (clients/ customers) to (description of product/ service benefit, with the emphasis on “what’s in it for me” from the customer’s point of view). 
> (NOTE: If you can include a reference to some kind of “social proof” here, such as a happy customer you’ve worked with, or a trade association you take part in, that’s a plus.) 
> Does anything at all of what I’ve just said (sound like an issue for you/ seem relevant to your world/ describe something with which you are dealing)?


John, I'm Kevin McKinley with GitLab. We are a single application for DevSecOps.

I spend a lot of my time with {{role}}, typically in the banking world. And typically, when I speak with these people, here's what they say to me: They tell me their people are doing a great job modernizing their architecture, but their managing their toolchain can sometimes be a pain (like having to write 1000 lines of code just to spin up some VMs), or with access being a huge issue, meaning getting everyone onto all the tools, with the right permission levels.


We've been very successful in helping {{industry}} companies who are 

a) concerned with protecting production applications from security threats/risks,
b) wanting to more efficiently comply with regulations requiring application security testing and policies for license management,
c) unhappy with the costs of traditional application security tools,
d) and interested in making security a more embedded part of the software development process
e) relying on third party and open-sourced code and containers.

We've been able to help enterprises like {{relevant_companies}} increase security into their delivery pipeline, while reducing the cost to fix and identify bugs.

Does anything at all of what I've just said seem relevant to your world?




John, I'm Kevin McKinley with GitLab. We are help banks like KeyCorp with increasing visibility and security in the environment.

I spend a lot of my time with cyber security professionals, typically in the banking world, who are concerned with protecting production applications from security threats/risks, wanting to more efficiently comply with regulations/audits, and interested in making security a more embedded part of the software development process. If you're anything at all like our current clients, chances are you're facing issues with visibility, security, and access. Meaning getting the right people, onto all the right tools, with the right permission levels.

Does anything at all of what I've just said seem relevant to your world?


****

### Banks - DevOps

> So, Bill, I’m with (So-and-So Inc.). We’re a company that specializes in (very brief description of what you do). Bill, I spend a lot of my time talking to (Vice Presidents of Everything), typically in the (manufacturing) world. And typically, when I speak to those people, here’s what they say to me: They tell me their people are doing a good job of (A, B, and C), but they are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). Has any of that ever been relevant to your world, or even worth a brief conversation?


John, I'm Kevin McKinley with GitLab. We are a single application for the entire software development lifecycle/DevOps.

I spend a lot of my time with {{role}}, typically in the banking world. And typically, when I speak with these people, here's what they say to me: They tell me their people are doing a great job modernizing their architecture, but their managing their toolchain can sometimes be a pain (like having to write 1000 lines of code just to spin up some VMs), or with access being a huge issue, meaning getting everyone onto all the tools, with the right permission levels.
Has any of that ever been relevant to your world, or even worth a brief conversation?


****

### Architect 

>  Bill, I’m (your name) with (name of your company). 
> We are a (type of company) firm that specializes in (solution you deliver). 
> We’ve been very successful in developing and implementing (brief description of product/ service) for (companies/ people) who are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). 
> We’ve been able to help our (clients/ customers) to (description of product/ service benefit, with the emphasis on “what’s in it for me” from the customer’s point of view). 
> (NOTE: If you can include a reference to some kind of “social proof” here, such as a happy customer you’ve worked with, or a trade association you take part in, that’s a plus.) 
> Does anything at all of what I’ve just said (sound like an issue for you/ seem relevant to your world/ describe something with which you are dealing)?


John, I'm Kevin McKinley with GitLab. We are a single application for DevSecOps.

We've been very successful in helping {{industry}} companies who are 

a) concerned with protecting the production instance from security threats,
b) unhappy with the costs of traditional application securty tools,
c) and interested in making security a more embedded part of the software development process

We've been able to help enterprises like Nasdaq, Ticketmaster, and Verizon increase security into their delivery pipeline, and reduce costs to fix bugs.

Does anything at all of what I've just said seem relavant to your world?


*****

### Engineer 

>  Bill, I’m (your name) with (name of your company). 
> We are a (type of company) firm that specializes in (solution you deliver). 
> We’ve been very successful in developing and implementing (brief description of product/ service) for (companies/ people) who are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). 
> We’ve been able to help our (clients/ customers) to (description of product/ service benefit, with the emphasis on “what’s in it for me” from the customer’s point of view). 
> (NOTE: If you can include a reference to some kind of “social proof” here, such as a happy customer you’ve worked with, or a trade association you take part in, that’s a plus.) 
> Does anything at all of what I’ve just said (sound like an issue for you/ seem relevant to your world/ describe something with which you are dealing)?


John, I'm Kevin McKinley with GitLab. We are a single application for DevSecOps.

We've been very successful in helping {{industry}} companies who are 

a) concerned with protecting the production instance from security threats,
b) unhappy with the costs of traditional application securty tools,
c) and interested in making security a more embedded part of the software development process

We've been able to help enterprises like Nasdaq, Ticketmaster, and Verizon increase security into their delivery pipeline, and reduce costs to fix bugs.

Does anything at all of what I've just said seem relavant to your world?


*****

### Development 

>  Bill, I’m (your name) with (name of your company). 
> We are a (type of company) firm that specializes in (solution you deliver). 
> We’ve been very successful in developing and implementing (brief description of product/ service) for (companies/ people) who are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). 
> We’ve been able to help our (clients/ customers) to (description of product/ service benefit, with the emphasis on “what’s in it for me” from the customer’s point of view). 
> (NOTE: If you can include a reference to some kind of “social proof” here, such as a happy customer you’ve worked with, or a trade association you take part in, that’s a plus.) 
> Does anything at all of what I’ve just said (sound like an issue for you/ seem relevant to your world/ describe something with which you are dealing)?


John, I'm Kevin McKinley with GitLab. We are a single application for DevSecOps.

We've been very successful in helping {{industry}} companies who are 

a) concerned with protecting the production instance from security threats,
b) unhappy with the costs of traditional application securty tools,
c) and interested in making security a more embedded part of the software development process

We've been able to help enterprises like Nasdaq, Ticketmaster, and Verizon increase security into their delivery pipeline, and reduce costs to fix bugs.

Does anything at all of what I've just said seem relavant to your world?


*****



### Version A - Bullet Points 

>  Bill, I’m (your name) with (name of your company). We are a (type of company) firm that specializes in (solution you deliver). We’ve been very successful in developing and implementing (brief description of product/ service) for (companies/ people) who are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). We’ve been able to help our (clients/ customers) to (description of product/ service benefit, with the emphasis on “what’s in it for me” from the customer’s point of view). (NOTE: If you can include a reference to some kind of “social proof” here, such as a happy customer you’ve worked with, or a trade association you take part in, that’s a plus.) Does anything at all of what I’ve just said (sound like an issue for you/ seem relevant to your world/ describe something with which you are dealing)?

John, I'm Kevin McKinley with GitLab. We are a single application for the entire software development lifecycle/DevSecOps.

We've been very successful in helping {{industry}} companies who are concerned with the costs/overhead associated with their toolchain, unhappy with the lack of visibility in their delivery pipeline, and disappointed with their current implementation of DevOps.

We've been able to help enterprises like {{relevant_companies}} deliver software/solutions 26x faster, reduce toolchain costs by 30%, and increase security and visibility into their delivery pipeline. 

Does anything at all of what I've just said seem relavant to your world?


### Version B - Storytelling

> So, Bill, I’m with (So-and-So Inc.). We’re a company that specializes in (very brief description of what you do). Bill, I spend a lot of my time talking to (Vice Presidents of Everything), typically in the (manufacturing) world. And typically, when I speak to those people, here’s what they say to me: They tell me their people are doing a good job of (A, B, and C), but they are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). Has any of that ever been relevant to your world, or even worth a brief conversation?

John, I'm with GitLab. We are a single application for the entire software development lifecycle/DevSecOps. 

John, I spend a lot of my time talking with {{role}}, typically in the {{industry}} world. And typically, when I speak with them, here's what they say to me:

They are concerned with the costs/overhead associated with their toolchain, unhappy with the lack of visibility in their delivery pipeline, and disappointed with their current implementation of DevOps.

Has any of that ever been relevant to your world, or even worth a brief conversation? 



### Version C - Features & Benefits

> I'm Mark Smith from ABC Company. We are a computer application development firm specializing in custom-designed inventory management programs for manufacturing and distribution operations. We’ve been very successful in developing and implementing systems for companies that are concerned about the costs associated with inaccurate inventory counts, unhappy with paperwork bottlenecks that slow down the order fulfillment process, or disappointed by the amount of time it takes to reconcile purchasing, invoicing, and shipping documents. We’ve been able to help customers like XYZ Company substantially improve their ability to track, process, and account for inventory while eliminating redundant paperwork and speeding up the accounting process. Does anything at all of what I’ve just said seem relevant to you?

I'm Kevin McKinley from GitLab. We are a single application for the entire software development lifecycle/DevSecOps. 

We've been very successful in developing and implementing DevOps for companies that are concerned with the costs/overhead associated with their toolchain, unhappy with the lack of visibility in their delivery pipeline, and disappointed with their current implementation of DevOps. 

We've been able to help enterprises like {{relevant_companies}} speed up software delivery 26x, reduce toolchain costs by 30%, and increase security and visibility into their delivery pipeline. 

Does any of what I've just said describe something with which you are dealing?



