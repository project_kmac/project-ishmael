# Call Assessment

This is a methodology for how I judge my cold calls.

## Call Outcomes

Calls are NOT binary. They're not good or bad based on whether or not you got the meeting. There are a multitude of factors as to why someone may or may not book a meeting with you today. 

There are only a handful of possible call outcomes.

Yes. You got the meeting.

Not now. Wrong timing.

Not me. Wrong person.

Not interested. Wrong messaging.

No. Wrong solution. (You had the right timing, the right person, and the right messaging... but your product doesn't fit.)

We assess the success of a call based on the Proccess and Methodology rather than the Outcome. (Meaning, did you follow the process, did you follow the methodology, was the call "Textbook"?)


## Three Call methodologies:

* Pattern Interrupt -> Up Front Contract -> Earn the Right -> 30 Sec Commercial -> Fishing for Pain -> Close Appt -> Set Agenda -> Post Sell

* Earn the Right -> Analyze the Situation -> Create the Need -> Sell the Solution -> Close for Next Step

* Introduction -> Reason -> Qualify -> Pitch -> Close Meeting


Here's what we'll judge on:

1. Tonality
2. Earn the Right
3. 30 Sec Commercial
4. Fish for Pain
5. Clear CTA
6. Set Agenda
7. Post Sell
8. Textbook Objection Handling

### Tonality

Do you sound like you belong, or do you sound like a salesperson?

The Goal here is to have tonality that ensures you make it through their mental "spam filter.

### Earn the Right

Did you do your research and present a specific "Why You, Why Now?"

The Goal here is to earn the right to continue the phone conversation.

### 30 Second Commercial

Did you present GitLab concisely in a way that was relevant to that prospect? For example: a) did you reference an existing customer that's in the same industry? b) If the prospect is in infrastructure, did you reference a case study of success with infrastructure?

The Goal here is to present in a way that is RELEVANT. (Concision is not necessary, but there's a certain elegance to being able to present concisely.)

### Fishing for Pain / Qualifying

Did you ask questions that uncovered where their pain points are? For example:

* How are you currently handling your CI/CD pipeline? 
* Have you ever had downtime because of a broken integration? 
* If we could help you speed up your software delivery cycles, would you be interested in talking in more detail?

The Goal here is to uncover a pain point that GitLab solves.

### Close for the Appointment

Did you ask for the appointment? Did you ask for the appointment multiple times in different ways? 

Here are a few suggested one-liners for asking for the appointment:

* Would it make sense to have a brief 5-10 min call...?
* Would you be "open-minded" to a brief, introductory call...?
* If we could help you do x... would it be worth a 3-5 minute conversation...?
* What's the best way to get on your calendar?
* Do you have a 10 min window sometime in the next week for a brief call?

The Goal here is to have a clear Call to Action (and to try 4x before disengaging).

### Set Agenda

Did you set expectations and agenda for the upcoming call? Did you ask/verify "What's top of mind for you? Any specific areas you'd like us to focus on our call?"

The Goal here is to have a clear agenda and expectations for the upcoming call.

### Post Sell

Did you confirm the details of the call? Did you confirm the correct email address for sending the calendar invite?

The Goal here is to be thorough and pay attention to detail so that we REDUCE NO-SHOWS.

### Objection Handling

As you may have noticed, I place objection handling last. Here's why. IF you're able to find the right "Why You, Why Now?" reason, and pitch a relevant and concise "30 Second Commercial", then there shouldn't be any objections to handle. Many of my calls don't have objection handling, because I did the up front work and presented with relevancy. The calls where I have to handle objections are typically when I didn't have enough information/relevance or a good enough reason. So, objection handling should be the outlier rather than the norm.

That being said, if you are faced with an objection, you should know how to handle it correctly so as to move the conversation forward.

The Goal here is not to answer their question directly, but to Aikido and move the conversation forward. To that end, did you handle the objection in a textbook manner?


