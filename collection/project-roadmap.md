# Project Roadmap

Things that need to be built into this project.

Please contribute wherever you are able.

* Event Invite - needs improvement. If you've had major success with leveraging events, please add
* Prospects Added - (Conversations is our goal, but the bottleneck for having quality conversations is "prospects added to pipeline". Whomever can crack the code of consistently adding 50+ prospects/day will dominate the leaderboards) -> This is now the LRR 
* ISS for DevSecOps - (Top Secret Project)
* HYDRA for LinkedIn - (When released, this will allow SDRs to generate leads passively)
* Nurture Campaigns - This is a MOF (Middle of Funnel) sequence that will warm up leads for re-engagement and new meetings