meet on Wed April 17th at 7th Avenue in New York?


Hello Eric,

{{first_name}}, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

I saw on your profile that you led the DevSecOps cultural transformation for BofA and thought you would enjoy our complimentary workshop. As you may already know, there are teams at BofA using our application, GitLab.

We're hosting this at Convene on 7th Ave in NYC on behalf of a customer to facilitate a "real world" conversation around DevOps transformation.

Executives from Goldman Sachs, Northwestern Mutual, Guardian Life, and Bank of America will be in attendance. Northwestern Mutual will be sharing their story of Automated Compliance in CI/CD and Kubernetes for Enterprise.

The event is free, although space is limited and it is by registration only. Food and drinks will be provided for the evening.

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You probably wouldn't be interested in joining us for this workshop, would you?

Warm Regards,
Kevin

*****

Hello Francesco,

Please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

I saw on your LinkedIn profile that you're responsible for the investments division and thought you might enjoy our complimentary workshop. As you may already know, GitLab is used by Goldman Sachs as part of their SDLC - and we've been working with MetLife's Cornerstone SDA as well.

We're hosting this at Convene on 7th Ave in NYC on behalf of a customer to facilitate a "real world" conversation around digital transformation in banking.

Executives from Goldman Sachs, Northwestern Mutual, Guardian Life, and Bank of America will be in attendance. Northwestern Mutual will be sharing their story of Automated Compliance in CI/CD and Kubernetes for Enterprise.

The event is free, although space is limited and it is by registration only. Food and drinks will be provided for the evening.

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You probably wouldn't be interested in joining us for this workshop, would you?

Warm Regards,
Kevin


*****
   12:00 PM Lunch 
   12:45 PM Welcome from GitLab - John May 
   1:00 PM  Automated Compliance in CI/CD, Ravi Devineni & Michael Pereira, Northwestern Mutual
   1:30 PM  Verizon - Innersourcing
   2:00 PM  Tool Chain Complexity - Joe Dunn -  Director of Infrastructure Architecture , Guardian Life 
   2:20 PM  GitLab 11.9 Demo - REB 
   2:45 PM  Running Gitlab in Kubernetes for an Enterprise,  Sean Corkum, Northwestern Mutual 
   3:15 PM  Goldman Sachs - Case Study
   3:45 PM  Panel Discussion led by Kim Lock 
   4:15 PM  Break for Happy Hour with 2 demo stations available.  
   
  



