# Messaging Matrix

This document will HAVE to evolve. There are two arguments against standardization of messaging. 1) By the time you're reading this, new features, new releases, and the increased scope of what GitLab offers will have grown far beyond what I'm able to articulate. 2) Additionally, each territory and named account will likely have different focal points, so as to make a common language less possible. 

So rather than attempt to provide a complete and finished product (i.e. a comprehensive Messaging Matrix that has standardized messaging for each persona and scenario) please use this as a skeleton, a framework to get started.




| Project Management / Business Analyst  	|  Application 	Development|  Quality Assurance / Testing 	|   Infrastructure/IT Ops/SRE/Build & Release	|   	|   Security	|   Data Center / Networking	|   Database	| Ecommerce |
|---	|---	|---	|---	|---	|---	|---	|---	|
|   	|   	|   	|   	|   	|   	|   	|   	|
|   	|   	|   	|   	|   	|   	|   	|   	|
|   	|   	|   	|   	|   	|   	|   	|   	|



"Okay, so tell me... what's not working with your pipeline/toolchain?"

**** 

DT: open-core _(ie. product velocity)_, gitlab transparency _(ie. user collaboration on issues)_, and a single conversation for all stakeholders focused on cycle time

****

## From handbook/ceo/pricing

| Self-managed tier | Core | Starter | Premium | Ultimate |
| GitLab.com | Free | Bronze | Silver | Gold |
| Per user per month | $0 | $4 | $19 | $99 |
| [Buyer](https://about.gitlab.com/handbook/marketing/product-marketing/#buyer-personas) | Individual Contributors | Manager | Director | Executive |
| Helps with | entire DevOps lifecycle | Prioritization, Automation | Advanced Kubernetes management, Progressive delivery, Availability | Value streams, Risk, Compliance, Security, Governance |
| Main competitor | None | Atlassian | GitHub | Collabnet |
| Type of sell | No sell | Feature | Benefit/solution | Transformation |

### Type of sell

- A feature sell means that people want to buy the extra features. This can be done self-serve.
- A benefit sell means that people buy the business outcomes that come with fully utilizing GitLab. You need case studies, metrics like the [conversational development index](https://docs.gitlab.com/ee/user/admin_area/monitoring/convdev.html), and a quarterly checkin with a technical account manager from customer success to review the status of the adoption plan. A competitive process can include a bake-off to show people are 10x faster in starting new projects with GitLab.
- A transformation sell means that people want to transform as an organization. They want to reduce cycle time with 10x and want us to help them. We do workshops with transformation consultants, and define a complete shared project.


****

