# Pre-Event

Need different CTAs based on goal (meeting at the booth, call, etc.)

## Evanta CIO/CISO Summit

subject: meet at Evanta Summit on Tuesday?

Hello Frank,

Noticed you'll be at the Evanta Summit in Charlotte on Tuesday. Reaching out because we will be there as well, and I'd like to share with you how we can help with your InfoSec initiatives.

GitLab is a single application for the entire DevSecOps lifecycle. Clients like GE use GitLab to enable better quality and improved security by embedding testing, QA and security scans into every pipeline.

Looking for an introductory 10 min meet to share how GitLab can help with your objectives. What's the best way to get on your calendar?

Warm Regards,
Kevin

