# Buyer Personas and Pain Points

This doc is about the common pains experienced by our typical buyer personas. What are the relevant problems that the buyer persona is unhappy/disappointed/concerned with? This way, when we're live with a prospect, we can tailor our messaging to what's most useful to that persona.

ToolChain Complexity = Brittle Integrations
Visibility ?
Speed of Software Development

## SDR Persona Value Props

https://docs.google.com/document/d/1ziTNHnudOF49XsPczmM0gekIaHyprpKGcveuPaV3Iv0/edit

At GitLab, leads typically fall into one of three roles/functions:

* Executive
* Director
* Architect/Engineer

### Executive

The executives are the CIOs, CTOs, and VPs who are focused on the big picture and strategy of their respective Business Unit (BU)

Their pain points typically revolve around high level, business value pains such as:

	* Keeping up with business demand and speed to market
	* Resolving bottlenecks
	* Complying with annual audits
	* Going fast enough to compete against the market
	


### Directors

Directors are a little more hands on than executives

Their pain points typically revolve around 

	* managing the complex tool chain and integrations
	* encouraging adoption of DevOps practices
	* dealing with the challenges of introducing DevOps into a legacy infrastructure

### Architects/Engineers

Architects and Engineers are boots-in-the-ground, in-the-weeds type folks who are excecuting on a day to day basis the projects that come from above.

Their pain points typically revolve around: 

	* Having to write 1000 lines of code just to spin up some VMs
	* Implementing cloud native architecture of containerization
	* Using Kubernetes

#### Architecture

John, I'm Kevin McKinley with GitLab. We are a single application for the entire software development lifecycle/DevSecOps.

We've been very successful in helping {{industry}} companies who are concerned with the costs/overhead associated with their toolchain, unhappy with the lack of visibility in their delivery pipeline, and disappointed with their current implementation of DevOps.

We've been able to help enterprises like Nasdaq, Ticketmaster, and Verizon deliver software/solutions 26x faster, reduce toolchain costs by 30%, and increase security and visibility into their delivery pipeline. 

Does anything at all of what I've just said seem relavant to your world?


I spend a lot of time with architects, and typically, when I speak with them they tell me that they are very happy with their world class dev team. But they tell me they can sometimes have concerns about the legacy stack their team is using, which causes a slow product release cycle or delays in project delivery. 

Or maybe they do have a modern software suite (Jira, BitBucket, Bamboo, Jenkins, etc.) and are mostly happy with it. But they occasionally worry about having too many moving parts, too many environment variables/config files/duplication, and as a result have to spend a lot of resource overhead managing the dependencies of the different services, their versions, and how they relate to each other.

Often, they've even got a pretty good core tool set. But perhaps they're not cloud native and the organization hasn't fully embraced automation yet. And as a result there's too much manual work (for example developers build their apps, use use Docker Compose. DevOps then converts manually and generates Cloud Formation and apply it to a stack that Devs have a hard time getting to.)

#### Application Development 

GitLab is the first single application for software development, security, and operations that enables complete DevOps, making the software lifecycle 3-10x faster and radically improving the speed of business.

#### Cloud

GitLab is optimized for cloud native. It’s the fastest way to build + deliver #cloud native apps. The integration with #Kubernetes, along with our CI/CD fully integrated can't be beat. 

#### DevOps

GitLab is the only application that handles every aspect of DevOps under 1 UI which gets rid of the bottlenecks that comes with a traditional 7+ tool chain. We also have the number 1 CI/CD tool in the industry ranked by Forrester. We can handle the whole DevOps chain or piece by piece

#### Frontend

#### Infrastructure

#### IT Operations 

GitLab is a single application for the software development lifecycle that allows development teams and operations to collaborate from planning to monitoring, with features like cycle analytics to improve operational efficiency. 

#### Software Development/Engineering



#### Security 

Security testing solutions are expensive and most teams wait until staging or production to test, creating bottlenecks and late-stage vulnerabilities. With built-in application security testing at the code level, our value is in shifting security to the left and fixing a vulnerability before a breach can save large companies hundred of millions of dollars.




## 30-Second Commercial

#### Shorter Version (170 Words)

David, I'm with GitLab, we provide a single application for the complete DevOps lifecycle.

I spend a lot of my time speaking with DevOps Directors and typically, here's what they say to me: they tell me they've got a great software engineering team in place, but often they're using legacy software with limited functionality like Perforce/SVN, which means they're releasing products only 2x a year.

Other times, they have a modern tech stack, but maybe they're using a daisy chain of tools which may have brittle integrations that means they spend a lot of time maintaining the different services.

The other common thread I see is that they're concerned about visibility into their toolchain, which isn't easy when you're running on 7+ tools (each with their own UI, users, and permission sets). And this means that triaging and troubleshooting a "small" bug can take weeks.

Does anything at all of what I’ve just said seem relevant to your world, and is worthy of a 3-5 minute conversation?


#### Longer Version (250 Words)

David, I'm with GitLab, a technology company that provides a single application for the complete DevOps lifecycle.

David, I spend a lot of my time speaking with [DevOps Directors, Chief Architects, VP of IT], and typically, when I speak with these people, here's what they say to me: they tell me they've got a great software engineering team in place, but often they're using an obsolete tech stack or legacy software like Perforce/SVN that has limited functionality, which hampers productivity and means they're releasing product twice a year and getting left in their competitor's dust.

Or maybe they do have a modern tech stack and invested a lot in their tool chain, but they're using a daisy chain of tools (typically, GitHub Enterprise/Bitbucket, Jira, Jenkins for CI/CD, some sort of security, etc.)
which have brittle integrations and may mean spending a lot of time maintaining/managing dependencies of the different services and how they relate to each other.

The other common thread I see is that they're concerned about visibility into their toolchain, which isn't easy when you're running on 7+ tools in your stack (each with their own UI and their own permission sets). And this means that triaging and debugging a "small" bug can take weeks.

Does anything at all of what I’ve just said (sound like an issue for you/ seem relevant to your world/ describe something with which you are dealing), and is worthy of a 3-5 minute conversation?


### Pain Points for Chief Architect / Head of DevOps

Organizational:
1) Standardization - Getting every team to work together using common tools/practices/etc. 
2) Adoption - Getting devs onboard to change habits and adopt new tools
3) Human Resources - Lack of people with the right technical skill set

Technical:
1) Complexity - Too many moving parts (DevOps daisy chain = too many environments/variables, or managing dependencies/versions of services within stack)
2) Automation - too much manual work (e.g. devs build app, use Docker Compose. DevOps then converts manually and generates Cloud Formation. Devs have a hard time getting to that stack.)
3) Visibility - Developers work with their own version that ignores the shared services which can lead to...
4) Troubleshooting - Problems are hard to diagnose because there's not enough visibility, tracking, or information, leading to "small" bugs taking weeks to troubleshoot

TYPE A
David , I’m with GitLab, the first single application for DevOps. David, I spend a lot of my time talking to Chief Architects, typically in the [Technology, Finance, Communications] world. And typically, when I speak to those people, here’s what they say to me: They tell me their people are doing a good job of (A,B,C), but they are concerned about embracing automation/going cloud native, disappointed with visibility, or unhappy with their legacy software. Has any of that ever been relevant to your world, or even worth a brief conversation?


TYPE B
"Typically, when I talk with a Chief Architect / Head of DevOps 

(Stroke) They are very happy with their development team/have a world class dev team
(Pain) But they tell me they're concerned about the legacy applications their team is using...
(Impact), and as a result their product release cycle is slow and outdated/they're releasing 2x a year instead of 12x a year, or there are constant delays in project delivery."

(Stroke) Or maybe they do have a modern software suite and are mostly happy with it,
(Pain) But they occassionally worry about having too many moving parts, too many environment variables/config files/duplication
(Impact) And as a result have to spend a lot of resource overhead managing the dependencies of the different services, their versions, and how they relate to each other.

(Stroke) Often, they've even got a pretty good core tool set.
(Pain) But they're not cloud native and the organization hasn't fully embraced automation yet
(Impact) And as a result there's too much manual work (for example developers build their apps, use use Docker Compose. DevOps then converts manually and generates Cloud Formation and apply it to a stach that Devs have a hard time getting to.)

"Dave let me ask you, any of that ever been an issue worth a 3-5 minute conversation?"
"If you had to pick one, where would you start?"
"When you say it's... Tell me a little bit more about that."
"Can you help me for a second, I want to make sure I can see the world from your point of view. Can you give me kind of a real life example?"
"And so because of that, what happens?"


### Pain Points for Head of IT / Director of IT / VP of IT

TYPE A
David , I’m with GitLab, the first single application for DevOps. David, I spend a lot of my time talking to Head/Director/VPs of IT, typically in the [Technology, Finance, Communications] world. And typically, when I speak to those people, here’s what they say to me: They tell me their people are doing a good job of (A,B,C), but they are concerned about embracing automation/going cloud native, disappointed with visibility, or unhappy with their legacy software. Has any of that ever been relevant to your world, or even worth a brief conversation?


TYPE B
Typically, when I talk with a Head of IT, 

1
(Stroke) they've got a modern suite software in their organization. 
(Pain) But they're concerned about security breaches because of devs bringing in their own tools without approval. 
(Impact) And as a result, there's this impending feeling of doom that a security breach is not an IF, but a WHEN. 

2
(Stroke) Maybe they even have state of the art security in place, 
(Pain) but because of the daisy chain of tools they're using, they occasionally worry about running over budget with licensing costs. 
(Impact) Which isn't something you want to have to report to your board of directors.

3
(Stroke) Or maybe we are do have the risk and budget under control...
(Pain) But there's a lack of visibility/tracking within the tools, 
(Impact) and this means problems are hard to diagnose and troubleshooting a "small" bug can take weeks and create expensive downtime.

"Dave let me ask you, any of that ever been an issue worth a 3-5 minute conversation?"

"If you had to pick one, where would you start?"

"When you say it's... Tell me a little bit more about that."

"Can you help me for a second, I want to make sure I can see the world from your point of view. Can you give me kind of a real life example?"

"And so because of that, what happens?"


### Pain Points for DevOps Director, DevOps Lead

TYPE A
David , I’m with GitLab, the first single application for DevOps. David, I spend a lot of my time talking to Head/Director/VPs of IT, typically in the [Technology, Finance, Communications] world. And typically, when I speak to those people, here’s what they say to me: They tell me their people are doing a good job of (A,B,C), but they are concerned about embracing automation/going cloud native, disappointed with visibility, or unhappy with their legacy software. Has any of that ever been relevant to your world, or even worth a brief conversation?


TYPE B
"Typically, when I talk with a Director of Dev Ops

(Stroke) They are very happy with...
(Pain) But they tell me they occassionally worry about...
(Impact) And as a result..."