# New Opportunities

When a new opp is created.

Deprecated in favour of Dragon Team version.

### Opp Template for SFDC 

Date connected: 
Name: 
Title:  
LinkedIn:  

Call prep notes. 

* VALUE DRIVER PATH?

* PAIN (Before/Negative Outcomes):

* DREAM (After/PBO)

* IMPORTANT: (Required Capabilities/Metrics)

* LOGISTICS:
- Tech Stack?
- Number of Users?
- Business Use Case?
- How are you going to migrate or adopt?



### Calendar Invite 

Hello Tim, 

Thank you for your response. I appreciate your invitation for a brief discussion on Fri, Sep 6 @ 4:00 PM EST. Kindly RSVP  to confirm that you've received this invite.

It's my understanding from my conversation with Jon Gibson that issues such as a) shifting away from HP Micro Focus, b) integrating QA with DevOps, and c) reducing time for manual QE testing may be of importance to you.

However, if you feel there are other more pressing topics we should cover during our 30 minute discussion, please advise and I am happy to accommodate.

Naturally you may have some questions for us, such as: how is GitLab relevant to your initiatives, how well we can integrate with your stack, etc..  

Occasionally, we'll to need to have some context in order to answer those questions, like: what are your current strategic initiatives, what's in your toolchain, etc. So we'll also have a few questions for you.

Together, at the end of the brief call we can work out what the next steps will look like, and how to best proceed—even if it’s a no at this stage. Please feel free to add to the agenda or invite anyone else.

Also on the call will be your enterprise team supporting MetLife, Sean Billow (Strategic Account Leader) and Josh Rector (Solution Architect). 

Warm Regards,
Kevin

P.S. I recommend downloading the Zoom app prior to ensure that it goes smoothly. If you experience any issues, I can dial you in. 

****




Hello Sailaja, 

Thank you for our call today. I appreciate your invitation for a brief discussion on Mon, May 13 @ 4:00 PM EST. Kindly RSVP  to confirm that you've received this invite.

You specifically mentioned that you are currently planning your roadmap for Continuous Delivery and that in order to get there certain aspects need to be automated first. We will be sure to address this on our call.

GitLab is an "all-in-one" DevOps tool that helps you achieve CD and automation. Some of the features that our clients get the most use out of is the GitLab CI/CD, integration with Kubernetes, and automated security and code analysis with SAST/DAST. (Here's a list of products we compete against: https://about.gitlab.com/comparison/)

Without having to "rip-and-replace" your current tools, this approach helps you reduce licensing costs and administrative overhead from the typical Atlassian/TFS stack. (Many of our clients have found GitLab to be less expensive than their current tool set: https://about.gitlab.com/roi/replace/)

Here's a video demo going over the capabilities of what you can do with GitLab (Specifically worth a watch for you with regards to automating your continuous delivery):
https://www.youtube.com/watch?v=4Uo_QP9rSGM)

Naturally you’ll have some questions for us, such as: how is GitLab relevant to your initiatives, how well we can integrate with your stack, etc.. 

Obviously, we'll to need to have some context in order to answer those questions, like: what are your current strategic initiatives, what's in your toolchain, etc. So we'll also have a few questions for you.

Together, at the end of the brief call we can work out what the next steps will look like, and how to best proceed—even if it’s a no at this stage. Please feel free to add to the agenda or invite anyone else.

Also on the call will be your enterprise team supporting Jeld-Wen, Sean Billow (Strategic Account Leader) and Josh Rector (Solution Architect). 

Included in this call from your your enterprise team supporting Freddie Mac will be Larry Biegel (Strategic Account Leader).


******

Hello Rob,

Thank you for taking my call earlier today. I appreciate your invitation for a call on Thursday, Aug 16 @ 3:30 PM PST, and requesting more information about GitLab.

GitLab is a single application for the whole software development lifecycle, (but it’s not a rip and replace). We integrate with your existing tool stack and meet you where you are to reduce administrative complexity as well as licensing costs. Clients like Dish, Comcast, and Verizon use GitLab to speed up software delivery 3x while reducing overhead 30% and automating their CI.

Some of the features that our clients get the most use out of is GitLab Container Registry, CI/CD, integration with Kubernetes, automated security and testing at the code level, as well as performance metrics for deployed apps.

Naturally you’re going to have a bunch of questions for me, such as: what can GitLab do for you at AT&T, how well we can integrate with your stack, the benefits of GitLab for CI, etc.. 

Obviously, we're going to need to better understand some things, such as: what are your current strategic initiatives, what's in your toolchain, etc. So we'll also have a few questions for you.

At the end of the brief call we will decide what the next steps will look like, and how to best proceed—even if it’s a no at this stage. If you feel that you want to add to the agenda or invite anyone else, just drop me a quick note.

Also on the call will be Sean Billow (SAL) and Brian Wald (Solutions Architect). 

Proposed Agenda:
1. Mutual Introductions
2. AT&T's current SDLC toolchain / DevOps ecosystem
4. Overview of GitLab
5. Questions/next steps

Please feel free to add to the agenda as you see fit to better use our time together.

Warmest regards,
{{sender.first_name}}

{{insert_zoom_invite}}


*****

Hello Kumar,

Thank you for taking my call today. I appreciate your invitation for a brief call on Friday, Oct 12 @ 3:00 PM CDT.

GitLab is a single application for the whole software development lifecycle, (but it’s not a rip and replace). We integrate with your existing tool stack and meet you where you are to reduce administrative complexity as well as licensing costs. 

Clients like Nasdaq, ING Bank, and Freddie Mac (as well as some teams at CNA) use GitLab to modernize their applications, move to the cloud, and deliver software faster while reducing their IT costs.

Some of the features that our clients get the most use out of is GitLab's CI/CD pipeline, orchestration of Kubernetes clusters, automated security and testing at the code level (SAST/DAST/Container & Dependency Scanning), as well as performance metrics for deployed apps.

Naturally you’re going to have a bunch of questions for me, such as: what can GitLab do for you at CNA, how well we can integrate with your stack, the benefits of GitLab for CI, etc.. 

Obviously, we're going to need to better understand some things, such as: what are your current strategic initiatives, what's in your toolchain, etc. So we'll also have a few questions for you.

At the end of the brief call we will decide what the next steps will look like, and how to best proceed—even if it’s a no at this stage. If you feel that you want to add to the agenda or invite anyone else, just drop me a quick note.

Also on the call will be Sean Billow (SAL) and Dave Astor (Solutions Architect). 
Proposed Agenda:
1. Mutual Introductions
2. Your current toolchain/ecosystem
4. Overview of GitLab
5. Questions/next steps

Please feel free to add to the agenda as you see fit to better use our time together.

Warmest regards,
Kevin

****

Hello Don,

Thank you for taking my call today. I appreciate your invitation for a brief call on Friday, Mar 29 @ 1:00 PM EST.

You specifically mentioned the Demo Infrastructure and Benchmarking Software and we will be sure to address this during our call.


Naturally you’ll have some questions for us, such as: how is GitLab relevant to your benchmarking and demo infrastructure, how well we can integrate with your stack, etc.. 

Obviously, we'll to need to have some context in order to answer those questions, like: what are your current strategic initiatives, what's in your toolchain, etc. So we'll also have a few questions for you.

Together, at the end of the brief call we can work out what the next steps will look like, and how to best proceed—even if it’s a no at this stage. Please feel free to add to the agenda or invite anyone else.

Also on the call will be your enterprise team supporting SAS, Sean Billow (Strategic Account Leader), Josh Rector (Solution Architect), and Dave Astor (Solutions Architect). 


Proposed Agenda:
1. Mutual Introductions
2. Your current toolchain/ecosystem
4. Overview of GitLab
5. Questions/next steps

Please feel free to add to the agenda as you see fit to better use our time together.

Warmest regards,
Kevin


### POST IQM

Hello Robb and Hector,

Thank you for our brief chat today. It was an absolute pleasure meeting you both, and I appreciate your invitation for a follow on call for Thur Sep 6 @ 3:00 PM PST, and requesting more information about GitLab.

You specifically mentioned testing automation, bringing legacy apps into a flexible architecture, and consolidating your toolchain. 

GitLab can help you achieve all of that while reducing licensing costs and administrative overhead to free up some of your budget. (Many of our clients have found GitLab to be less expensive than their current stack: https://about.gitlab.com/roi/replace/)

Some of the features that our clients get the most use out of is GitLab Container Registry, CI/CD, integration with Kubernetes, automated security and testing at the code level, as well as performance metrics for deployed apps. (Here's a list of features: https://about.gitlab.com/comparison/

Here's a video demo going over the capabilities of what you can do with GitLab (Specifically worth a watch for you with regards to consolidation):
https://www.youtube.com/watch?v=4Uo_QP9rSGM

Looking forward to connecting with you again on Sept 6. I've cc'ed Sean and Brian should you have any questions between now and then.

Warmest Regards,
Kevin


****

### Setting up a Demo

Hello Sandy and Wes,

Thank you for our call earlier today. We appreciate your invitation for a call on Thursday, Oct 11 @ 12:30 PM - 2:00 PM CDT, and requesting a demo of GitLab.

GitLab is a single application for the whole software development lifecycle, (but it’s not a rip and replace). We integrate with your existing tool stack and meet you where you are to reduce administrative complexity as well as licensing costs. Clients like ING Bank, Freddie Mac, and Goldman Sachs use GitLab to speed up software delivery 3x while reducing overhead 30% and automating their CI.

Some of the features that our clients get the most use out of is GitLab Container Registry, CI/CD, integration with Kubernetes, automated security and testing at the code level, Value Stream Management, as well as performance metrics for deployed apps.

Naturally you’re going to have a bunch of questions for me, such as: what can GitLab do for you at Regions, how well we can integrate with your stack, the benefits of GitLab for CI, etc.. 

Obviously, we're going to need to better understand some things, such as: what are your current strategic initiatives, what's in your toolchain, etc. So we'll also have a few questions for you.

At the end of the demo we will decide what the next steps will look like, and whether to proceed to a POC—even if it’s a no at this stage. If you feel that you want to add to the agenda or invite anyone else, just drop me a quick note.

Also on the call will be Sean Billow (SAL) and David Astor (Solutions Architect). 

Proposed Agenda:
1. Recap
2. Sean's Presentation
3. Brian's Demo
4. Questions/Next Steps

Please feel free to add to the agenda as you see fit to better use our time together.

Warm Regards,
Kevin


****

Hello Janet and Dexter,

Thank you for our call today. It was an absolute pleasure meeting you both, and we appreciate your invitation for a demo session on Friday, April 12 @ 1:30 PM EST.

To ensure that your teams get a lot of value out of this call, we'll plan to make this interactive and tailored to your situations. You specifically mentioned using UCD, Sonarqube, TFS, and Azure, so we'll be sure to address integrations and feature parity.

As a refresher, GitLab is an "all-in-one" DevOps tool. This helps you reduce licensing costs and administrative overhead from the typical Atlassian/TFS stack. (Many of our clients have found GitLab to be less expensive than their current tool set: https://about.gitlab.com/roi/replace/)

Some of the features that our clients get the most use out of is the GitLab dashboard's metrics, automated security and code analysis with SAST/DAST, Container Registry, CI/CD, integration with Kubernetes, as well as performance metrics for deployed apps. (Here's a list of products we compete against: https://about.gitlab.com/comparison/

Here's a video demo going over the capabilities of what you can do with GitLab (Specifically worth a watch for you with):
https://www.youtube.com/watch?v=4Uo_QP9rSGM)

Proposed Agenda:
Mutual Introductions
Recap from Previous call
GitLab Co Overview
GitLab Product Walkthrough
Time Reserved for Questions
Next Steps


Naturally, throughout the demo, you’ll have some questions for us. Please feel free to interrupt to ensure that we're able to address any ideas or concerns.

Obviously, we'll to need to have some context in order to answer those questions, so we'll probably have more questions in return.

Together, at the end of this demo, product walkthrough, and discussion, we'll have a pretty good idea of what the next steps look like and how to best proceed. (Typically, that's an on-site visit or choosing a project to start a small proof of concept.) 

What we'd like to suggest is that before ending the call, saying either "Yes, we'd like to do a POC and here's when and how we can start" or "No, this won't work for our needs" is okay. What's important is that together, let us decide and have a clear understanding of what happens next. 

On the call from the GitLab side will be your enterprise team supporting MetLife, Sean Billow (Strategic Account Leader) and Josh Rector (Solution Architect). Please feel free to add to the agenda or invite anyone else.  

Warmest Regards,
Kevin


### Session Invite

Hello all,

We appreciate your invitation for a session on Thursday, Dec 20 @ 10:30 AM PST.

Our goal for this call is to ensure that you're getting the most out of your investment in GitLab. To that end we will provide a walkthrough of GitLab's CI/CD, Kubernetes integration, and Containerization. (If you have other topics you'd like to learn about, please suggest that as well).

As part of your Enterprise team supporting Lenovo, we want to make sure we're providing the right level of support throughout this process.

At the end of this call we will decide what the next steps will look like, and how to best proceed—even if it’s a no at this stage. If you want to add to the agenda or invite anyone else, feel free to forward this invitation or drop me a quick note.

Also on the call will be Sean Billow (SAL) and Dave Astor (Solutions Architect). 
Proposed Agenda:
1. Mutual Introductions
2. Your current tollchain/ecosystem
4. Overview of GitLab
5. Questions/next steps

Please feel free to add to the agenda as you see fit to better use our time together.

*****

Hello Adam and Matt,

Thank you for our call earlier today. We appreciate your invitation for a call on Friday, Jan 25 @ 11:00 AM PST and requesting a demo of GitLab.

GitLab is a single application that enables "Out of the Box" and "Off the Shelf" DevOps pipelines. This means being able to build images, pushing to containers, and deploying to Kubernetes (even Canary deployments) is all possible without the headache of integrating your typical Atlassian + Jenkins stack. CI/CD just works.

During this session, we'll give an overview of GitLab and a detailed walkthrough to showcase the "Art-of-the-Possible" with a single application for the entire DevOps lifecycle. Ideally, this will be an interactive session, so please come prepared with questions, ideas, and a potential project in mind.

At the end of the demo we will decide what the next steps will look like, and whether to proceed to a POC—even if it’s a no at this stage. If you feel that you want to add to the agenda or invite anyone else, just drop me a quick note.

Also on the call will be Sean Billow (SAL) and David Astor (Solutions Architect). 

Proposed Agenda:
1. Recap
 from Previous Call
2. GitLab Overview
3. GitLab Walkthrough
4. Questions/Next Steps

Please feel free to add to the agenda as you see fit to better use our time together.

Warm Regards,
Kevin

****

### Call Intro

We appreciate your invitation for a call today.

Naturally you’re going to have some questions for us, like: what can GitLab do for you or how well we can integrate with your stack.. 

Obviously in order to answer, we're going to need to better understand some things, such as: what are your current strategic initiatives, what's in your toolchain, etc. So we'll also have a few questions for you.

At the end of the brief call we will decide what the next steps will look like, and how to best proceed—even if it’s a no at this stage. 

Does that sound like what you were hoping to get out of this call, or is there anything you'd like to add to the agenda before we begin?

Lastly, we have this call scheduled for 15 minutes, but I can stay on a little longer if needed. Does anyone have a hard stop we should know about before we proceed?

Let get started with a quick round robin of introductions, to give everybody an idea of who's on the other side. I can kick us off here on the GitLab team, then transfer it over to you.

****

## The Magic Words
Once you get the ball rolling in any of the three modes we’ve discussed in this chapter—acquisition, retention, or win back—give yourself an “insurance policy” by posting this question to VITO:

You: “Ms. Importanta, in the next [several weeks], your team and our experts will be working together to discover if what we suspect is possible can actually take place inasmuch as [increasing shareholder value] while achieving [full compliance] is a major priority for you. I have a personal favor to ask of you (pause for a quick moment): Whatever their findings and decisions are, whether for or against starting a business relationship with my organization, would you grant me the privilege of an in-person visit with you?”

Then, if you ever need to play your get-out-of-jail-free card—play it!

****

Don’t send any more than you can effectively follow up on, but do send enough that you are taking action every day.”

Prefer making promises to setting goals. How many goals have you accomplished? How many promises have you kept?

