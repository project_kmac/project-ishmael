# Asking Your SAL to convert your SAOs

Because of the pairing we use here at GitLab, it's likely that at some point or another you will be waiting on your SAL to convert the IQMs you've set up into SAOs.

This is all fair and part of the job, and it happens to everyone.

Remember that as a POD (SAL-SDR Pair), we rely on each other. SALs need SDRs to keep their pipelines full, and SDRs need SALs to convert their opps into SAOs. It's a two way street, and I encourage you to find ways to keep the relationship on positive terms.

Feeling like your SAL isn't converting your SAOs (and thereby putting you behind on the board, or not meeting quota) is a fast path towards creating friction and tension in your working relationship, and if not addressed correctly, leads to finger-pointing, blame, and projection.

Not what we want, so here's a simple script to establishing a cadence for converting IQMs to SAOs that protects the pod.

****


Here's the exact message I would send on Slack:

"Hey {{SAL.first_name}}, I have a call coming up with Jeffrey (my team lead), would you mind converting the opps/meetings from this week please? Thank you"

And then link to the exact opps that need to be converted.

This makes it convenient for them and takes the burden off for having to remember which opp is what.

By establishing a regular cadence, this protects you as an SDR from the existential anxiety of "When will my SAL convert the opp?/Will I hit quota this month?" as well as the completely avoidable resentments/misunderstandings that can occur because of one party being reliant upon the other to convert the IQM to an SAO.

If there's an issue with the conversations, please feel message your team lead to go to bat for you and make sure your "At-Bats" are converted.

SALs are encouraged to do it within 24hrs of the IQM, but as long as you have a cadence, everyone will feel safe and taken care of.

**** 

All that being said, it is an integral aspect of your professional development to be able to ask for what you need. If you DO NOT ask your SAL to convert your SAOs consistently, you have no excuse to be resentful when they don't do it. 

Chances are, they're not holding your SAOs hostage -- they've got a lot on their plates too, and it's easy to forget if it's not made a priority by you.

Avoiding asking for your conversions is guaranteeing problems with your SAL-SDR relationship.