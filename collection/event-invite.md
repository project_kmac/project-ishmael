# Event Invite

Leveraging events -> SAOs.

Here's a link to Emily Kyle's Outreach template for Jenkins World.: https://docs.google.com/document/d/1f9jyuGcFzRDXljLM94s0YJK2kPz4DdkDsJlJ-uqyIRY/edit

## Email

subject-a: {{first.name}}, looping you in
subject-b: {{first.name}}, here's your invite


Hey {{first.name}},

Going to be visiting your area and thought it'd be important to loop you in.

The team and I are going to be visiting Cary, NC this week for SFA, and we're going to be hosting a DevOps Happy Hour during the conference. 

Right now, we have 23 DevOps Directors, Engineers, and Architects who've RSVP'd.

Wanted to invite you in for FREE food and drinks on us, and a chance to connect with some of your peers.

[video-thumbnail]
[link-to-video]

Date: Thursday, Sept 13th
Time: 5:30 - 7:30 PM
Location:  Rockbottom Brewery on N. Tryon (401 N Tryon St, Charlotte, NC 28202)

This is open to you, whether or not you're attending SFA.

If you would like to be added to the guest list, simply send back a one word reply "YES" and I'll make sure you get in.

Warm Regards,

Kevin


## LinkedIn Post

When you post something like this on LinkedIn, please link it to the rest of the team so we can like, comment, and share for maximum visibility. 

Also, this works way better with video, so I would upload your video using LinkedIn's interface, and then have the text pasted in below.

****

ARE YOU GOING TO BE AT SOUTHERN FRIED AGILE?

Hey Southern Fried Agilists!

The team and I are going to be visiting Cary, NC this week for SFA.

We're going to be hosting a DevOps Happy Hour during the conference. 

Wanted to invite you in for FREE food and drinks on us, and a chance to connect with some of your peers.

Here are the details...

Date: Thursday, Sept 13th
Time: 5:30 - 7:30 PM
Location:  Rockbottom Brewery on N. Tryon (401 N Tryon St, Charlotte, NC 28202)

This is open to you, whether or not you're attending SFA.

If you would like to be added to the guest list, simply comment below with a one word reply "YES" and I'll make sure you get in.





## Video Script

Add the video to LinkedIn post and to Email. Script is the same as what the email says. Doesn't have to be perfect, just hit record and ship it.

Version 1: https://share.vidyard.com/watch/zqEi2aYUXQqcT1Q15gJj9x?

Version 2: https://drive.google.com/open?id=1T0tbalQvErVTs1LcEAIG5Kqm2OQdVCC3