# Email Templates

A typical email outreach sequence for an SDR, using AT&T as an example. Use variables like [FNAME] and [COMPANY], [FIRST LINE], and [P.S.] for mail merge.

Important to highlight similar companies that we have proven successes with. 

Emails are like Golf. You don't need to get a hole-in-one (like you do for example, with a cold cold). As long as each stroke gets you closer, you can "tap-tap-tap" the prospect in. 

RE: Executive assistants. If you have a high level exec with an EA and can't get past the phone, email is a great way to make the connection and get the appointment.
 
****

## Templates within Outreach:

Huge thanks to Kaleb Hill for uploading templates into Outreach for everyone's use.

https://app1a.outreach.io/templates?q=ishmael&direction=desc&order=recent


### GitLab Snippets

NAME, GitLab enables telecom companies like AT&T to release software up to 26x faster while reducing costs by as much as 30%. Clients include Comcast, Dish, and Verizon. Even if there's no immediate need and only a "down-the-road" interest, are you open minded to a 10-min exploratory conversation?

GitLab's single application for DevOps enables telecom companies like AT&T to release software up to 26x faster while reducing costs by as much as 30% and eliminating toolchain complexity. Some clients include Comcast, Dish, and Verizon.

GitLab's single application for DevOps enables telecom companies like AT&T to plan, develop, and release software up to 26x faster while reducing costs by as much as 30% and eliminating toolchain complexity. Some clients include Comcast, Dish, and Verizon.


GitLab's single application for DevOps enables telecom companies like AT&T to simplify architecture and eliminate toolchain complexity while increasing visibility into the delivery pipeline. Some clients include Comcast, Dish, and Verizon.

GitLab's single application for DevOps enables telecom companies like AT&T to simplify architecture and eliminate toolchain complexity while increasing visibility into the delivery pipeline. We've helped clients like Comcast, Dish, and Verizon deliver software up to 26x faster and reduce costs by as much as 30%.

Greg, GitLab's single app for DevOps with auto testing enables companies like Comcast and Verizon to develop software up to 26x faster while reducing costs by as much as 30%. Even if there's no immediate need and only a "down-the-road" interest, would you be open to a 10min exploratory conversation?

GitLab is a single application for the entire software development lifecycle. This enables clients like Jaguar Land Rover, Tesla, and Goldman Sachs to shift security left, adopt DevSecOps, enable compliance for IT audits (including Sarbanes-Oxley and JSOX), deliver software faster, and reduce IT costs by 30%. As an additional example, JLR uses GitLab to achieve"blue-green deployment" for vehicles.

Typically, clients find the most benefit when they're a) needing to consolidate their toolchain, b) looking for SCM + CI/CD, or c) moving towards cloud native technologies like containerization, microservices, and Kubernetes.

****
#### Adam Pestreich - Deploy this when you already have some GL adoption

subject: {{company}} using GitLab

Hi {{first_name}}-

I'm reaching out as there are several instances of GitLab being used at UTC and there has been increased interest in GitLab within the organization.
 
GitLab is an end-to-end DevOps solution that covers the entire development life cycle in a single UI and companies like the largest bank in New York and the largest private wealth management firm in the world use it in order to reduce DevOps tool friction, reduce tooling cost and allow software development teams to ship secure code 2-3x faster.

Given your role, I thought I should reach out to discuss current conversations we are having within the organization and how GitLab supports your current initiatives. What is the best way to go about getting 15 minutes on your calendar for a quick introduction?
 
Regards,
Adam

****



#### Mike Miranda

subject: GitLab at {{company}}

Hi {{first_name}},

GitLab is currently running a POC with the Cerner CI/CD team that manages Jenkins. They're using GitLab CI/CD to prove the value of cloud native + ease of use with Kubernetes. Considering your role, I thought it made sense to reach out. 

GitLab is an end-to-end application for the entire DevOps lifecycle. With GitLab, shorter software lifecycles enable faster iteration + quicker responses to market feedback to help you keep pace with the value you offer your customers. 

What day would work best for a brief call to connect and learn more? 

****

subject: Time to chat?

Hi {{first_name}},

Teams at Cerner are evaluating GitLab with an interest in saving on tooling and shipping software faster.

GitLab can empower your dev, ops and security teams to collaborate more effectively and iterate faster while reducing costs.

I was hoping to grab just 15 minutes on your calendar next week to share more. What day would work best on your end?

****

#### Ridiculous Open Rate 

subject: {{company}} + GitLab

Hi {{first_name}},

I'm reaching out because groups at CSG are using GitLab for software development and operations. 
 
GitLab is a single application that covers the entire SDLC, from planning to monitoring. Companies like Sony, Redhat, and Oracle are using it to consolidate their tool chain and automate their workflow - freeing teams to manage projects, not tools for a 3x faster delivery.  

My understanding is part of your role involves reviewing/influencing the technology direction. Open minded to a brief chat with my team to dive a bit deeper? 

I'm available either {{weekdays_from_now 1}} and {{weekdays_from_now 2}}, which ever works best for you.

Cheers,

David

****

{{first_name}}- I know you're busy but we are excited to share with you why companies are migrating to Gitlab because:

They ship code 3X – 10X faster 
It's the only single application for DevOps; making for an even tighter feedback loop
Forrester rated GitLab the #1 tool for CI/CD in the market

Given your role within the SDLC, open to a brief chat to dive a bit deep into how CSG can build + deploy faster and improve release cycle times?

I'm pretty open in early September, what's a good day that works best for you?

Cheers,

David

****

Hi {{first_name}},

I made you a quick video that's under a minute, I promise. Just press play! 

[link to video]
[thumbnail]

Look forward to hearing back form you! 

Cheers,

David


#### Fin Serv - Soft Touch

subject: CNA + GitLab

Hi Donne,

I hope you enjoyed a nice weekend. I'd like to briefly share with you why 
companies like Nasdaq, ING Bank, and Freddie Mac use GitLab for financial services regulatory compliance.

Have any interest in learning more?

https://about.gitlab.com/solutions/financial-services-regulatory-compliance/

Warm Regards,
Kevin

#### Matt Walsh Sept 2018

**** First email: ****

 Hi {{first_name}},

 I'm reaching out because groups at GE are using GitLab for software development and operations, and we're having conversations with the digital teams.

 GitLab is a single application that covers the entire SDLC, from planning to testing and monitoring. Companies like Oracle, IBM and Lockheed Martin are using it to consolidate their tool chain and automate their workflow - freeing teams to manage projects, not tools for a 3x faster delivery. 

 My understanding is part of your role involves reviewing/influencing the technology direction. Open minded to a brief chat with my team to dive a bit deeper?

 I'm available the first week of September if that works for you.

 Cheers,
 Matt



**** Second email ****

 {{first_name}}-

 I know you're busy, but I hope to connect with you to share how we're helping teams at GE simplify and streamline their development and IT operations environments.

 Companies like IBM, NASDAQ, Intel and Freddie Mac migrated to GitLab because:

 Ship code 3X – 10X faster
 Tighter feedback loop + greater visibility down the build chain
 Market leading Continuous Integration & Continuous Deployment (Forrester 2017)

 Can I share more on a quick call, possibly third or fourth week of September?

 Matt





#### Post LinkedIn Connect

Thanks for connecting with me on LinkedIn. I saw that you were responsible for application security controls for containers, and thought this would be relevant.

GitLab enables telecom companies like Comcast and Verizon to simplify defining and configuring cloud infrastructure (strong Kubernetes integrations), limiting security with privileges and authentication, as well as enabling better visibility and compliance (LDAP group sync, audit logs, etc.)

Does anything at all of what I’ve just said seem relevant to your world, and may be worthy of a 5-10 minute discussion?


Warm Regards,

{{sender.first_name}}


#### Post-Referral Email

Hey {{first_name}}, 

I had a conversation with Tony Noori in Systems Engineering. He and I got around to talk about you and your role. He thought it might be important we speak, I promised him I'd reach out to you.

Call me at {{sender.phone_number}}



Warm Regards,

{{sender.first_name}}

*****

#### Post-Call Email (No Appointment Set)

Hey {{first_name}}, 

Thank you for taking my call earlier today. I appreciate your curiosity in requesting more information about GitLab.

GitLab is a single application for the whole software development lifecycle, (but it’s not a rip and replace). We integrate with your existing tool stack and meet you where you are to reduce complexity and costs (administrative and licensing). Clients like {{insert_relevant_companies}} use GitLab to speed up software delivery while reducing overhead and consolidating their toolchain.

Some of the features that our clients get the most use out of is GitLab Container Registry, CI/CD, integration with Kubernetes, automated security and testing at the code level, as well as performance metrics for deployed apps.

Does anything at all of what I’ve just said seem relevant to your world and worthy of a 3-5 minute discussion?

Warm Regards,
{{sender.first_name}}

#### Post-Call Email (Send More Information) by Mike Miranda

Hi David, 

Thanks for taking my call. The reason I reached out is we're confident GitLab can help you meet your goals of improving both the merchant and customer experience by iterating faster at a reduced cost. 

GitLab is an open source software company that is a single application for the complete DevOps lifecycle. Many large retailers (Walmart Brazil, Nordstrom, Macy's, etc.) depend on GitLab to help them deliver higher quality and more secure solutions faster. 

Don't worry we integrate, so you don't have to rip and replace.  

Why Customers Select GitLab.  
- Open source + Open Core
- Reduce risk
- Reduce Costs
- Increase automation
- Deliver better software faster
- Increase Speed of Innovation
- Customer Stories


About GitLab (LINK)
GitLab Direction (LINK)

****

### Monkey's Fist / Beachhead

{{first_name}},

Looking for a little bit of help. Reaching out because I'm doing research on your company to learn about how you use DevOps. 

Would you mind sharing with me, in general terms, how your software development/delivery pipeline is structured?

/ Would you mind pointing me in the right direction as to whom I should be speaking with?

Warm Regards,

{{sender.first_name}}

Chuck, looking for a little bit of help. Reaching out because I'm doing research on Unisys to learn about how you use DevOps. 

Would you mind sharing with me, in general terms, how your software development/delivery pipeline is structured?

Warm Regards,
{{sender.first_name}}


*****
### John Rosso

Subject Line: {{!Comment insert personal conection in subject line, ie.e "Your Presentation at Google Next"}}

{{first_name}},

I had a chance to review your presentation and appreciated the comments you made about getting "share-of-wallet" with your top customers.

GitLab's single application for software development enables telecom companies like Comcast, Verizon, and Dish to release software up to 26x faster while reducing costs by as much as 30%. 

Dave, even if there's no immediate need, and only a "down-the-road" interest, are you open minded to a 10 minute exploratory conversation?

Can we help you? I am not sure yet. A quick 10 minute phone call should help us figure that out. I will call you Thursday at 11 AM. If that time is inconvenient, please suggest an alternative.


Warm Regards,

{{sender.first_name}}


****

### 30 Second Commercial with Third Party Stories

Subject Line: {{first_name}}, question about {{company}}

Hello {{first_name}},

Came across your name while researching {{company}}. {{!Comments}}

I'm with GitLab, a technology company that provides a single application for the complete DevOps lifecycle.

Reaching out {{first_name}}, because I spend a lot of my time speaking with {{title}}, and typically here's what they say to me: they tell me they've got a great software engineering team in place, but often they're using legacy software with limited functionality like Perforce/SVN, which means they're releasing products only 2x a year.

Other times, they might have a modern tech stack, but maybe they're using a daisy chain of tools which may have brittle integrations that means they spend a lot of time maintaining the different services.

The other common thread I see is that they're concerned about visibility into their toolchain, which isn't easy when you're running on 7+ tools (each with their own UI, users, and permission sets). And this means that triaging and troubleshooting a "small" bug can take weeks.

Does anything at all of what I’ve just said seem relevant to your world and worthy of a 3-5 minute discussion?



Warm Regards,

{{sender.first_name}}

****

#### jBarrows


Hello {{first_name}},


{{!Comments first_line}}

The reason this prompted me to reach out to you is because we work with companies to take incremental steps to reduce their overhead, simplify their existing toolchain, and automate workflow to increase cycle time speed. Companies like Verizon, Ticketmaster, and Alibaba have used GitLab to speed up software delivery as much as 26x while reducing licensing & overhead costs by up to 30%. 

Who is the best person to speak with at {{company}} about your software development pipeline?

Warm Regards,
{{sender.first_name}}


Recently read about how Pepsi had moved to the cloud for your HR, in an article where Shakti Jauhar said, "this is not about technology. The journey really is about significantly enhancing the experience of the user, and at the same time doing things differently."

The reason this prompted me to reach out to you is because we work with companies to move to the cloud in your Development/Operations. Companies like Verizon, Ticketmaster, and Alibaba have used GitLab to speed up software delivery as much as 26x while reducing licensing & overhead costs by up to 30%, and increasing security & visibility in your delivery pipeline.

{{first_name}}, even if there's no immediate need, and only a "down-the-road" interest, are you open minded to a 10 minute exploratory conversation?


Had a chance to see Peter Lega's presentation on how Merck uses DevOps in a regulatory space, where he described how Quality Assurance & DevOps work together to enable quality within the organization, balancing increasing cycle times with safety.  

The reason this prompted me to reach out to you is because we enable DevOps and help companies to take incremental steps to automate testing into their workflow to increase cycle time speed. Bayer, CERN, and KWS have used GitLab to speed up software delivery as much as 26x while reducing licensing & overhead costs by up to 30%, and increasing security & visibility in their delivery pipeline.

{{first_name}}, even if there's no immediate need, and only a "down-the-road" interest, are you open minded to a 10 minute exploratory conversation?


Recently read in an article how Peter O'Donoghue talked about barriers to federal DevOps adoption: "A typical applications team may have to navigate six or seven other contractors in order to be able to deploy their application and get it into production." He recommended a model heavily using automation to manage builds, deployments, regression testing and security scanning, to let government agencies achieve better, cheaper, faster and more secure results.

The reason this prompted me to reach out to you is because our single application enables DevOps and allows for automation in managing builds, deployments, regression testing, and security scanning. Intel, Interpol, and the US Air Force have used GitLab to speed up software delivery as much as 26x while reducing licensing & overhead costs by up to 30%, and increasing security & visibility in their delivery pipeline.

{{first_name}}, even if there's no immediate need, and only a "down-the-road" interest, are you open minded to a 10 minute exploratory conversation?

Wow. I read this morning Julia Davis' comments at Pegaworld where she talked about how “Aflac was an expensive siloed environment,” with “broken trust between IT and business”. Really excited to hear about how Aflac is using technology: "Transformation is not just about technology, it is about transforming the process."

The reason this prompted me to reach out to you is because, along those same lines of transforming the process, our end to end solution for DevOps and the software development lifecycle helps companies speed up software delivery as much as 26x, while reducing licensing & overhead costs by up to 30%, automating security, & increasing visibility in their delivery pipeline. And because Aflac is now already an Agile organization, DevOps is the logical next step.

{{first_name}}, even if there's no immediate need, and only a "down-the-road" interest, are you open minded to a 10 minute exploratory conversation?

Julia, even if there's no immediate need, and only a "down-the-road" interest, who would be the best person to speak with at Aflac about your software delivery pipeline? 


****

### Selling in Manufacturing and Logistics

This is William Lee with ABC Company. I am guessing you have not heard of me or our company. 

Our clients tell us that, as a result of our inventory management program, they no longer worry about their supplier keeping them stocked on a timely basis with our product, and thus are no longer concerned that their production line will unexpectedly shut down, costing them a lot of dollars in lost production. However, I don’t even know if these are issues you are facing that are worth having a further discussion with me. 

It would be disrespectful of me to presume we could potentially help your company without sitting down and learning more about your business. If you would be kind enough to let me know if it would simply be worth having a cup of coffee sometime to learn more about each other’s worlds, I would appreciate it very much. I look forward to hearing from you either way.


Follow up two weeks later...

“This is William Lee with ABC Company. I thought I would circle back with you on my previous note. If you would have the chance to reply either way, I would appreciate it very much. Thanks.”

Final follow up...

This is William Lee with ABC Company. I have sent you a few previous messages and I haven’t heard back, so I am getting the feeling that there is simply 0% interest in even learning more about each other’s businesses and you are simply being too nice to tell me that. If that is the case, I completely understand and respect it. We are not for everyone. If you would be kind enough to confirm my suspicions, I will close your file and not bother you further. Best of luck in your business.



****
### Personalized Alex Berman

Subject Line: {{first_name}} - {{company}}
Hello {{first_name}},

Came across your name while researching contacts at {{company}}. {{!Comments first_line}}

I'm with GitLab; We've helped companies like Comcast and Verizon release software up to 26x faster while reducing costs by as much as 30%.

Reaching out because we've worked with other telecom companies like AT&T with automation, Kubernetes, CI/CD, and going cloud native, and wanted to see if you were open to discussing your DevOps strategy.

Reaching out because we've helped ther telecom companies like AT&T take incremental steps to reduce their overhead by integrating with their existing toolchain to better automate workflow and speed cycle time.

Would you be open to a quick call? Happy to send over some times.


Warm Regards,

{{sender.first_name}}


P.S. Congratulations on {{!Comments post_script}}!

****
### Features and Benefits Bullet Points

Subject Line: Question about {{company}}

Hello {{first_name}},

Quick question for you: How are you using DevOps at {{company}}?

I'm with GitLab, and we've helped companies like Comcast and Verizon release software up to 26x faster while reducing costs by as much as 30%.

Typically, there’s three ways we can do this for you:

* Increase visibility with built in Issue Tracker/Kanban Board
* Simplify toolchain and reduce integration complexity from 7+ tools (typically GitHub/Bitbucket, Jira, Jenkins, etc.) down to ONE tool and ONE UI  
(Note: With strong integrations, there's no need to rip-and-replace. GitLab plays well with your existing stack)
* Speed up development cycles and eliminate bottlenecks from occurring by streamlining your workflow process and increase automation and collaboration throughout the development process

We can do this because we've created the first single application for the entire DevOps lifecycle. GitLab is used by over 100,000 organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel.

If it makes sense to talk further, what does your calendar look like for a call?


Warm Regards,

{{sender.first_name}}

****

### Appropriate person

Subject Line: Appropriate person

Hello {{first_name}},

Writing in hopes of finding the appropriate person who handles DevOps. I also wrote to {{!Comment person x, person y, and person z}} in that pursuit. 

GitLab enables companies to release software up to 26x faster while reducing costs by as much as 30%. We do this via a comprehensive, single application for the entire Software Development Lifecycle, which increases visibility, reduces integration complexity, and eliminates security and quality bottlenecks. Companies like {{company}} can incremental steps to reduce their overhead by integrating with their existing toolchain to better automate workflow and speed cycle time. GitLab is used by over 100,000 organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel.

If you are the appropriate person to speak with, what does your calendar look like? If not, whom do you recommend I speak with?



Warm Regards,

{{sender.first_name}}

****



### Replies

Hey {{first_name}}, when would be a good time for you to discuss this on a quick 10-minute call? Do any of these times work? 

{{first_name}} can we help you? I am not sure yet. A quick 10 minute phone call should help us figure that out. I will call you Thursday at 11 AM. If that time is inconvenient, please suggest an alternative.

Hi {{first_name}}, are you the appropriate person to speak with about this? If not, whom should I reach out to?


****

### Last Email

Subject Line: RE: {{first_name}}, question about {{company}}

Hi {{first_name}}, at this point I’m going to assume DevOps is not on your timeline for this year. Please feel free to reach out if you have any questions about increasing software delivery speed or reducing toolchain complexity.


Warm Regards,

{{sender.first_name}}

{{sender.phone_number}}


****

### Reply to Disqualified Prospect


Hey {{first_name}}, not a problem. I really appreciate your time. It doesn't sound like we can help you. Quick question, maybe you can help me. 

If you were me in my business (enabling DevOps), any recommendations of people in your network whom you think it may make sense to take a couple of minutes to speak with?


Warm Regards,

{{sender.first_name}}

{{sender.phone_number}}

****

### IQM Set

Hi {{first_name}}, 

Thank you for taking my call earlier today. I appreciate the invitation to meet to explore [insert issue]. 

As discussed, naturally you’re going to have a bunch of questions for me, such as: [insert three or four typical issues surrounding the issue]. 

Obviously, as agreed, I’m going to need to better understand some things, such as: [insert three or four typical questions that you need to know answers to concerning the prospect’s world, experiences, and history, and the issues]. 

At the end of the meeting we will decide what the next steps will look like, and how to best proceed—even if it’s a no at this stage. If you feel that you want to add to the agenda or invite anyone else, just drop me a quick note.

Also on the call will be {{SAL}} and {{Solutions Architect}}. 


Proposed Agenda:
1. Mutual Introductions
2. Current GitLab/Henry Schein relationship
3. Your current DevOps ecosystem
4. Overview of GitLab, especially Kubernetes
5. Questions/next steps

Please feel free to add to the agenda as you see fit to better use our time together.
​

Warm Regards,
{{sender.first_name}}

{{insert Zoom meeting invite}}


****

## Subject Lines to Test

* Lori, how is AT&T using DevOps?
* Lori - AT&T and GitLab
* Hi from Kevin
* Question about AT&T
* What’s your DevOps strategy?
* Lori, riddle me this
* Lori, loved the new post on {{subject}}
* Lori, congrats on the {{!Comment}}
* Came across AT&T through Lori
* Use personal conection in subject line, ie.e "Your Presentation at Google Next"
* Time to chat?
* GitLab at {{company}}


{{company}} + GitLab = Seems to be the BEST subject line for OPENS. DFish has a 90% open rate, on email, in 2018 (how insane is that!!?) with a sample size of 92 prospects.

**** 

### No Need Email

I apologize for any confusion on my part, please know that our chat today is not intended to sell you any products. That's something we'll never do here at GitLab. However, we do see that your team is using GitLab/GitHub/Atlassian quite a bit, and this is why I think a chat with me will be extremely helpful going forward.

As your part of your dedicated strategic account team, our role is to be an adviser and subject matter expert to enterprises like yours. The reason for the call is to understand more about {{company}} and how you're using GitLab since you signed up. That way, we can give you custom tips, best practices, and exclusive benefits to help you be more successful in your DevOps adoption journey. This is something we do for all of our strategic and large enterprise clients at no additional cost.

Is there a better time for us to connect?

Looking forward to hearing from you,



****

## Mike Miranda

### Relevant News + Role

Hi Sathya,

In a recent article Jeremy King highlights Walmart's focus on "increas[ing] operating efficiencies while decreasing enterprise expenses". This is also a goal noted in the 10-K. Considering your role, I thought you'd be interested in learning how customers are leveraging GitLab to do exactly that. 

GitLab is the first single application for the entire DevOps lifecycle, from planning something to rolling it out (e.g. container registery, monitoring, etc.) all in a single product. This allows the entire organization to get on the same page while making the software lifecycle 10x faster and radically improving the speed of business.

When would you be available for 15 minutes to learn more? I have availability next Tuesday and Wednesday. 

Best, 

## Eric Arroyo.

### LinkedIn Messaging

Thank you for connecting with me! We are currently working with a small team of about 100 users at Avaya to speed up software development and to unify the DevOps tool chain (which saves a lot of money for the company) and we just partnered with Google Cloud. I was hoping to grab 15 min with you to discuss the current initiatives Avaya has around Google Cloud and DevOps. We are the only application that handles every aspect of DevOps under 1 UI which gets rid of the bottlenecks that comes with a traditional 7+ tool chain. We also have the number 1 ranked CI/CD tool in the industry ranked by Forrester. We can handle the whole DevOps chain or piece by piece. Do you have 15 min next Tuesday or Wednesday to talk about your current goals for DevOps?


### Email 1

Hi {{first_name}},

I help IT Leaders improve & speed up their software development to eliminate bottlenecks that comes with a traditional 7+. 

In my research, I noticed that General Mills is currently leveraging DevOps tools to gain a competitive advantage. 

GitLab works with companies such as Intel and Kroger to unify the DevOps toolchain and speed up software delivery by 2-10x (which saves a lot of money for the company).

I believe GitLab can help you eliminate bottlenecks from occurring by streamlining your workflow process and increase automation and collaboration throughout the development process.

Let’s schedule some time to discuss your DevOps strategy.

Do you have 15 minutes this week or next week?

### Email 2

Hi {{first_name}}, 

I wanted to follow up on my recent outreach. Love to get your thoughts?

We just partnered with Google Cloud are currently helping companies like Intel, Kroger, and Oracle to increase collaboration throughout the development process and ship quality code 2x-10x faster, which can drastically save money in the process. 

I would like to grab 15 min of your time and learn about your current goals for DevOps to see if GitLab can help your team in anyway.

Do you have any open time this week or next?


### Email 3

Hi {{first_name}}, 

As a leader at General Mills, it’s my understanding that your role involves influencing the technology direction.

I would like to grab 15 min of your time and learn about your current goals for DevOps to see if GitLab can help your team in anyway.

GitLab's end-to-end concurrent DevOps solution covers the entire development life cycle, from planning to monitoring. 

Not only do we offer Disaster Recovery but we also provide issue management, version control, code review, #1 CI/CD (rated by Forrester) and more. 2/3 of the organizations that self-host git use GitLab, which is more than 100,000 organizations and millions of users.

Do you have 15min this week or next for a quick introduction into GitLab?


### Email 4

{{first_name}} -General Mills | GitLab

Hi {{first_name}},

Here is a customer success story that I think you will find relevant.
If possible, I am trying to get 15 minutes on your calendar.

Do you have time this week or next for a quick introduction?

Thanks,



### Email 5

When speaking with IT leasers .

Hi {{first_name}},

let me guess your team has their favorite tools and you’re not looking to rip and replace your whole DevOps toolchain. And that makes sense!

With Gitlab’s integrations, you don’t have to rip and replace your team’s favorite tools. Instead, keep whats working and incorporate Gitlab's value in places that are lacking. 

Why not increase visibility, collaboration, automation, security, and quality code with Gitlab. Would love to share the value that Uber, NASA and many more are seeing with using GitLab single UI

Do you have 15min this week or next for a quick introduction into GitLab?

## Matthew Walsh

### Email 1

Hi Jason--I noticed your work there at GE and felt compelled to reach out. GitLab is currently in use at GE Healthcare and GE Digital. Would you be interested in a chat on how it may benefit your team or teams close to you as well?

As you may know, GitLab is the first end-to-end single application solution to the software development toolchain, including the market leading CI/CD (Forrester 2017) and Automated DevOps for the enterprise: https://about.gitlab.com/ 

Would you like to connect for 10-mins the week of July 9th?

