# Resources

If you're a new SDR, please go through these resources.

## Browser Extensions

* DiscoverOrg
* ZoomInfo
* Outreach Everywhere

Utilities:
* TypioForm Recovery
* OneTab
* Vidyard
* LinkedHelper
* Authy - So you can efficiently do 2FA from your browser
* Time Zone Converter - https://chrome.google.com/webstore/detail/time-zone-converter-savvy/plhnjpnbkmdmooideifhkonobdkgbbof?hl=en

I use a free Activtrak account to block Youtube, Reddit, Facebook, etc. Seriously, don't waste your time.

## Physical Hardware

* 11x17 Size WhiteBoard + Expo Markers (for your videos. You can get the whiteboard inexpensively at a thrift store)
* Buy a good mic and headset for your cold calls (I use an Audio Technica ATR2100 mic)
* If you're having Wifi connectivity issues due to distance from the router/not an ethernet connection, consider purchasing this - https://www.bestbuy.com/site/netgear-powerline-1000-network-extender-white/4760603.p?skuId=4760603 (thanks to Jeffrey Broussard for this suggestion)

