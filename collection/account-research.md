# Account Research

All sales development starts here. The effort you put into account research in the beginning will give you the lay of the land, the key players, and the fulcrums of leverage you need for your campaigns to be successful. Store all of your account research in one place so that it is easily referenceable. The time you invest into gathering this information upfront will pay dividends down the road.

## Annual Report, 10K-10Q
  
    Google "{{Company Name}} Annual Report"
    Read the letter from the chairman - these will normally be the first couple of pages
      
        What are the successes from last year?
        Any awards / accolades that are mentioned that could be useful for an ice breaker?
        What areas are they naming as important for growth in this year?
        What is the alignment of the business? (Different business units / functional groups within the org?)
      
    
    Look through the Table of Contents (normally after letter from the Chairman, before the 10Q-10K
        Look for keywords like "focus, invest, opportunities, strategic, etc."
      
        Is there an executive summary?
        Outlook?
        Risks?
        Financial Analysis - where are they spending, what are they spending? Can you identify potential growth/risk opportunities (WIP - Link to potential articles or resources for more info)
      
    
  

## Alerts
  
    Set up DiscoverOrg alerts to be delivered to mailbox
    Set up LinkedIn Sales Navigator Lead / Account Alerts
    Set up Google Alerts Also, consider setting alerts for big names in company like CEO, CIO, CTO to find quotes you can reference later
    Set up Owler Alerts 
  

## Technologies / Trends
  
    Do a DiscoverOrg search for Technologies (not reliable, but a good starting point)
    Comb through LinkedIn Sales Navigator (search Account + Keywords (Atlassian, Git, BitBucket, GitHub, GitLab, etc.)
    Go on Glassdoor, Indeed, LinkedIn Jobs. Look for Job openings - are they ramping up hiring in a certain area? What are some of the techs required for new employees?

