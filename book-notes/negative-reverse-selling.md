# Tom Niesen

https://www.youtube.com/watch?v=6aLfbP-Vhys

Everybody has problems. People don't fix problems, they fix pain. I don't look for problems, I look for pain.

## Examples: 

> I'm sure you guys can help us.

"Well, who are you buying from? ABC Company? They're a good competitor. I don't see why they'd be giving you any problems."

> We're really happy with who we're using, there's no way we'd change.

"No probalem, that's great. You probably hold your vendors in high esteem like our customers hold us in high esteem. You probably never ever ever have a problem with..." Quality, (AND) Pricing, (AND) Lead Time

"...but they take care of you anyways, right? / but at least they get things taken care of, right? / but at least they fix it in the long run?"

"I gotta ask you a question. A few minutes ago, you said everything was fine, no problems. And over the last few minutes, I've heard you do have some issues with pricing/consistencies/lead times. What should we do about that?"

****

## Four Things You Need to Practice Judo

1. Emerson's Law of Compensation; "The way to get what you want is by helping others get what THEY want"
2. A Full Pipeline; It's difficult to go for the No when your pipeline is empty.
3. Practice
4. Willingness to Fail; A judoka is ready to fall 


## Your job is to solve your prospect's problem WITHOUT your product or service.

* What machines now do you have from different manufacturers? Why?
* If it is 20% faster, would that help?
* Have you tried to retrofit it to speed it up?
* Have you asked the manufacturer how to speed it up?
* Have you tried better maintenance?

I'm trying to fix your problem without buying from me. Because at some point, all the objections will have arisen and been dealt with.

* What different companies have you tried?
* What would you like them to do better?
* Have you told them that?
* Could it just be your service manager, not the company?
* How can I help you without changing what you're doing? 

## Why Would You Even Give Me An Appointment?

If you're getting an appointment with someone you've wanted to get into for some time, and you finally get that appointment, the first thing that should come out of your mouth, before you even do an upfront contract or anything else is...

"Hey look, I appreciate you giving me the appointment/inviting me in. I know you've been buying from my top competitor for five years. Why would you even give me an appointment? They're a good company."


## How to Practice Negative Reversing

**Week 1: Practice on things you don't care about with people you don't know**

"You probably couldn't get me a cheaper rate, could you?" 
"You probably couldn't upgrade me to a suite, could you?"
"You probably couldn't upgrade me to first class..."

**Week 2: Ask for Double what you think, and it's got to pass the giggle test.**

"You couldn't get me the Suburban for the price of a Tahoe, could you?"
"You probably couldn't get me a free drink ticket AND a free breakfast ticket?"
You probably couldn't get me a steak dinner

**Week 3: Negative Reverse everyday for things you know you can't get... It's okay to fail**

* Whale - Feed my family for a year
* Deer - Feed my family for a month
* Rabbit - Good dinner, lots of them out there, but you need to keep catching
* Rat - A bad lunch

Negative reverse the rats. 

**Week 4: Negative reverse selling with suspects you don't care about yet**

"I've been watching you, and I've got a problem with what you're doing. People come up to you, you get into arguments with them, and then they make appointments with you."

I could probably use some of that sales training.
What do you sell?
We sell x...
You don't need sales training for that. Everybody needs that. You don't even need to sell that. You just walk in and give it to them.
You obviously don't know my business. You know how hard it is to sell that?
No, tell me about that.


"Lots of times, people think they need to upgrade their system, but they really don't. Why would you even think about needing to upgrade your system?"
Well, I started a business, I got four lines coming in, and when I'm on the phone - we bought it from Office Depot - and another call comes in, I gotta put this phone down and go over to that phone.
Why don't you push your desks together?
I can't do that. We're too busy. Plus I've got six more lines coming in because we're growing.
Well, I don't know if I can really show you anything here. Why don't I come out and make an appointment to come visit you?
When can you be there?

**Week 5: Plan to Negative Reverse a Client today**

"It probably wouldn't make any sense for me to come in and talk to you about our other product line. There's probably no need for it."

**Week 6: Practice on accounts you know you can't get**

How about this. I send you four questions to ask him. If the answer to any of those questions are negative, there's no sense for us to meet. But if they're all positive, we should probably have at least a phone conversation if nothing else.

When should we meet, if that does happen. We'll put it in pencil. Possibly, tentatively, if you say yes to these four questions.

Am I the only one you guys are looking at? Who am I up against? Oh. *sighs* What's going to happen when you guys find out I'm NOT as good as they are, and I charge more than they do?

Well, what are you guys looking for? (Well, we heard you had a process.)

So here's my problem. Helen told me I couldn't close you. And in order for me to show you the process, I've gotta... Here, I got a great idea. Let's have an agreement up front. How about, I show you the process, and when you're done seeing it, you guys decide "No" it's not the process for you." Or "Yes, you'd like to go in this direction." And the Yes only means that we'll evaluate one of your sales teams and take a look at it piece by piece. Because you've got a hundred and fifty sales people. We would just take it one step at a time. And we just have that agreement. So could we agree to that upfront? 

****

### Eg

>I'm interested in what you have to offer.

"Really? I'm surprised by that. What in particular are you interested in?"