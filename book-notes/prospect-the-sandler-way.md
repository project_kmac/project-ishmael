# Prospect the Sandler Way - John Rosso

Book on Amazon: https://www.amazon.com/Prospect-Sandler-Way-John-Rosso/dp/098326144X

Webinar: https://www.youtube.com/watch?v=vaQf7n8XO7w

## John Rosso's LinkedIn Inmail

Tara, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

I'm the CEO of Sandler Training in Charleston. We are a sales and sales management training and coaching company that teaches a methodology used by the top 1% of salespeople.

I saw on your profile that you work with a number of businesses and thought you would enjoy my complimentary workshop. We will discuss the key differences between the people who can sell versus people who will sell, and discuss the 5 critical weaknesses that prevent salespeople from executing.

I hope you can join me as my guest. If you can't make it, let me know if there's an interest in the future. I'll keep you on that list.

****

Joe, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

I'm the SDR of GitLab in Raleigh, NC. We are a software company that's built an application for the entire SDLC/DevOps lifecycle used by the top Fortune 500 enterprises. Clients include Goldman Sachs, Freddie Mac, and BNY Mellon.

I saw on your profile that you work in the Infrastructure/Operations/Application Development/DevOps arm of the organization and thought you would enjoy our complimentary workshop. 

We will discuss the key differences between the enterprises that are successful in their digital transformation, and discuss the 5 critical weaknesses that prevent the enterprise from realizing the benefits of DevOps.

I hope you can join me as my guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.


****

Randy, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

I'm the SDR of GitLab for Chattanooga, TN. We are a software company that's built an application for the entire SDLC used by clients like Michelin to accelerate their DevOps adoption.

I saw on your profile that you oversee applications development and enterprise architecture and thought you would enjoy our complimentary workshop. 

In this workshop, we will discuss "Why Enterprise Digital Transformation Fails" and how you can realize the full benefits of DevOps.

You wouldn't be interested in joining us for this presentation, would you?

Warm Regards,
Kevin

*****

Joe, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

I'm the SDR of GitLab in for BofA/AT&T. We are a software company that's built an application for the entire SDLC/DevOps lifecycle used by the top Fortune 500 enterprises. Clients include Goldman Sachs, Freddie Mac, and BNY Mellon. (As you may already know, there are also a few teams at AT&T utilizing GitLab)

I saw on your profile that you work in the Infrastructure/Operations/Application Development/DevOps arm of the organization and thought you would enjoy our complimentary workshop. 

We're hosting this at TopGolf Dallas on behalf of a customer to facilitate a "real world" conversation on DevOps.

Executives from USAA, Toyota, and more will be in attendance, with USAA sharing the story of their Journey to Continuous Delivery.

I hope you can join me as my guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You wouldn't be interested in joining us for this presentation, would you?

Warm Regards,
Kevin

## 2014 Sandler Summit

Typically, when I speak with CTOs, here's what they say to me...

They typically have a very experienced very good, senior IT leadership team. But occasionally get frustrated because they have all of these initiatives, whether that's Agile, Digital Transformation, or DevOps, but things aren't crossing the finish line. Which means that they aren't fully realizing the promised value of a digital transformation.

Or maybe they are doing a good job of enabling DevOps and in fact have a pretty sophisticated pipeline. But they're concerned that having bought all of these tools, they've now created a lot of toolchain complexity, maintenance overhead, and may in fact have just built the next "legacy" system. Which means that 

Has any of that ever been an issue worth a 3-5 minute conversation.

Three Questions:
* Can you tell me a little bit about that?
* Help me out, can you give me kind of a real life, recent example?
* And so, because of that, what?

Question A:
"Dave let me ask you, any of that ever been an issue worth a 3-5 minute conversation?"

Question B:
"If you had to pick just one, where would you start?"

Question 1:
"When you say it's... Tell me a little bit more about that."
Question 2:
"Can you help me for a second, I want to make sure I can see the world from your point of view. Can you give me kind of a real life example?"
Question 3:
"And so because of that, what happens?"

Question C:
"Sounds like there could potentially be a fit, could I ask you a couple more questions?"


* Do this for me
* Take out your calendar
* Invite me out
* Pick out a day

*Post Sell*

Perfect Dave. Wednesday at 2 PM is great.

So here's my thought. I have some questions in my head I do want to ask you. You said it was x... I want to ask you a little more about that, I want to understand a little more about what's in your toolchain/deployment pipeline. Some of the things you're doing to deal with that. What's worked, what hasn't - talk to you a little bit about whether that even matters in your business. Give you the opportunity to pick my brain, learn how we do this for other organizations. If we find that it's not a fit, 100% okay. It's two guys in Charlotte who get a chance to meet- nothing bad happens. If it makes some sense to work further, even better. We'll spend the last five minutes of that meeting figuring out, "Does it make sense to invest more time and build a business relationship?" I don't know what that will look like, we may decide to work together that day. We may decide we need a second meeting with other people. Who knows. Are you comfortable with that?

Can you do me a favour Dave?
* Think about what your top three issues are
* Send me an org chart

Post-Sell:
I will send you the same agenda I just shared with you in an invite. Please look at it and make any changes that you want.

Hey Dave, this is a busy day for me, I am running from one part of the town to the next. Is there any chance at all that you're going to have to change, reschedule at 

## Webinar Notes 

Typical/Conditioned Sales Call:

"Hello David, this is Kevin McKinley at GitLab. How are you today? The reason I'm calling is because... "

1. Introduce Name + Business
2. HAYT
3. Reason for CAll
4. Bridge/Because (Typically Egocentric)
5. Ask For Appt


Approach Call Mistakes:
HAYT Crime = "How A You Today?"	
UnJust = Eliminate "just" and weak words
Sing-Songy/Mother May I? (Codependency)
Gone in 30 Seconds / Hit and Run Calls


Gatekeepers have a three-question system:

1. May I ask who's calling?
2. What company are you calling from?
3. What is this about?

"The sales processes is not where you want to get your emotional needs met."

Sandler Call:

1. Pattern Interrupt
2. Up Front Contract
3. Personal Connection / Research
4. Fishing for Pain / 3rd Party Stories
5. Close Appt
6. Set Agenda for Appt
7. Post Sell to Avoid BackOuts


### Step 1: Pattern Interrupt

The role of the pattern interrupt is to disrupt our conditioned response to a cold/prospecting call. It takes the prospect off from that script, and puts them in our script.

"Hello David, this is Kevin McKinley. I'm looking for a little bit of help..."
"Hello David, this is Kevin McKinley. I don't know if it's going to make sense for us to talk..."
"Hello David, this is Kevin McKinley. I'm guessing that my name doesn't sound familiar to you..."
"Hello David, this is Kevin McKinley. My guess is that my name is not familiar at all in any way... Listen, not a problem, I wasn't sure it would be. Let me take a moment, share with you the reason why I'm calling, put a little bit of context around that conversation, and then you and I can figure out if we should talk further if that works..."


### Step 2: Up-Front Contract



"Let me share with you the reason why I'm calling and then you and I can decide if it makes sense for us to meet/talk further, does that sound fair?"
"Let me tell you the reason why I'm calling, put a little bit of context around our situation, and then you and I can figure out if it makes sense to talk further if that works."


### Step 3: Personal Connection

**Referral**-

Sandler:
"Hey Mike, Kevin McKinley. Looking for just a little bit of help. I don't know if it's going to make sense for us to talk. Let me put a little bit of context around the conversation, and then tell me if going to make sense to talk further. Fair enough? One of the reasons I had a conversation with Dave Mattson at Mattson Incorporated. He and I got around to talk about you and your company. He thought it might be important we speak, I promised him I'd reach out to you."

"One of the reasons I'm reaching out is because I had a nice conversation with Dave Mattson at Mattson Incorporated. We got around to talking about you and your business. He thought it might be important we speak, I promised him I'd reach out to you."


Typical/Conditioned:
"Hey Mike, Kevin McKinley. I got your name from Dave Mattson."

"Hey Mike, this is Kevin McKinley at GitLab. I got your name from Dave Mattson over at Mattson Incorporated. We are the first single application for software development, security, and operations that enables Concurrent DevOps, making the software lifecycle 3 times faster and radically improving the speed of business. I think we may really be able to help you in your DevOps cycle. If you could just give me a quick call back, my number is..."

*Voicemail*

"Hey Mike, Kevin McKinley. Just had a nice conversation with Dave Mattson at Mattson Incorporated. He and I got around to talk about you and your company. He thought it might be important we speak, I promised him I'd reach out to you. Mike, call me, Kevin McKinley at 555-555-5555, 555-555-5555."

*Follow up call*

"Hey Mike, Kevin McKinley. It is 2:35 PM Wednesday. Reached out to you earlier on Monday. Hoped we could connect. Shared with you that Dave Mattson and I got around to talking about your business, and again, he thought it would be valuable you and I speak. I did promise him I would reach out to you."


*If a prospect says, "Thanks for the time. Really not a need." Ask for a referral.*

"Hey listen Dave, not a problem. I really appreciate your time. It doesn't sound like we can help you. Hey, quick question, maybe you can help me. If you were me in my business, any recommendations of people in your circle, in your network whom you think it may make sense to take a couple of minutes with (not unlike the call you had)?"


**Research About the Person**

Has this person given any presentations/speeches recently?

"Hey Dave, one of the reasons I was reaching out is I had a chance to review the presentations you gave on the changing face of supply chain, at Supply Chain International at Vegas this last October..."  

Sample Email 1:

>Dave,
>
>I had a chance to review your presentation and appreciate the comments you made about getting "share-of-wallet" with your top customers.
>
>We have worked with a number of major retailers on loyalty programs and line expansions, increasing the annual buying from top customers by 37%-127%
>
>Can we help you? I am not sure yet. A quick 10 minute phone call should help us figure that out. I will call you Thursday at 2 PM. If that time is inconvenient, please suggest an alternative.


Sample Email 2:

>I had a chance to review your presentation and appreciate the comments you made about getting "share-of-wallet" with your top customers.
>
>We have worked with a number of major retailers on loyalty programs and line expansions, increasing the annual buying from top customers by 37%-127%
>
>Dave, even if there's no immediate need, and only a "down-the-road" interest, are you open minded to a 10 minute exploratory conversation?


**Recent/Relevant Company News**

"Mike, John Rosso, looking for some help. Don't know yet if it's going to make sense for us to talk. Let me put a little bit of context around the conversation, and then we figure out if it makes sense to talk further. Hey, just read in Crane's business review that you recently acquired or  opened up a new plant in ABC Ohio..."

"Dave, Kevin McKinley here. I've been doing some research on you and your organization. (Insert personal connection). I think it may be important that we speak. Dave, call me, Kevin McKinley at 555-555-5555. That's 555-555-5555"

"Hey Timothy, Kevin McKinley here. I'm looking for some help. Don't know yet if it's going to make sense for us to talk. Let me put a little bit of context around the conversation. And then we can figure out if it makes sense to talk further... I read in the New York Times that Warby Parker raised $75m in funding and that 2018 will be your first full profitable year..."


### Step 4: Fishing for Pain

Nobody cares about your features and benefits. Selling is not about you, your product, or your company. It's about your prospect, and their problems.

**Structure your pain staments as such:**

1. Stroke
2. Pain
3. Impact

**No FIRST PARTY STORIES**

"Dave, are you having problems with [DevOps, moving to the cloud, etc.]?"

If this is an approach call, and we don't have a relationship already established, then there may not be enough trust for the prospect to talk to us about their problems. 

**Don't Bullet It"**

"Dave, I work with CEOs of Software Companies like yourself who are:
* Frustrated by they don't close enough business
* paint poin 2
* pain point 3"

This doesn't tell a story. This puts them in a logical headset, rather than put themselves into the movie. This is intellectual jargon that evokes ZERO emotional response.

**Tell 3rd Party Stories**

"Typically, when I talk with a [Chief Architect, Head of IT, Director of Dev Ops]
(Stroke) They are very happy with...
(Pain) But they tell me they occassionally worry about...
(Impact) And as a result..."



"Dave, often when I talk with a Senior Executive at a Software Company, they'll often tell us one of three things:

"#1 
(Stroke)They've got a senior salesforce. They know their products. They're pretty good at what they do.
(Pain) Occasionally where they get frustrated, is they see all this business that's been forecasted, it's in the pipeline, but it just doesn't seem to be coming over the finish line.
(Impact) And that impacts our quarterly cashflow."

*Nest Three of the Pain Statements*

Oftentimes, use the second 3rd party story to show that the first pain is not really a problem.

"#2 
(Stroke) Listen, they're closing what's in the pipeline. 
(Pain) Where we sometimes get frustrated is they're not putting enough quality in that funnell, 
(Impact) to grow the business the way we want to see it grow." 

"#3
(Stroke) Or maybe we are putting enough business in, and we're closing it.
(Pain) But we're in a very commoditized based industry, so we find our sales team discounting a little bit more agressive than they need to, 
(Impact) and we're just not getting the margin we need to reinvest back into the business."

"Dave let me ask you, any of that ever been an issue worth a 3-5 minute conversation?"

"If you had to pick one, where would you start?"

"When you say it's... Tell me a little bit more about that."

"Can you help me for a second, I want to make sure I can see the world from your point of view. Can you give me kind of a real life example?"

They start telling a story. **Every Time I Think About It I Get Angry**. Give them time to put themselves into the story. Let them relive the experience, because people buy emotionally.

"And so because of that, what happens?"

Is it relevant? Does it resonate? Are you willing to have a 3-5 minute conversation? Then close for the appointment.

**HOMEWORK:**
"Of the last/best/top 50 prospects that have bought, what are the top 3 reasons they normally buy, what's the impact of them suffering/having that particular pain?"

### Step 5: Closing For An Appointment

* "Do this for me..."
* "Take out your calendar..."
* "Can I make a suggestion..."
* "Invite me out..."
* "Pick a day..."

"Dave, can I make a suggestion? Do this for me. Take a look at your calendar. Let's find a time in the next week, two weeks, where you can invite me out for an hour. You and I can talk much more in depth about some of the things you're doing with regard to increasing that forecast accuracy, what's working, what are some of the things you do know you need to change, pick my brain about the kind of work we do, and then we can figure out together, is there a reason to do some work together. If not listen, 100% okay. We're very good at what we do, we're not right in every situation. And we can figure that out together. Sound fair? And if it does make sense then we can begin to map out more specifically what would be the steps your organization and my organization would take to really figure out, does it make sense to do business together?"

Closing the call professionally sets an agenda for the appointment, authoritatively closes the appointment, and post-sell to avoid back-outs.

**********************

# Book Notes 
Notes from the book.

UpFront Contracts:
* What you and the prospect are expecting to take place
* Time
* Agenda
* Outcome - What decisions will be made as a result of the meeting/conversation

"Bonding and rapport means developing an evironment of trust, comfort, crdibility and functional equality with the prospect; setting an up-front contract means acting on, and reinforcing, that environment of emotional comfort between peers."

"People make buying decisions emotionally and then justify those decisions intellectually."

"You have the sales equivalent of a cure for cancer, and the more people you talk to, the more likely it is that someone out there will benefit from what you offer."

Contact Development Plan vs Call Script

Pattern Interrupt
UFC
Personal Connection / Research
30-Second Commercial

### 30 Second Commercial

The objective of the 30-Second Commercial is *not to sell anything*. The object is to spark the prospect's curiousity and engage him in a conversation. The prospect should be asking himself, "How do they do that?", "Would that work in my situation?", or "How much does that cost?"

* Introduce the name of your company and the nature of your product/service
* Pain statement
* Benefit statement
* Hook question: Measures the relevance of your product/service to the prospect's situation. Purpose is too hook the prospect into a conversation - not create a sale.

e.g.

David, I'm with GitLab. GitLab is the first single application for software development, security, and operations that enables DevOps, making the software lifecycle 3 times faster and radically improving the speed of business.  I spend a lot of my time talking to Chief Architects/Head of IT/Lead DevOps. Typically, when I speak to these people, here's what they say to me: 



>  Bill, I’m (your name) with (name of your company). We are a (type of company) firm that specializes in (solution you deliver). We’ve been very successful in developing and implementing (brief description of product/ service) for (companies/ people) who are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). We’ve been able to help our (clients/ customers) to (description of product/ service benefit, with the emphasis on “what’s in it for me” from the customer’s point of view). (NOTE: If you can include a reference to some kind of “social proof” here, such as a happy customer you’ve worked with, or a trade association you take part in, that’s a plus.) Does anything at all of what I’ve just said (sound like an issue for you/ seem relevant to your world/ describe something with which you are dealing)?

David, I’m Kevin McKinley with GitLab. GitLab is the first single application for software development, security, and operations that enables Concurrent DevOps. We’ve been very successful in developing and implementing (brief description of product/ service) for (companies/ people) who are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). We’ve been able to help our (clients/ customers) to (description of product/ service benefit, with the emphasis on “what’s in it for me” from the customer’s point of view). (NOTE: If you can include a reference to some kind of “social proof” here, such as a happy customer you’ve worked with, or a trade association you take part in, that’s a plus.) Does anything at all of what I’ve just said (sound like an issue for you/ seem relevant to your world/ describe something with which you are dealing)?

> So, Bill, I’m with (So-and-So Inc.). We’re a company that specializes in (very brief description of what you do). Bill, I spend a lot of my time talking to (Vice Presidents of Everything), typically in the (manufacturing) world. And typically, when I speak to those people, here’s what they say to me: They tell me their people are doing a good job of (A, B, and C), but they are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). Has any of that ever been relevant to your world, or even worth a brief conversation?

 So, David , I’m with GitLab. We’re a company that specializes in (very brief description of what you do). Bill, I spend a lot of my time talking to (Vice Presidents of Everything), typically in the (manufacturing) world. And typically, when I speak to those people, here’s what they say to me: They tell me their people are doing a good job of (A, B, and C), but they are concerned about (potential problem #1), disappointed with (potential problem #2), or unhappy with (potential problem #3). Has any of that ever been relevant to your world, or even worth a brief conversation?

> I'm Mark Smith from ABC Company. We are a computer application development firm specializing in custom-designed inventory management programs for manufacturing and distribution operations. We’ve been very successful in developing and implementing systems for companies that are concerned about the costs associated with inaccurate inventory counts, unhappy with paperwork bottlenecks that slow down the order fulfillment process, or disappointed by the amount of time it takes to reconcile purchasing, invoicing, and shipping documents. We’ve been able to help customers like XYZ Company substantially improve their ability to track, process, and account for inventory while eliminating redundant paperwork and speeding up the accounting process. Does anything at all of what I’ve just said seem relevant to you?

I'm Kevin McKinley from GitLab. We've developed a single application for the entire software development toolchain. We've been very successful in helping companies across all DevOps maturity levels that are concerned with modernizing their infrastructure and adopting cloud or transforming from on-premise, unhappy with the silos/handoffs/bottlenecks that slow down their solution delivery, or disappointed by the amount of time it takes to manage and maintain the integrations within the different tools used in the organization. We've been able to help over 100,000 enterprise customers, including Nasdaq, Freddie Mac, and ING Bank deliver software 2-3x faster, reduce toolchain costs by 30%, and increase visibility into their delivery pipeline. Does any of that seem relevant to your world, or even worth a brief conversation?

#### Pain Points for Chief Architect / Head of DevOps

Organizational:
1) Standardization - Getting every team to work together using common tools/practices/etc. 
2) Adoption - Getting devs onboard to change habits and adopt new tools
3) Human Resources - Lack of people with the right technical skill set

Technical:
1) Complexity - Too many moving parts (DevOps daisy chain = too many environments/variables, or managing dependencies/versions of services within stack)
2) Automation - too much manual work (e.g. devs build app, use Docker Compose. DevOps then converts manually and generates Cloud Formation. Devs have a hard time getting to that stack.)
3) Visibility - Developers work with their own version that ignores the shared services which can lead to...
4) Troubleshooting - Problems are hard to diagnose because there's not enough visibility, tracking, or information, leading to "small" bugs taking weeks to troubleshoot

TYPE A
David , I’m with GitLab, the first single application for DevOps. David, I spend a lot of my time talking to Chief Architects, typically in the [Technology, Finance, Communications] world. And typically, when I speak to those people, here’s what they say to me: They tell me their people are doing a good job of (A,B,C), but they are concerned about embracing automation/going cloud native, disappointed with visibility, or unhappy with their legacy software. Has any of that ever been relevant to your world, or even worth a brief conversation?


TYPE B
"Typically, when I talk with a Chief Architect / Head of DevOps 

(Stroke) They are very happy with their development team/have a world class dev team
(Pain) But they tell me they're concerned about the legacy applications their team is using...
(Impact), and as a result their product release cycle is slow and outdated/they're releasing 2x a year instead of 12x a year, or there are constant delays in project delivery."

(Stroke) Or maybe they do have a modern software suite and are mostly happy with it,
(Pain) But they occassionally worry about having too many moving parts, too many environment variables/config files/duplication
(Impact) And as a result have to spend a lot of resource overhead managing the dependencies of the different services, their versions, and how they relate to each other.

(Stroke) Often, they've even got a pretty good core tool set.
(Pain) But they're not cloud native and the organization hasn't fully embraced automation yet
(Impact) And as a result there's too much manual work (for example developers build their apps, use use Docker Compose. DevOps then converts manually and generates Cloud Formation and apply it to a stach that Devs have a hard time getting to.)

"Dave let me ask you, any of that ever been an issue worth a 3-5 minute conversation?"
"If you had to pick one, where would you start?"
"When you say it's... Tell me a little bit more about that."
"Can you help me for a second, I want to make sure I can see the world from your point of view. Can you give me kind of a real life example?"
"And so because of that, what happens?"


#### Pain Points for Head of IT / Director of IT / VP of IT

TYPE A
David , I’m with GitLab, the first single application for DevOps. David, I spend a lot of my time talking to Head/Director/VPs of IT, typically in the [Technology, Finance, Communications] world. And typically, when I speak to those people, here’s what they say to me: They tell me their people are doing a good job of (A,B,C), but they are concerned about embracing automation/going cloud native, disappointed with visibility, or unhappy with their legacy software. Has any of that ever been relevant to your world, or even worth a brief conversation?


TYPE B
Typically, when I talk with a Head of IT, 

1
(Stroke) they've got a modern suite software in their organization. 
(Pain) But they're concerned about security breaches because of devs bringing in their own tools without approval. 
(Impact) And as a result, there's this impending feeling of doom that a security breach is not an IF, but a WHEN. 

2
(Stroke) Maybe they even have state of the art security in place, 
(Pain) but because of the daisy chain of tools they're using, they occasionally worry about running over budget with licensing costs. 
(Impact) Which isn't something you want to have to report to your board of directors.

3
(Stroke) Or maybe we are do have the risk and budget under control...
(Pain) But there's a lack of visibility/tracking within the tools, 
(Impact) and this means problems are hard to diagnose and troubleshooting a "small" bug can take weeks and create expensive downtime.

"Dave let me ask you, any of that ever been an issue worth a 3-5 minute conversation?"

"If you had to pick one, where would you start?"

"When you say it's... Tell me a little bit more about that."

"Can you help me for a second, I want to make sure I can see the world from your point of view. Can you give me kind of a real life example?"

"And so because of that, what happens?"


#### Pain Points for DevOps Director, DevOps Lead

TYPE A
David , I’m with GitLab, the first single application for DevOps. David, I spend a lot of my time talking to Head/Director/VPs of IT, typically in the [Technology, Finance, Communications] world. And typically, when I speak to those people, here’s what they say to me: They tell me their people are doing a good job of (A,B,C), but they are concerned about embracing automation/going cloud native, disappointed with visibility, or unhappy with their legacy software. Has any of that ever been relevant to your world, or even worth a brief conversation?


TYPE B
"Typically, when I talk with a Director of Dev Ops

(Stroke) They are very happy with...
(Pain) But they tell me they occassionally worry about...
(Impact) And as a result..."


### Leaving Voicemails

Research:
> Hey, Bill. John Rosso. Listen, I’m doing a little bit of research on you and ABC Company. I saw that you published a paper recently on sustainable materials in the packaging industry. I have a couple of questions for you. Bill, call me— again, it’s John Rosso, and my number is (555)-555-5555.

Personal Connection:
> Hey, Bill. This is John Rosso. It’s about 4: 20 on Tuesday. Hey, I had a nice conversation with David Jones over at Acme Corporation. He thought it would be important that you and I talk. I promised him I would reach out to you. The number is (555)-555-5555.


Disappearing Act
> Carol, it’s John Rosso from Sandler Training. Hey, I thought we had a productive meeting a couple of weeks back. However, I’ve left several voice mail messages and haven’t heard back from you. Maybe you’ve been extremely busy and you have been unable to get back to me. That’s understandable. Or perhaps you’ve decided on an approach that is different from what we discussed. If that’s the case, my number is (555)-555-5555. If you could give me a quick call back, I can close the file on this for now.

### Classic Pain Questions

> Appendix C
>
>Classic Pain Questions 
>
>Pain questions can help you define the extent of the prospect’s perceived problem. Some good initial pain questions include: 
>
> * Tell me more about that. 
> * Can you be a bit more specific? Give me an example. 
> * How long has that been a problem? 
> * What have you tried to do about that? 
> * How do you feel about that? 
> * Have you given up trying to deal with the problem? 
>
> You may also want to ask questions that focus on a facet of your product or service that addresses some aspect of the prospect’s existing or potential problem, or provides a benefit the prospect is not currently receiving.  For instance: 
>
> * If there was one thing you’d like to see improved in your existing ____, what would that be?
> * What would be the long-term benefit of (streamlining/ consolidating/ expanding) your ____ process?
> * What impact would there be on (profits/ production/ revenues/ expenses) if you could ____?
> * What value would there be if it were easier to ____?
> * How significant a benefit would it be if you could reduce the manpower needed to?
> * What would be the impact of increasing the effectiveness of ____? 
> * Being able to speed up the ____ process would allow you to do what? 
> * What reasons might you have for reducing the time it takes to ____? 
> * How important would it be to reduce the cost of your ____? 
> * What would you be able to accomplish if you were able to ____? 
> * If you wanted to ____, how difficult might it be? 
> * If you needed to ____, what obstacles would you encounter? 
> * What would prevent you from improving ____? 
> * If it were necessary to ____, what would be the biggest challenge? 
> * If ____ didn’t improve, how concerned would you be?

> - Rosso, John. Prospect the Sandler Way: A 30-Day Program for Mastering Stress-Free Lead Development (Kindle Locations 1573-1584). Sandler Training. Kindle Edition. 

### Referrals

David, this is John. I wanted to take the opportunity to introduce the two of you. David is a good friend of mine, and John is a sales training specialist who is engaged with a number of my clients and who does top-notch work. John, I would ask you reach out to Dave and set up a time to speak. If either of you want me to be part of that conversation, or have any questions, please reach out.

