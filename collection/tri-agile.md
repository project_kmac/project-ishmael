
meet on Tues April 2 at Sammy's Bar & Grill?

{{first_name}}, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

I saw on your profile that you led the DevSecOps cultural transformation for AT&T and thought you would enjoy our complimentary Agile happy hour. As you may already know, there are teams at Syneos using our application, GitLab.

As a silver sponsor of  the TriAgile conference, we're hosting a casual evening of drinks and bites at Sammy’s Bar & Grill with Agilist professionals. Colleagues from SAS, IBM, and more will be in attendance.

Appetizers and drinks will be provided.

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You probably wouldn't be interested in joining us for this happy hour, would you?

Warm Regards,
Kevin

****


Mark, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

I saw on your profile that you lead the QA and compliance processes, and thought you would enjoy our complimentary Agile happy hour. As you may already know, there are teams at Syneos using our application, GitLab.

As a silver sponsor of  the TriAgile conference, we're hosting a casual evening of drinks and bites at Sammy’s Bar & Grill with Agilist professionals. Colleagues from SAS, IBM, and more will be in attendance.

Appetizers and drinks will be provided.

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You probably wouldn't be interested in joining us for this happy hour, would you?

Warm Regards,
Kevin

*****

Hello David,

You're not going to TriAgile next week, by any chance, are you?

As a silver sponsor of  the TriAgile conference, we're hosting a casual evening of drinks and bites at Sammy’s Bar & Grill with Agilist professionals. Colleagues from SAS, IBM, and more will be in attendance.

Appetizers and drinks will be provided.

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You probably wouldn't be interested in joining us for this happy hour, would you?

Warm Regards,
Kevin

P.S. I also have some passes to the conference if you'd like to join and aren't already registered.

****

meet next Tues April 2 at Sammy's Bar & Grill?

Hi Julia,

Thank you so much registering for the TriAgile Happy Hour next Tuesday, April 2 at Sammy's Bar and Grill.

As you may already be aware, there are several teams at SAS using GitLab. 

Sean Billow, who oversees your account, will be there as well and wanted to make sure you are able to connect with him.

Do you have a window for a brief 15 min call this week or next?

Warm Regards,
Kevin