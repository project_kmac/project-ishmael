# Series D Announcement 

[Planting Seeds] 
https://drive.google.com/open?id=1yn7laP9ocxsB-V-fljF_znYyv9TkXw8X

Our Series D announcement is only 8 Days away (Sept 20). 

That gives us plenty of time to plan ahead and get started leveraging this upcoming news. It will be big - think back to the GitHub acquisition.

 So it's important to get ahead of this and have messaging, sequences, and templates ready to leverage the news. 

 Next week, GitLab will essentially be **omnipresent**, so all of your emails, calls, videos, social touches will have added weight. 

How are you going to maximize this opportunity? Share an example of the outreach you'll use. I'll start - link to sample email in the comments.

Here's your challenge, should you accept.

For the SDR (sorry BDRs, outbound only) who gets the MOST meetings booked from their Series D outreach, I will personally buy you dinner at your favourite restaurant. 

Take your SO, go to Fleming's or whatever you like, dinner's on me. Call it "Series D Date Night".

Now, I don't think anyone going to get more Series D meetings booked than me, so I'm betting I'll get to keep my money.

If you're up to the challenge, reply with a comment below saying "SERIES D".

** DO NOT POST/EMAIL/SAY ANYTHING ABOUT THE SERIES D PUBLICLY UNTIL IT IS ANNOUNCED OFFICIALLY. THIS IS TOP SECRET FOLKS **


*****

If you want to swipe my sequence, feel free to do so here: https://app1a.outreach.io/sequences/1541

## Email Monday Sept 17

subject: I confess

Hey {{first_name}},

>	You’re gonna hear a lot of things
>	about me. [...] Well you believe it. 'Cuz I did all of it.

![alt text](https://media1.tenor.com/images/7718c04ff8ef092e558329f90369f292/tenor.gif "I Confess")

Later this week, you're going to hear a lot of things about GitLab.

... Well you believe it. Because we did all of it.

Yes, we did help Ticketmaster get a 15x faster build time (down from 2 hours just to 8 mins)

Yes, we did help Paessler AG automate QA tasks down from 1 hour to 30 seconds...

You'll probably hear quotes from Global Investment Banking Institutions like... "Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure"

Well, I confess... it's true.

When this lands, I'll probably booked solid for the upcoming months. 

I wanted to give you a chance to get in before the deluge happens.

In case you don't already know, GitLab is a single application for the entire software development lifecycle. 

We're used by enterprises like ING Bank, Nasdaq, and Freddie Mac to consolidate their toolchain, deliver software 3x faster, and reduce IT costs by 30%.

Not asking you to rip & replace; just want to give you the chance to have a 5-10 min call and share how we can help.

Here are a few suggested times:



Best,
{{sender.first_name}}

## Email Sept 20

// This email not to be sent out until the Series D Announcement goes out

subject: {{first_name}}, I'm sorry

Dear {{first_name}},

I owe you a HUGE apology.

These last few months, I've been so busy helping other clients, that I've completely neglected your needs.

By the time you get this email, chances are you'll have heard the news that {{insert-top-secret-news}} <!--GitLab's raised over $100m in a Series D for a valuation of $1B.-->

As part of this last round, we completed a client review of what's happened in the last six months. 

In just that short span of time, our clients have been able to speed up software delivery, reduce IT costs, and consolidate their toolchain. For example...

* Jaguar Land Rover was able to reduce feedback loops cycles from 4-6 weeks... to just 30 minutes. 
* Ticketmaster migrated from Jenkins to GitLab CI to achieve 15x faster build time. "Our GitLab CI build and test takes under 8 minutes to build, test, and publish artifacts"
* Paessler AG automated QA tasks down from 1 hour to 30 seconds
* "Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure" - Leading Global Investment Banking Institution

We've been so busy helping THEM achieve these kinds of extraordinary results that I haven't had the time to share with YOU how you can move to daily releases, while saving over 30% on your IT budget, all under one consolidated tool.

For that, I'm really sorry.

Every day I've been hearing from software engineers, architects, and directors who are tired of the overhead of maintaining their toolchain.  An email from a CISO who's concerned about the vulnerabilities Jenkins introduces into their org.  A LinkedIn message from a DevOps practitioner who's struggling to go faster to keep up with the pace of business.

The truth is, it's not hard to have a consolidated toolchain that will enable agility. It's just that I've dropped the ball helping YOU get those kinds of results.

No more.

I've opened up some time in the next week to speak to some ultra-committed people about how you can speed up software delivery without spending a dime more on IT.

There's no charge for this and it's totally free.

But it's not for everyone.

I can help if any of the following applies to you:

* You must have some sort of DevOps, Agile, or tooling initiative in place
* You're currently using Jenkins (or Travis or Circle) for CI
* You're looking into using Kubernetes or some sort of cloud/containerization technologies
* You're open-minded to learning about how other Fortune500 enterprises are using GitLab

If you're tired of bloated tool stacks, and you're ready to reap the rewards of DevOps, then I'm making the time to show you how it's done.

Click here to book your session: [calendar_link] 


Warmest regards,

{{sender.first_name}}
