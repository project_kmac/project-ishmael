# LinkedIn Events


## Evanta CIO/CISO Summit

Are you at #Evanta CISO / CIO Summit looking for ways to leverage #Agile, hashtag#DevOps, or hashtag#microservices, and modernize your application strategy?

That's exactly what GitLab helps clients in the Healthcare, Financial Services, and Energy Industries accomplish.

Typically, enterprises find the most benefit when they're already looking for the latest and greatest in cloud native tech like containerization, microservices, automated CI/CD pipelines, and Kubernetes.

Want to learn more? Visit us in booth 521 and ask for Sean Billow

****

## Gartner APPS

Are you at Gartner Applications Summit looking for ways to leverage hashtag#Agile, hashtag#DevOps, or hashtag#microservices, and modernize your application strategy?

That's exactly what GitLab helps clients in the Healthcare, Financial Services, and Energy Industries accomplish.

Typically, enterprises find the most benefit when they're already looking for the latest and greatest in cloud native tech like containerization, microservices, automated CI/CD pipelines, and Kubernetes.

Want to learn more? Visit us in booth 521 and ask for Sean Billow


****

## AWS re:Invent

LOOKING for IT Directors/VPs of IT... Who are going to AWS re:Invent in Las Vegas

If you are going to AWS:reInvent in Las Vegas this November... then read on.

We help the enterprise adopt DevOps, modernize their legacy applications infrastructure, and align IT to business strategy.

That's our business. Helping YOUR business deliver software faster while reducing your IT costs - through a single application for the entire SDLC.

We are looking to meet with the right IT Directors/VPs of IT who have these initiatives in place at the AWS re:Invent.

To book a time slot and meet our CEO in person at AWS re:Invent, fill out the application below and we will reach out and take it from there if it's a good fit. hashtag#AWS hashtag#reinvent hashtag#AWSreInvent hashtag#devops

****