dinner at The Capital Grille next Tuesday?


Hello Chris,

What do you, a bank vault, and a Bone-In Kona Crusted Dry Aged NY Strip have in common?

You're all invited to a dinner at The Capital Grille on Wall Street next Tuesday Dec 4 from 6 - 8:30 PM.

We're doing this at the request of a customer to facilitate a FinServices focused "real world" conversation on DevOps.

Executives from Goldman Sachs, Citigroup, and BNY Mellon and other banks will be in attendance, with Goldman Sachs sharing how they've been able to achieve 1,000+ builds per day

It is assigned seating so please do let me know so we can reserve your spot.

Warmest regards,
Kevin

P.S. Figured this was a longshot on late notice, but wanted to offer it to you in case you're available. Also sending an invitation to John and Keith.
