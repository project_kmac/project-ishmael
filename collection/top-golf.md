Wiley, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

I'm the SDR of GitLab for AT&T. We are a software company that's built an application for the entire SDLC/DevOps lifecycle, and as you may already know, there are already a few teams at AT&T utilizing GitLab.

I saw on your profile that you work in the public cloud infrastructure arm of the organization and thought you would enjoy our complimentary workshop, especially with regards to implementing infrastructure as code.

We're hosting this at TopGolf Dallas on behalf of a customer to facilitate a "real world" conversation on DevOps.

Executives from USAA, Toyota, and more will be in attendance with USAA sharing the story of their Journey to Continuous Delivery and Toyota talking about building lean, continuous integration and delivery pipelines.

Food, drinks, and golf games provided.

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You probably wouldn't be interested in joining us for this workshop, would you?

Warm Regards,
Kevin


****


Wiley, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

I saw on your profile that you work in the public cloud infrastructure arm of the organization and thought you would enjoy our complimentary workshop, especially with regards to implementing infrastructure as code.

We're hosting this at TopGolf Dallas on behalf of a customer to facilitate a "real world" conversation on DevOps.

Executives from USAA, Toyota, and more will be in attendance -- with USAA sharing the story of their Journey to Continuous Delivery and Toyota talking about building lean continuous integration and delivery pipelines.

Food, drinks, and golf games provided.

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You probably wouldn't be interested in joining us for this workshop, would you?

Warm Regards,
Kevin


****


meet on Tues, March 19 at TopGolf in Dallas?

, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

I saw on your profile that you led the DevSecOps cultural transformation for AT&T and thought you would enjoy our complimentary workshop.

We're hosting this at TopGolf Dallas on behalf of a customer to facilitate a "real world" conversation on DevOps. And as you may already know, there are a few teams at AT&T utilizing GitLab for DevOps.

Executives from USAA, Toyota, and more will be in attendance -- with USAA sharing the story of their Journey to Continuous Delivery and Toyota talking about building lean continuous integration and delivery pipelines.

Food, drinks, and golf games provided.

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You probably wouldn't be interested in joining us for this workshop, would you?

Warm Regards,
Kevin


****

Bank of America

Benjamin, please accept my apology for the intrusion. I'm using the Inmail feature of LinkedIn - and as per their guidance, the protocol to reach out using this feature is appropriate.

I saw on your profile that you develop and maintain the company project management system for B of A and thought you would enjoy our complimentary workshop.

We're hosting this at TopGolf Dallas on behalf of a customer to facilitate a "real world" conversation on DevOps.

Executives from USAA, Toyota, and more will be in attendance -- with USAA sharing the story of their Journey to Continuous Delivery and Toyota talking about building lean continuous integration and delivery pipelines.

Food, drinks, and golf games provided.

I hope you can join as our guest. If you can't make it, let me know if there's an interest in the future and I'll keep you on that list.

You probably wouldn't be interested in joining us for this workshop, would you?

Warm Regards,
Kevin



meet on Tue March 19 at TopGolf in Dallas?



*****


Response

David, great to hear that you'll be joining us. Here is the registration page with the exact details. Please do sign up, as it is by reservation only. 

https://about.gitlab.com/events/gitlab-connect-dallas/


Cheers,
Kevin




https://about.gitlab.com/events/gitlab-connect-dallas/


*****

Email for folks who have already registered to the event. Send to set up SAL meetings.

Thanks to Adam Pestreich for this template:


Hi {{first_name}},

Thank you so much registering for GitLab Connect in Dallas next Tuesday.

As you may already be aware, there are several teams at AT&T using GitLab. Sean Billow, who oversees your account, will be there as well and wanted to make sure you are able to connect with him at the event.

Here are a few open time slots.
Tuesday (Mar 19) 3-3:15 PM CDT
Tuesday (Mar 19) 3:30-3:45 PM CDT
Tuesday (Mar 19) 4-4:15 PM CDT
Tuesday (Mar 19) 4:30-4:45 PM CDT
Tuesday (Mar 19) 5-5:15 PM CDT
Tuesday (Mar 19) 5:30-5:45 PM CDT
Tuesday (Mar 19) 6-6:15 PM CDT

Do any of these work for you?

Warm Regards,
Kevin


