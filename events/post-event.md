# Post Event Follow Up

## Southern Fried Agile

subject: Mark thanks for stopping by our booth at SFA

Hey Mark,

Thanks for stopping by our booth at SFA and requesting a follow up.

You mentioned that you currently use GitHub and Travis CI, and are curious to learn more about our CI/CD.

As part of the Enterprise team supporting Movement here, wanted to extend an overview/demo with your team and our strategic account leader and solutions architect.

We do this to provide support to all of our large clients. What's the best way to get in touch?

Happy Thanksgiving!
Kevin

P.S. Apologies if you've gotten multiple emails today - the first was automated from marketing but I wanted to make sure I sent one personally as well.


## AWS re:Invent

subject: {{first_name}} thanks for stopping by our booth at SFA

Hey {{first_name}},

Thanks for stopping by our booth at AWS and requesting a follow up.

You mentioned that you {{!insert_relevant_notes}}.

As part of the Enterprise team supporting {{company}} here, wanted to extend an overview/demo with your team and our strategic account leader and solutions architect.

We do this to provide support to all of our large clients. What's the best way to get in touch?

Warm Regards,
Kevin
