# AWS re:Invent 2018

This is the biggest event of the year. However, we don't know exactly who's going to be there (we don't have an attendee list like with some of the other events). This is an attempt at an approach method that will work even if don't know if they're going to the event.

****

Here's the Outreach sequence: https://app1a.outreach.io/sequences/1963

Please clone and edit as needed if you're going to use this.

Add prospects most likely to be there. Add prospects from your Dream 100 client list.

Keep these feed up and keep your eye open for prospects from your Named/Geo accounts who are there

* LinkedIn feed: https://www.linkedin.com/feed/topic/?keywords=%23awsreinvent
* Twitter feed: https://twitter.com/search?f=tweets&vertical=default&q=aws%20reinvent&src=tyah  

Send email/call/text/InMail (yes, all channels) immediately if someone is there.

****

Here's how the call script goes.

1. Pattern Interrupt 

"Hey {first_name}, it's Kevin McKinley. Are you by any chance going to be at AWS this week?"

Whether they say Yes or No, you move into the UpFront Contract -> Pitch. The importance of this is that their answer dictates whether you ask for an in-person meeting/booth visit, or ask for a regular IQM.

2. UpFront Contract 

"Okay, great. Let me share with you the reason why I'm calling, and you can decide if it makes sense to talk further."

3. Pitch (30 Second Commercial)

4. Ask for meeting

If they're at AWS:
"Looking to set up an in person meeting with you and our CEO while you're at AWS..."

If not at AWS, regular Ask:
"Looking for a brief 5-10 min call to share how GitLab can help... what's the best way to get on your calendar?" 

