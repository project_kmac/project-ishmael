# Email Sequences

## Acushnet - Master

Drop your Tier 1 and Tier 2 prospects into this. Use custom33 (personalized first line) for each prospect so that you can customize/personalize at scale.

https://app1a.outreach.io/sequences/1853/

## Meeting No Show

https://app1a.outreach.io/sequences/1854

## Follow Up - With Calls

Use for prospects with a phone number that have had some level of engagement/interest (replied to an email, asked for more information, picked up the phone, etc.)

https://app1a.outreach.io/sequences/1880

## Follow Up - Without Calls

Use for prospects without a phone number that have had some level of engagement/interest (replied to an email, asked for more information, picked up the phone, etc.)


https://app1a.outreach.io/sequences/1881

## Referral Sequence

When you get a referral to another prospect. Please view the referral call in the call scripts for how to handle calls and leaving referral voicemails.

https://app1a.outreach.io/sequences/1855

## Pre-Event 

WIP

## Post-Event Follow Up

https://app1a.outreach.io/sequences/2011


## WIP - Tier 3 / Volume Sequence

Remember your LinkedIn Sales Nav scrape for keywords? We filtered out and prioritized Tier 1 & Tier 2 based on titles (Director, VP, CXO). Lower ranking titles (Senior, Manager, Entry Level) go into a volume sequence. Not a high priority, but this does help round out your outreach.

